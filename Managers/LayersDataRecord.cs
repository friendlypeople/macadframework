// Copyright (c) 2012 Сергей Попов, Виталий Сайдин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Sergey Popov, Vitaliy Saydin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using Autodesk.AutoCAD.DatabaseServices;

namespace MAcadFramework.Managers
{
	/// <summary>
	/// Настроки слоя для использования в менеджере слоев
	/// </summary>
	public class LayersDataRecord
	{
		private string		_name		= String.Empty;
		private LineWeight  _weight		= LineWeight.ByLayer;
		private short		_colorIndex = 7;
		private bool		_visible	= true;
		private string		_lineName = "";
		private bool		_isPlottable = true;

		/// <summary>
		/// Инициализирует новый экземпляр класса LayersDataRecord.
		/// </summary>
		/// <param name="name">Имя слоя</param>
		/// <param name="weight">Толщина линии.</param>
		/// <param name="colorIndex">Цвет слоя.</param>
		public LayersDataRecord(string name, LineWeight weight, short colorIndex)
		{
			_name = name;
			_colorIndex = colorIndex;
			_weight = weight;
		}

		/// <summary>
		///  Инициализирует новый экземпляр класса LayersDataRecord.
		/// </summary>
		/// <param name="name">Имя слоя</param>
		/// <param name="colorIndex">Цвет слоя.</param>
		public LayersDataRecord(string name, short colorIndex)
		{
			_name = name;
			_colorIndex = colorIndex;
		}

		/// <summary>
		/// Инициализирует новый экземпляр класса LayersDataRecord.
		/// </summary>
		/// <param name="name">Имя слоя</param>
		/// <param name="colorIndex">Цвет слоя.</param>
		/// <param name="linetypeName">Название типа линии.</param>
		public LayersDataRecord(string name, short colorIndex, string linetypeName)
		{
			_name = name;
			_colorIndex = colorIndex;
			_lineName = linetypeName;
		}

		/// <summary>
		/// Инициализирует новый экземпляр класса LayersDataRecord.
		/// </summary>
		/// <param name="name">Имя слоя</param>
		/// <param name="colorIndex">Цвет слоя.</param>
		/// <param name="isPlottable">если установлено <c>true</c> [этот слой можно печатать].</param>
		public LayersDataRecord(string name, short colorIndex, bool isPlottable)
		{
			_name = name;
			_colorIndex = colorIndex;
			_isPlottable = isPlottable;
		}

		/// <summary>
		/// Получить или задать цвет слоя.
		/// </summary>
		/// <value>Цвет слоя.</value>
		public short ColorIndex
		{
			get { return _colorIndex; }
			set { _colorIndex = value; }
		}

		/// <summary>
		/// Получить или задать толщину линии.
		/// </summary>
		/// <value>Толщина линии.</value>
		public LineWeight Weight
		{
			get { return _weight; }
			set { _weight = value; }
		}

		/// <summary>
		/// Получить или задать имя слоя.
		/// </summary>
		/// <value>Имя слоя.</value>
		public string Name
		{
			get { return _name; }
			set { _name = value; }
		}

		/// <summary>
		/// Получить или задать значение, указывающее, является ли данный слой видимым.
		/// </summary>
		/// <value><c>true</c> если слой видимый; иначе, <c>false</c>.</value>
		public bool IsVisible
		{
			get { return _visible; }
			set { _visible = value; }
		}

		/// <summary>
		/// Получить или задать значение, указывающее, является ли данный слой plottable.
		/// </summary>
		/// <value><c>true</c> если слой можно печатать; иначе, <c>false</c>.</value>
		public bool IsPlottable
		{
			get { return _isPlottable; }
			set { _isPlottable = value; }
		}

		/// <summary>
		/// Получить или задать имя типа линии для слоя
		/// </summary>
		/// <value>Имя типа линии для слоя</value>
		public string LineName
		{
			get { return _lineName; }
			set { _lineName = value; }
		}
	}
}
