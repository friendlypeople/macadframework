// Copyright (c) 2012 Сергей Попов, Виталий Сайдин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Sergey Popov, Vitaliy Saydin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections.Generic;
using Autodesk.AutoCAD.Colors;
using Autodesk.AutoCAD.DatabaseServices;
using MAcadFramework.Adapters;
using MAcadFramework.Routines;

namespace MAcadFramework.Managers
{
	/// <summary>
    /// Менеджер слоев
    /// </summary>
	/// <remarks>
	/// Обеспечивает создание словоев по настройкам. Используется для группового и отложенного
	/// создания слоев.
	/// </remarks>
	public sealed class LayersManager
	{
		private readonly Dictionary<string, LayersDataRecord> _systemLayerTable = new Dictionary<string, LayersDataRecord>(10);

        /// <summary>
        /// Добавлить запись настроек слоя в менеджер.
        /// </summary>
        /// <param name="ldr">Настройки слоя</param>
		public void AddRecord(LayersDataRecord ldr)
		{
			if (!_systemLayerTable.ContainsKey(ldr.Name))
			{
				_systemLayerTable.Add(ldr.Name, ldr);
			}
		}

        /// <summary>
        /// Создать слой.
        /// </summary>
        /// <param name="name">Имя слоя</param>
        /// <returns>Оболочка над слоем <see cref="LayerAdapter"/></returns>
		public ILayerAdapter MakeLayer(string name)
		{
			LayersDataRecord ldr;
			if (!_systemLayerTable.TryGetValue(name, out ldr))
			{
				return new NullableLayer();
			}
			using (EditorRoutine.LockDoc())
			{
				using (Transaction trans = DBRoutine.StartTransaction())
				{
					Color tempColor = Color.FromColorIndex(ColorMethod.ByLayer, ldr.ColorIndex);
					// Получаем таблицу слоев
					LayerTable layersTable =
						(LayerTable) trans.GetObject(DBRoutine.WorkingDatabase.LayerTableId, OpenMode.ForWrite, false);
					if (layersTable == null)
					{
						trans.Abort();
						return new NullableLayer();
					}

					// Если есть в таблице слоев слой с именем...
					LayerTableRecord layer;
					if (!layersTable.Has(ldr.Name))
					{
						layer = new LayerTableRecord();
						layer.Name = ldr.Name;
						layer.Color = tempColor;
						layer.LineWeight = ldr.Weight;
						layer.IsPlottable = ldr.IsPlottable;
						SafelySetLineStyleToLayer(ldr, layer);
						

						layersTable.Add(layer);
						// Завершительные действия с транзакциями по закрытию вновь созданного объекта
						trans.TransactionManager.AddNewlyCreatedDBObject(layer, true);
					}
					else
					{
						layer = trans.GetObject(layersTable[ldr.Name], OpenMode.ForWrite) as LayerTableRecord;
						if (layer == null)
						{
							trans.Abort();
							return new NullableLayer();
						}
						layer.Color = tempColor;
						layer.LineWeight = ldr.Weight;
						layer.IsPlottable = ldr.IsPlottable;
						SafelySetLineStyleToLayer(ldr, layer);
					}
					trans.Commit();
					return new LayerAdapter(layer.Id, layer.Name);
				}
			}
		}

        /// <summary>
        /// Безопасно задать стиль линии для слоя.
        /// </summary>
        /// <param name="ldr">The LDR.</param>
        /// <param name="layer">The layer.</param>
		private static void SafelySetLineStyleToLayer(LayersDataRecord ldr, LayerTableRecord layer)
		{
			if (!String.IsNullOrEmpty(ldr.LineName))
			{
				ObjectId id = LineTypeRoutine.GetObjectId(ldr.LineName);
				if (id != ObjectId.Null)
					layer.LinetypeObjectId = id;
			}
		}
	}
}
