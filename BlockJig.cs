// Copyright (c) 2012 Сергей Попов, Виталий Сайдин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Sergey Popov, Vitaliy Saydin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System.Collections.Generic;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;

namespace MAcadFramework
{
	/// <summary>
	/// Класс Jig(закрепления) блока
	/// </summary>
	public class BlockJig : EntityJig
	{
		private Point3d _pos;
		private readonly Dictionary<ObjectId, AttributeInfo> _attInfo;
		private readonly Transaction _tr;

		internal BlockJig(Transaction tr, BlockReference br, Dictionary<ObjectId, AttributeInfo> attInfo)
			: base(br)
		{
			_pos = br.Position;
			_attInfo = attInfo;
			_tr = tr;
		}

		protected override bool Update()
		{
			BlockReference br = Entity as BlockReference;

			if (br != null)
			{
				br.Position = _pos;

				if (br.AttributeCollection.Count != 0)
				{
					foreach (ObjectId id in br.AttributeCollection)
					{
						DBObject obj = _tr.GetObject(id, OpenMode.ForRead);
						AttributeReference ar = obj as AttributeReference;

						// Apply block transform to att def position
						if (ar != null)
						{
							ar.UpgradeOpen();
							AttributeInfo ai = _attInfo[ar.ObjectId];
							ar.Position = ai.Position.TransformBy(br.BlockTransform);

							if (ai.IsAligned)
							{
								ar.AlignmentPoint = ai.Alignment.TransformBy(br.BlockTransform);
							}

							if (ar.IsMTextAttribute)
							{
								ar.UpdateMTextAttribute();
							}
						}
					}
				}
			}
			return true;
		}

		protected override SamplerStatus Sampler(JigPrompts prompts)
		{
			JigPromptPointOptions opts = new JigPromptPointOptions("\nSelect insertion point:");

			opts.BasePoint = new Point3d(0, 0, 0);
			opts.UserInputControls = UserInputControls.NoZeroResponseAccepted;

			PromptPointResult ppr = prompts.AcquirePoint(opts);

			if (_pos == ppr.Value)
			{
				return SamplerStatus.NoChange;
			}

			_pos = ppr.Value;
			return SamplerStatus.OK;
		}

        /// <summary>
        /// Запустить jig для блока.
        /// </summary>
        /// <returns>Результат Jig'а</returns>
		public PromptStatus Run()
		{
			Document doc = Application.DocumentManager.MdiActiveDocument;
			Editor ed = doc.Editor;
			PromptResult promptResult = ed.Drag(this);
			return promptResult.Status;
		}
	}
}
