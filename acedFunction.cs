// Copyright (c) 2012 Сергей Попов, Виталий Сайдин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Sergey Popov, Vitaliy Saydin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Runtime.InteropServices;
using Autodesk.AutoCAD.GraphicsSystem;

namespace MAcadFramework
{
	public class acedFunction
	{

        /// <summary>
        /// Перевод точки между системами координат автокада.
        /// </summary>
  
		[System.Security.SuppressUnmanagedCodeSecurity]
		[DllImport("acad.exe", CallingConvention = CallingConvention.Cdecl)]
		public static extern int acedTrans(double[] pt1, IntPtr rb1, IntPtr rb2, int disp, double[] pt2);

		// Функция возвращающая строку: Command:
		[DllImport("acad.exe")]
		public static extern void acedPostCommandPrompt();

#if x86
        private const string AcadGetCurrentDwgViewEntryPoint = "?AcadGetCurrentDwgView@@YAPAUHWND__@@XZ";
#else
        private const string AcadGetCurrentDwgViewEntryPoint = "?AcadGetCurrentDwgView@@YAPEAUHWND__@@XZ";
#endif
        /// <summary>
        /// Получить текущий DWG вид.
        /// </summary>
        [DllImport("acad.exe", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode, EntryPoint = AcadGetCurrentDwgViewEntryPoint)]
		public static extern IntPtr AcadGetCurrentDwgView();

        /// <summary>
        /// Установить толщину подсвеченной линии.
        /// </summary>
        /// <param name="value">Значение.</param>
        [DllImport("acad.exe", EntryPoint = "?acgsSetHighlightLineWeight@@YAXG@Z", CallingConvention = CallingConvention.Cdecl)]
        public static extern void acgsSetHighlightLineWeight(UInt16 value);

        /// <summary>
        /// Получить толщину подсвеченной линии.
        /// </summary>
        /// <returns></returns>
        [DllImport("acad.exe", EntryPoint = "?acgsGetHighlightLineWeight@@YAGXZ", CallingConvention = CallingConvention.Cdecl)]
        public static extern UInt16 acgsGetHighlightLineWeight();

        /// <summary>
        /// Получить шаблон подсвеченной линии.
        /// </summary>
        /// <returns></returns>
        [DllImport("acad.exe", EntryPoint = "?acgsGetHighlightLinePattern@@YA?AW4LinePattern@AcGs@@XZ", CallingConvention = CallingConvention.Cdecl)]
        public static extern LinePattern acgsGetHighlightLinePattern();

        /// <summary>
        /// Установить шаблон подсвеченной линии.
        /// </summary>
        /// <param name="pattern">The pattern.</param>
        [DllImport("acad.exe", EntryPoint = "?acgsSetHighlightLinePattern@@YAXW4LinePattern@AcGs@@@Z", CallingConvention = CallingConvention.Cdecl)]
        public static extern void acgsSetHighlightLinePattern(LinePattern pattern);

        /// <summary>
        /// Получить цвет подсветки.
        /// </summary>
        /// <returns></returns>
        [DllImport("acad.exe", EntryPoint = "?acgsGetHighlightColor@@YAGXZ", CallingConvention = CallingConvention.Cdecl)]
        public static extern UInt16 acgsGetHighlightColor();

        /// <summary>
        /// Установить цвет подсветки.
        /// </summary>
        /// <param name="colorIndex">Index of the color.</param>
        [DllImport("acad.exe", EntryPoint = "?acgsSetHighlightColor@@YAXG@Z", CallingConvention = CallingConvention.Cdecl)]
        public static extern void acgsSetHighlightColor(UInt16 colorIndex);
	}
}
