
//////////////////////////////////////////////////////////////////////////
//         (C) Copyright 2006 by Alexander Rivilis
//////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;

namespace MAcadFramework.Geometry
{
	/// <summary>
	/// Класс для преобразования точки (или вектора) между системами координат.
	/// </summary>
	/// <remarks>
	/// Системы координат AutoCAD
	/// WCS - World Coordinate System.
	/// The “reference” coordinate system. All other coordinate systems are defined relative to the WCS,
	/// which never changes. Values measured relative to the WCS are stable across changes to other coordinate systems.
	/// 
	/// UCS - User Coordinate System.
	/// The “working” coordinate system. All points passed to AutoCAD commands, including those returned from
	/// AutoLISP routines and external functions, are points in the current UCS (unless the user precedes them with a * at
	/// the Command prompt). If you want your application to send coordinates in the WCS, ECS, or DCS to AutoCAD commands,
	/// you must first convert them to the UCS by calling acedTrans().
	/// 
	/// ECS - Entity Coordinate System.
	/// Point values returned by acdbEntGet() are expressed in this coordinate system relative to the entity itself.
	/// Such points are useless until they are converted into the WCS, current UCS, or current DCS,
	/// according to the intended use of the entity. Conversely, points must be translated into an ECS
	/// before they are written to the database by means of acdbEntMod() or acdbEntMake().
	/// 
	/// DCS - Display Coordinate System.
	/// The coordinate system into which objects are transformed before they are displayed.
	/// The origin of the DCS is the point stored in the AutoCAD TARGET system variable,
	/// and its Z axis is the viewing direction. In other words, a viewport is always a plan view of its DCS.
	/// These coordinates can be used to determine where something appears to the AutoCAD user.
	/// 
	/// PSDCS Paper Space DCS.
	/// This coordinate system can be transformed only to or from the DCS of the currently active model space viewport.
	/// This is essentially a 2D transformation, where the X and Y coordinates are always scaled and are offset
	/// if the disp argument is 0. The Z coordinate is scaled but is never translated; it can be used to find the scale factor
	/// between the two coordinate systems. The PSDCS (integer code 2) can be transformed only into the current model space viewport:
	/// if the from argument equals 3, the to argument must equal 2, and vice versa.
	/// </remarks>
	public class CSTrans
	{
		/// <summary>
		/// Short integer
		/// </summary>
		const int RTSHORT = 5003;

		/// <summary>
		/// Преобразование из системы координат пользователя в мировую систему координат
		/// </summary>
		/// <param name="pt">Исходная точка</param>
		/// <param name="isVect">Если задано <c>true</c> [то считать вектором].</param>
		/// <returns>Преобразованная точка</returns>
		public static Point3d Ucs2Wcs(Point3d pt, bool isVect)
		{
			return Translate(pt, 1, 0, isVect);
		}

		/// <summary>
		/// Преобразование из системы координат мировой в пользовательскую систему координат
		/// </summary>
		/// <param name="pt">Исходная точка</param>
		/// <param name="isVect">Если задано <c>true</c> [то считать вектором].</param>
		/// <returns>Преобразованная точка</returns>
		public static Point3d Wcs2Ucs(Point3d pt, bool isVect)
		{
			return Translate(pt, 0, 1, isVect);
		}


		/// <summary>
		/// Преобразование из системы координат мировой в текущую систему координат чертежа
		/// </summary>
		/// <param name="pt">Исходная точка</param>
		/// <param name="isVect">Если задано <c>true</c> [то считать вектором].</param>
		/// <returns>Преобразованная точка</returns>
		public static Point3d Wcs2Dcs(Point3d pt, bool isVect)
		{
			return Translate(pt, 0, 2, isVect);
		}

		/// <summary>
		/// Преобразование из текущей системы координат чертежа в мировую систему координат
		/// </summary>
		/// <param name="pt">Исходная точка</param>
		/// <param name="isVect">Если задано <c>true</c> [то считать вектором].</param>
		/// <returns>Преобразованная точка</returns>
		public static Point3d Dcs2Wcs(Point3d pt, bool isVect)
		{
			return Translate(pt, 2, 0, isVect);
		}

		/// <summary>
		/// Преобразование из системы координат пользователя в текущую систему координат чертежа
		/// </summary>
		/// <param name="pt">Исходная точка</param>
		/// <param name="isVect">Если задано <c>true</c> [то считать вектором].</param>
		/// <returns>Преобразованная точка</returns>
		public static Point3d Ucs2Dcs(Point3d pt, bool isVect)
		{
			return Translate(pt, 1, 2, isVect);
		}

		/// <summary>
		/// Преобразование из текущей системы координат чертежа в пользовательскую систему координат
		/// </summary>
		/// <param name="pt">Исходная точка</param>
		/// <param name="isVect">Если задано <c>true</c> [то считать вектором].</param>
		/// <returns>Преобразованная точка</returns>
		public static Point3d Dcs2Ucs(Point3d pt, bool isVect)
		{
			return Translate(pt, 2, 1, isVect);
		}

		/// <summary>
		/// Преобразование из текущей системы координат чертежа в систему координат пространства листа
		/// </summary>
		/// <param name="pt">Исходная точка</param>
		/// <param name="isVect">Если задано <c>true</c> [то считать вектором].</param>
		/// <returns>Преобразованная точка</returns>
		public static Point3d Dcs2PsUcs(Point3d pt, bool isVect)
		{
			return Translate(pt, 2, 3, isVect);
		}

		/// <summary>
		/// Преобразование из системы координат пространства листа в текущую систему координат чертежа
		/// </summary>
		/// <param name="pt">Исходная точка</param>
		/// <param name="isVect">Если задано <c>true</c> [то считать вектором].</param>
		/// <returns>Преобразованная точка</returns>
		public static Point3d PsUcs2Dcs(Point3d pt, bool isVect)
		{
			return Translate(pt, 3, 2, isVect);
		}

		/// <summary>
		/// Функция преобразования между системами координат
		/// </summary>
		/// <param name="pt">Точка для преобразования</param>
		/// <param name="sys1">Первая система координат</param>
		/// <param name="sys2">Вторая система координат</param>
		/// <param name="isVect">Если false — преобразование точки, если true — преобразование вектора</param>
		/// <returns>Возвращает преобразованную точку</returns>
		private static Point3d Translate(Point3d pt, int sys1, int sys2, bool isVect)
		{
			double[] ptTr = { 0, 0, 0 };
			ResultBuffer rb1 = new ResultBuffer(new TypedValue(RTSHORT, sys1));
			ResultBuffer rb2 = new ResultBuffer(new TypedValue(RTSHORT, sys2));
			acedFunction.acedTrans(pt.ToArray(), rb1.UnmanagedObject, rb2.UnmanagedObject, isVect ? 1 : 0, ptTr);
			rb1.Dispose(); rb2.Dispose();
			return new Point3d(ptTr);
		}
	}
}
