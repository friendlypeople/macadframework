// Copyright (c) 2012 Сергей Попов, Виталий Сайдин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Sergey Popov, Vitaliy Saydin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System.Collections.Generic;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;

namespace MAcadFramework.Aggregates
{
	/// <summary>
	/// Узел полилинии
	/// </summary>
	public class PolylineNode
	{
		private readonly Polyline _poly;
		private readonly int _index;
		private readonly int _next;
		private readonly int _prevoise;

		/// <summary>
		/// Создание нового экземпляра класса <see cref="PolylineNode"/>.
		/// </summary>
		/// <param name="poly">Полилиния AutoCAD</param>
		/// <param name="index">Индекс текущего узла.</param>
		/// <param name="next">Индекс следующего узла.</param>
		/// <param name="prevoise">Индекс предыдущего узла.</param>
		public PolylineNode(Polyline poly, int index, int next, int prevoise)
		{
			_poly = poly;
			_index = index;
			_next = next;
			_prevoise = prevoise;
		}

		/// <summary>
		///  Получить координаты 3D текущей точки
		/// </summary>
		/// <value>Координаты текущей точки</value>
		public Point3d Point3D
		{
			get { return _poly.GetPoint3dAt(_index); }
		}

        /// <summary>
        ///  Получить координаты 2D текущей точки
        /// </summary>
        /// <value>Координаты текущей точки</value>
        public Point2d Point2D
        {
            get { return _poly.GetPoint2dAt(_index); }
        }

        /// <summary>
        ///  Получить кривизну
        /// </summary>
        /// <value>Кривизна узла</value>
        public double Bulge
        {
            get { return _poly.GetBulgeAt(_index); }
        }

		/// <summary>
		/// Получить координаты следующей точки
		/// </summary>
		/// <value>Координаты следующей точки</value>
		public Point3d NextPoint3D
		{
			get { return _poly.GetPoint3dAt(_next); }
		}

        /// <summary>
        /// Получить координаты следующей точки
        /// </summary>
        /// <value>Координаты следующей точки</value>
        public Point2d NextPoint2D
        {
            get { return _poly.GetPoint2dAt(_next); }
        }

		/// <summary>
		/// Получить координаты предыдущей точки
		/// </summary>
		/// <value>Координаты предыдущей точки</value>
		public Point3d PrevoisePoint3D
		{
			get { return _poly.GetPoint3dAt(_prevoise); }
		}

        /// <summary>
        /// Получить координаты предыдущей точки
        /// </summary>
        /// <value>Координаты предыдущей точки</value>
        public Point2d PrevoisePoint2D
        {
            get { return _poly.GetPoint2dAt(_prevoise); }
        }

		/// <summary>
		/// Получить вектор к предыдущей точке
		/// </summary>
		/// <value>Вектор к предыдущей точке</value>
		public Vector3d VectorToPrevoisePoint3D
		{
			get { return PrevoisePoint3D - Point3D; }
		}

        /// <summary>
        /// Получить вектор к предыдущей точке
        /// </summary>
        /// <value>Вектор к предыдущей точке</value>
        public Vector2d VectorToPrevoisePoint2D
        {
            get { return PrevoisePoint2D - Point2D; }
        }

		/// <summary>
		/// Получить вектор к следующей точке
		/// </summary>
		/// <value>Вектор к следующей точке</value>
		public Vector3d VectorToNextPoint3D
		{
			get { return NextPoint3D - Point3D; }
		}

        /// <summary>
        /// Получить вектор к следующей точке
        /// </summary>
        /// <value>Вектор к следующей точке</value>
        public Vector2d VectorToNextPoint2D
        {
            get { return NextPoint2D - Point2D; }
        }

		/// <summary>
		/// Получить тип сегмента
		/// </summary>
		/// <value> Тип сегмента </value>
        public SegmentType SegmnetType
        {
            get { return _poly.GetSegmentType(_index); }
        }
	}

	/// <summary>
	/// Сегмент полилинии
	/// </summary>
	/// <remarks>
	/// Этот класс не реализован, так что ждем помощи
	/// </remarks>
	public class PolylineSegment { }

	/// <summary>
	/// Класс для работы с узлами полилинии
	/// </summary>
	/// <remarks>
	/// Обходит полилинию считая ее ВСЕГДА замкнутой
	/// и игнорирует совпадение последней точки с первой считаяя ее за одну
	/// </remarks>
	public class PolylineInspector
	{
		private readonly Polyline _poly;
		private readonly List<PolylineNode>		_vertexesList = new List<PolylineNode>(10);
		private readonly List<PolylineSegment>	_segmentsList = new List<PolylineSegment>(10);
		private int _index;

		/// <summary>
		/// У полилинии последняя точка совпадает с первой 
		/// </summary>
		private bool _isBadClosed;
		private bool _isClosed;

		private int _numOfVertexes;
		private int _top;


		/// <summary>
		/// Получить или задать IsClosed
		/// </summary>
		/// <value>IsClosed</value>
		public bool IsClosed
		{
			get { return _isClosed; }
		}



		#region Parse

		private void ParseMainProperties()
		{
			_numOfVertexes = _poly.NumberOfVertices;
            Point3d pt = _poly.GetPoint3dAt(0);
			Tolerance tolerance = new Tolerance(0.1, 0.1);
			if (pt.IsEqualTo(_poly.GetPoint3dAt(_numOfVertexes - 1), tolerance))
				_isBadClosed = true;
			_isClosed = _poly.Closed;
			_top = _numOfVertexes;

			if (_isBadClosed) _top--;
		}

		private void ParseGeometry()
		{
			int top = _numOfVertexes;
			if (_isBadClosed) top--;

			for (int i = 0; i < top; i++)
			{
				int next = i;
				int prevoise = i;
				Next(ref next);
				Prevoise(ref prevoise);
				_vertexesList.Add(new PolylineNode(_poly, i, next, prevoise));
			}
		}

		private void Parse()
		{
			ParseMainProperties();
			ParseGeometry();
		}
		#endregion

		/// <summary>
		/// Создание нового экземпляра класса <see cref="PolylineInspector"/>.
		/// </summary>
		/// <param name="poly">The poly.</param>
		public PolylineInspector(Polyline poly)
		{
			_poly = poly;
			Parse();
		}

		private bool Next(ref int val)
		{

			val++;
            if (val >= _top)
			{
				val = 0;
				return false;
			}
			return true;
		}

		private bool Prevoise(ref int val)
		{
			if (val == 0)
            {
                val = _top - 1;
				return false;
			}
			val--;
			return true;
		}


		/// <summary>
		/// Проверить наличие следующего узла
		/// </summary>
		/// <returns><c>true</c> если следующий узел есть, иначе <c>false</c>, и следующий узел вернется первый</returns>
		public bool Next()
		{
			return Next(ref _index);
		}

		/// <summary>
		/// Проверить наличие предыдущего узла
		/// </summary>
		/// <returns><c>true</c> если предыдущий узел есть, иначе <c>false</c>, и предыдущий узел вернется последний</returns>
		public bool Prevoise()
		{
			return Prevoise(ref _index);
		}

		/// <summary>
		/// Получить текущий узел полилинии
		/// </summary>
		public PolylineNode ElementCurrent
		{
			get { return _vertexesList[_index]; }
		}


		/// <summary>
		/// Получить следующий узел полилинии
		/// </summary>
		public PolylineNode ElementNext
		{
			get
			{
				int i = _index;
				Next(ref i);
				return _vertexesList[i];
			}
		}

		/// <summary>
		/// Получить предыдущий узел полилинии
		/// </summary>
		public PolylineNode ElementPrevoise
		{
			get
			{
				int i = _index;
				Prevoise(ref i);
				return _vertexesList[i];
			}
		}

		/// <summary>
		/// Получить или задать текущий индекс узла
		/// </summary>
		/// <value> Текущий индекс узла. </value>
		public int Index
		{
			get { return _index; }
			set
			{
				if (value < 0)
					_index = 0;
				else if (value >= _top)
					_index = _top - 1;
				else
					_index = value;
			}
		}

		/// <summary>
		/// Обновить данные по плилинии
		/// </summary>
        public void Update()
        {
            Parse();
        }
	}
}
