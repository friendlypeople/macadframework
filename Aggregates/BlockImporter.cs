// Copyright (c) 2012 Сергей Попов, Виталий Сайдин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Sergey Popov, Vitaliy Saydin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections.Generic;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Celsio.Runtime.Reflection;
using MAcadFramework.Routines;

namespace MAcadFramework.Aggregates
{
	/// <summary>
	/// Класс отвечающий за вставку блока в чертеж
	/// </summary>
	/// <example>
	/// <code><![CDATA[
	/// // Создаем объект настроек для вставки блока,
	/// // указывая имя блока, имя файла и точку вставки
	/// BlockImporterOption bio = BlockImporterOption.CreateOptionsDirectlyInsert(blockName, fileName, point);
	/// 
	/// // Задаем транзакцию для вставки
	/// bio.Transaction = trans;
	/// 
	/// // Задаем параметры динамического блока
	/// bio.BlockParams.Add("Distance", e1.FrontPlane.Width);
	/// 
	/// // Вставляем блок
	/// BlockImporterResult bir = BlockImporter.InsertBlock(bio);
	/// 
	/// // Проверяем результат
	/// if (!bir.IsValid)
	/// {
	///		EditorRoutine.WriteMessage(bir.ErrorStatus);
	/// }
	/// ]]></code>
	/// </example>
	public static class BlockImporter
	{
		private static void SetPropertyToReference(BlockReference br, IDictionary<string, object> props)
		{
			if (props.Count <= 0) return;
			if (br.IsDynamicBlock)
			{
				DynamicBlockReferencePropertyCollection col = br.DynamicBlockReferencePropertyCollection;
				foreach (DynamicBlockReferenceProperty p in col)
				{
					if (props.ContainsKey(p.PropertyName))
					{
						p.Value = props[p.PropertyName];
					}
				}
			}
		}

		private static void SetAttributeToReference(BlockTableRecord btr, Transaction tr, BlockReference br, Dictionary<ObjectId, AttributeInfo> attInfo)
		{
			if (btr.HasAttributeDefinitions)
			{
				foreach (ObjectId id in btr)
				{
					DBObject obj = tr.GetObject(id, OpenMode.ForRead);

					AttributeDefinition ad = obj as AttributeDefinition;

				    if (ad == null || ad.Constant) continue;

				    AttributeReference ar = new AttributeReference();
				    ar.SetAttributeFromBlock(ad, br.BlockTransform);

				    ar.Position = ad.Position.TransformBy(br.BlockTransform);

				    if (ad.Justify != AttachmentPoint.BaseLeft)
				        ar.AlignmentPoint = ad.AlignmentPoint.TransformBy(br.BlockTransform);

				    if (ar.IsMTextAttribute)
				        ar.UpdateMTextAttribute();

				    ar.TextString = ad.TextString;
				    

				    ObjectId arId = br.AttributeCollection.AppendAttribute(ar);
				    tr.AddNewlyCreatedDBObject(ar, true);

				    // Initialize our dictionary with the ObjectId of
				    // the attribute reference + attribute definition info
				    attInfo.Add(arId, new AttributeInfo(ad.Position, ad.AlignmentPoint, ad.Justify != AttachmentPoint.BaseLeft));
				}
			}
		}

        private static void SomeDarkMagic(BlockReference br, BlockAttributes ba, Transaction tr)
        {
            foreach (ObjectId id in br.AttributeCollection)
            {
                DBObject obj = tr.GetObject(id, OpenMode.ForWrite);

                AttributeReference ar = obj as AttributeReference;

                if (ar == null) continue;
                if (!ba.Contains(ar.Tag)) return;
                Dictionary<string, object> property = ba.GetExtendedProperty(ar.Tag);
                StrongPropertyWrapper swp = new StrongPropertyWrapper(ar);
                foreach (KeyValuePair<string, object> pair in property)
                {
                    if (swp.ContainsSetter(pair.Key))
                        swp[pair.Key] = pair.Value;
                }
            }
        }

	    private static BlockReference InsertReference(BlockImporterOption opt, ObjectId id)
		{
			return opt.Transaction == null ? UseInternalTransaction(opt, id) : UseExternalTransaction(opt, id);
		}

		private static BlockReference UseExternalTransaction(BlockImporterOption opt, ObjectId id)
		{
			return InsertReference(opt, opt.Transaction, id);
		}

		private static BlockReference UseInternalTransaction(BlockImporterOption opt, ObjectId id)
		{
			using (Transaction tr = DBRoutine.StartTransaction())
			{
				try
				{
					BlockReference br = InsertReference(opt, tr, id);
					tr.Commit();
					return br;
				}
				catch
				{
					tr.Abort();
					throw;
				}
			}
		}

		private static BlockReference InsertReference(BlockImporterOption opt, Transaction transaction, ObjectId id)
		{
			Database db = DBRoutine.WorkingDatabase;
			BlockReference br = new BlockReference(opt.Origin, id);
			BlockTableRecord btr = (BlockTableRecord)transaction.GetObject(id, OpenMode.ForRead);

			if (string.IsNullOrEmpty(opt.LayoutName))
			{
				BlockTable bt = (BlockTable)transaction.GetObject(db.BlockTableId, OpenMode.ForRead, false);
				BlockTableRecord space;
				if (SystemVariable.GetSystemVariable<short>(SystemVariable.TileMode) == 0)
				{
					space = transaction.GetObject(bt[BlockTableRecord.PaperSpace], OpenMode.ForWrite, false) as BlockTableRecord;
				}
				else
				{
					space = transaction.GetObject(bt[BlockTableRecord.ModelSpace], OpenMode.ForWrite, false) as BlockTableRecord;
				}
				// Block needs to be inserted to current space before being able to append attribute to it
				space.AppendEntity(br);

			}
			else
			{
				// Менеджер лайоутов
				LayoutManager lm = LayoutManager.Current;
				// ID лайоута по имени
				ObjectId layId = lm.GetLayoutId(opt.LayoutName);

				// Объект Layout на чтение
				Layout layout = transaction.GetObject(layId, OpenMode.ForRead) as Layout;
				if (layout != null)
				{
					// ID таблицы блоков для лайоута
					ObjectId btrId = layout.BlockTableRecordId;
					// Запись таблицы блоков, описывающая пространство лайоута
					BlockTableRecord lbtr = (BlockTableRecord)transaction.GetObject(btrId, OpenMode.ForWrite, false);
					// Добавляем объект
					lbtr.AppendEntity(br);
				}
			}

			if (!String.IsNullOrEmpty(opt.Layer))
			{
				if (!LayersRoutine.Exists(opt.Layer, false))
					throw new Exception("Не найден слой " + opt.Layer);

				br.Layer = opt.Layer;
			}

			transaction.AddNewlyCreatedDBObject(br, true);
			br.TransformBy(opt.Transformation);

			// Ишутин 06.10.10:
			// Отсюда перенесена строка "transaction.AddNewlyCreatedDBObject(br, true);"
			// т.к. если "br.TransformBy(opt.Transformation);" выдаст exception,
			// то это повредит автокаду

			// Обновим атрибуты
			Dictionary<ObjectId, AttributeInfo> attInfo = new Dictionary<ObjectId, AttributeInfo>();
			SetAttributeToReference(btr, transaction, br, attInfo);

			// Внесем параметры
			SetPropertyToReference(br, opt.Properies.RawTable);

            SomeDarkMagic(br, opt.Attributes, transaction);


			// Джиг
			if (opt.UseJig && String.IsNullOrEmpty(opt.LayoutName))
			{
				BlockJig myJig = new BlockJig(transaction, br, attInfo);
				PromptStatus stat = myJig.Run();
				if (stat == PromptStatus.Cancel)
					throw new Exception("Произведена отмена вставки блока");
				if (stat != PromptStatus.OK)
					throw new Exception("Ошибка при запуске джиггера для блока " + opt.BlockName);
			}

			return br;
		}

        /// <summary>
        /// Вставка блока с указанными настройками
        /// </summary>
        /// <param name="opt">Параметры вставки настройки.</param>
        /// <returns>Результат вставки блока</returns>
		public static BlockImporterResult InsertBlock(BlockImporterOption opt)
		{
			BlockImporterResult bir = new BlockImporterResult();
			try
			{
				if (opt == null)
					throw new Exception("Не задана ссылка на объект BlockImporterOption");

				if (string.IsNullOrEmpty(opt.BlockName))
					throw new Exception("Не задано имя блока для вставки");

				// Прверим наличие нашего блока в текущей базе данных AutoCAD
				ObjectId blockId = BlockRoutine.GetBlockId(opt.BlockName);

				if (!DBRoutine.CheckId(blockId))
				{
					// Его нет, но нам указали где искать
					if (!string.IsNullOrEmpty(opt.FileName))
					{
						if (!BlockRoutine.CopyToHostDatabase(opt.FileName, opt.BlockName))
							throw new Exception("Блок " + opt.BlockName + " отсутствует в файле " + opt.FileName + " или не может быть вставлен");

						blockId = BlockRoutine.GetBlockId(opt.BlockName);


						if (blockId == ObjectId.Null)
							throw new Exception("Блок " + opt.BlockName + " отсутствует в базе");
					}
					else
						throw new Exception("Блок " + opt.BlockName + " отсутствует в базе");
				}
				// После всех ухищрений, считаем что у нас есть Id блока из нашей базы
				BlockReference br = InsertReference(opt, blockId);
				bir.BlockReference = br;
				bir.Id = br.ObjectId;
				bir.IsValid = true;

			}
			catch (Exception e)
			{
				bir.ErrorStatus = e.Message;
				return bir;
			}
			catch
			{
				bir.ErrorStatus = "Unhandled exception";
				return bir;
			}
			return bir;
		}


	}

}
