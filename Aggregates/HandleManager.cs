// Copyright (c) 2012 Сергей Попов, Виталий Сайдин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Sergey Popov, Vitaliy Saydin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System.Collections;
using System.Collections.Generic;
using System.IO;
using Autodesk.AutoCAD.DatabaseServices;
using Celsio.Serializers.AbnormalSerializer;
using MAcadFramework.Routines;

namespace MAcadFramework.Aggregates
{
	/// <summary>
	/// Класс для групповой обработки дескрипторов AutoCAD
	/// </summary>
	/// <remarks>
	/// Упрощает работу с группой дескрипторов AutoCAD, отсеевая дубликаты и не действительные дескрипторы
	/// <para>Под действительными дескрипторами понимаются неудаленные из базы AutoCAD.</para>
	/// </remarks>
	[AbnormalSerialize]
	public class HandleManager : IEnumerable
	{
		[SerializeDeclarate]
		private List<Handle> _handleList = new List<Handle>(5);

		private List<Handle> ThinOutHanleList()
		{
			List<Handle> tempList = new List<Handle>(5);
			foreach (Handle h in _handleList)
			{
				if (DBRoutine.CheckHandle(h)) { tempList.Add(h); }
			}
			return tempList;
		}

        ///<summary>
        /// Получить массив байтов
        ///</summary>
        ///<returns>Массив байтов</returns>
        public byte[] ToByteArray()
        {
            BinaryAbnormalSerializer serializer = new BinaryAbnormalSerializer(GetType());
            using (MemoryStream ms = new MemoryStream())
            {
                serializer.Serialize(ms, this);
                byte[] array = ms.ToArray();
                return array;
            }
        }

        ///<summary>
        /// Получить массив байтов
        ///</summary>
        ///<returns>Массив байтов</returns>
        public void FromByteArray(byte[] array)
        {
            BinaryAbnormalSerializer serializer = new BinaryAbnormalSerializer(GetType());
            using (Stream ms = new MemoryStream(array))
            {
                serializer.Deserialize(ms, this);
            }
        }

		/// <summary>
		/// Проверяет наличие указанного дескриптора в менеджере
		/// </summary>
		/// <param name="handle">Дескриптор объекта AutoCAD</param>
		/// <returns> <c>true</c> если дескриптор присутствует, иначе <c>false</c> </returns>
		public bool Exists(Handle handle)
		{
			foreach (Handle h in ValidHandles)
			{
				if (h.Equals(handle))
					return true;
			}
			return false;
		}

		/// <summary>
		/// Удалить из менеджера не действительные дескрипторы и дубликаты
		/// </summary>
		public void Refresh()
		{ 
			_handleList = ThinOutHanleList();
            for (int i = 0; i < _handleList.Count; i++)
            {
                for (int j = 0; j < _handleList.Count; j++)
                {
                    if (i == j) continue;
                    if (_handleList[i].Value == _handleList[j].Value)
                    {
                        _handleList.RemoveAt(j);
                        j--;
                    }
                }
            }
		}

		/// <summary>
		/// Получить количество дескрипторов в менеджере.
		/// </summary>
		/// <value>Количество дескрипторов в менеджере</value>
        public int Count 
		{
			get { return _handleList.Count; }
		}

		/// <summary>
		/// Получить первый объект в списке
		/// </summary>
		/// <value>Первый объект в списке иначе default(Handle)</value>
		public Handle SingleObject
		{
			get
			{
				if (_handleList.Count > 0)
					return _handleList[0];
				return default(Handle);
			}
		}

		/// <summary>
		/// Получить список дескрипторов
		/// </summary>
		/// <value>Cписок дескрипторов</value>
		public List<Handle> Handles
		{
			get { return _handleList; }
		}

		/// <summary>
		/// Получить список действительных дескрипторов без изменения коллекции
		/// </summary>
		/// <value>Cписок действительных дескрипторов</value>
		public List<Handle> ValidHandles
		{
			get { return ThinOutHanleList(); }
		}

		/// <summary>
		/// Проверяет актуальность менеджера на предмет содержания дескрипторов
		/// </summary>
		/// <value><c>true</c> если менеджер содержит дескрипторы, иначе <c>false</c>.
		/// </value>
        public bool IsValid
        {
            get {  return ThinOutHanleList().Count > 0; }
        }

		/// <summary>
		/// Добавить дескриптор в менеджер
		/// </summary>
		/// <param name="handle">Дескриптор объекта AutoCAD</param>
		public void Add(Handle handle)
		{
			_handleList.Add(handle);
		}

		/// <summary>
		/// Добавить список дескрипторов в менеджер
		/// </summary>
		/// <param name="list">Список дескрипторов.</param>
		public void Add(IEnumerable<Handle> list)
		{
			_handleList.AddRange(list);
		}

		/// <summary>
		/// Добавить дескрипторы из менеджера
		/// </summary>
		/// <param name="hm">Менеджер дескрипторов</param>
		public void Add(HandleManager hm)
		{
			_handleList.AddRange(hm.ValidHandles);
		}

		/// <summary>
		/// Удалить все объекты из базы, на которые есть ссылки в менеджере
		/// </summary>
		/// <remarks>
		/// Метод использует внутреннюю транзакцию AutoCAD для удаление объектов,
		/// дескрипторы тоже удаляются.
		/// </remarks>
		public void Clear()
		{
			Refresh();
			using (EditorRoutine.LockDoc())
			{
				DBRoutine.EraseObjects(ValidHandles);
			}
			_handleList.Clear();
		}

		/// <summary>
		// Удалить все объекты из базы, на которые есть ссылки в менеджере
		/// </summary>
		/// <param name="trans">Tранзакция AutoCAD</param>
		/// <remarks>
		/// Метод использует внешнюю транзакцию AutoCAD для удаление объектов,
		/// дескрипторы тоже удаляются.
		/// </remarks>
		public void Clear(Transaction trans)
		{
			Refresh();
			using (EditorRoutine.LockDoc())
			{
				DBRoutine.EraseObjects(ValidHandles, trans);
			}
			_handleList.Clear();
		}

		#region IEnumerable Members

		/// <summary>
		/// Returns an enumerator that iterates through a collection.
		/// </summary>
		/// <returns>
		/// An <see cref="T:System.Collections.IEnumerator"/> object that can be used to iterate through the collection.
		/// </returns>
		public IEnumerator GetEnumerator() { return _handleList.GetEnumerator(); }

		#endregion
	}
}
