// Copyright (c) 2012 Сергей Попов, Виталий Сайдин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Sergey Popov, Vitaliy Saydin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections;
using System.Collections.Generic;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using MAcadFramework.Routines;

namespace MAcadFramework.Aggregates
{
	/// <summary>
	/// Класс для облегчения работы с освобождением неуправляемых ресурсов
	/// </summary>
	/// <remarks>
	/// <para>
	/// Класс для регистрации объектов которые не добавляются в базу данных Акада
	/// и поэтому требуют Dispose в обязательном порядке. Для очистки не добавленных в таблицу объектов
	/// можно вызывать статический метод <see cref="SafelyCreater.Dispose(DBObject)"/> и его производные
	/// </para>
	/// </remarks>
	/// <example>
	/// В этом примере показано как взорвать блок, добавить в пространство модели только солиды,
	/// остальные объекты очистить с использоывнием <see cref="SafelyCreater"/>
	/// <code><![CDATA[
	/// try
	/// {
	///		// получаем ссылку на блок
	///		BlockReference block = EntityProvider.GetObjectPointer<BlockReference>(handle);
	///		// Создаем экземпляр сборщика
	///		using (SafelyCreater sc = new SafelyCreater())
	///		{
	///			DBObjectCollection col = new DBObjectCollection();
	///			
	///			// Взрываем блок и получаем его содержимое
	///			block.Explode(col);
	///			
	///			// Регистрируем полученную коллекцию
	///			sc.Push(col);
	///			using (Transaction transaction = DBRoutine.StartTransaction())
	///			{
	///				foreach (DBObject obj in col)
	///				{
	///					if (obj is Solid3d)
	///					{
	///						// Выталкиваем объект на пространство модели
	///						sc.PopObjectToModelSpace(obj, transaction);
	///					}
	///				}
	///			}
	///		}
	///		// Здесь все объекты не добавленные в базу будут очишенны
	///	}
	/// catch (Exception ex)
	/// {
	///		// Выводим сообщение об ошибке
	/// }
	/// ]]></code>
	/// </example>
	public class SafelyCreater : IDisposable
	{
		private readonly List<DBObject> _objectTable = new List<DBObject>(10);

		#region Создание объектов с занесением в список

		/// <summary>
		/// Создать окружность с занесением ее в таблицу очистки
		/// </summary>
		/// <returns>Временная созданная окружность</returns>
		public Circle CreateCircle()
		{
			Circle temp = new Circle();
			_objectTable.Add(temp);
			return temp;
		}
		/// <summary>
		/// Создать окружность с занесением ее в таблицу очистки
		/// </summary>
		/// <param name="center">Центр окружности.</param>
		/// <param name="normal">Нормаль к плоскости в которой располагается окружность.</param>
		/// <param name="radius">Радиус окружности.</param>
		/// <returns>Временная созданная окружность</returns>
		public Circle CreateCircle(Point3d center, Vector3d normal, double radius)
		{
			Circle temp = new Circle(center, normal, radius);
			_objectTable.Add(temp);
			return temp;
		}

		/// <summary>
		/// Создание линии с занисением в таблицу очистки
		/// </summary>
		/// <returns>Временная созданная линия</returns>
		public Line CreateLine()
		{
			Line temp = new Line();
			_objectTable.Add(temp);
			return temp;
		}
		/// <summary>
		/// Создание линии с занисением в таблицу очистки
		/// </summary>
		/// <param name="startPoint">Начальная точка линии.</param>
		/// <param name="endPoint">Конечная точка линии.</param>
		/// <returns>Временная созданная линия</returns>
		public Line CreateLine(Point3d startPoint, Point3d endPoint)
		{
			Line temp = new Line(startPoint, endPoint);
			_objectTable.Add(temp);
			return temp;
		}

		#endregion

        /// <summary>
        /// Помеcтить объект в таблицу очистки.
        /// </summary>
        /// <param name="obj">Временный объект.</param>
		public void Push(DBObject obj)
		{
			if (obj != null) _objectTable.Add(obj);
		}

        /// <summary>
		/// Помеcтить указанные объекты в таблицу очистки.
        /// </summary>
        /// <param name="objs">Массив временных объектов.</param>
		public void Push(params DBObject[] objs)
		{
			foreach (DBObject obj in objs)
				if (obj != null) _objectTable.Add(obj);
		}

        /// <summary>
		/// Помеcтить указанные объекты в таблицу очистки.
        /// </summary>
        /// <param name="objs">Коллекция временных объектов.</param>
		public void Push(DBObjectCollection objs)
		{
			if (objs == null) return;
			foreach (DBObject obj in objs)
				_objectTable.Add(obj);
		}

        /// <summary>
		/// Помеcтить указанные объекты в таблицу очистки.
        /// </summary>
		/// <param name="objs">Коллекция временных объектов.</param>
		public void Push(ICollection objs)
		{
			if (objs == null) return;
			foreach (object obj in objs)
				if (obj is DBObject) _objectTable.Add(obj as DBObject);
		}

		/// <summary>
		/// Убрать объект из списка очищаемых и добавить в пространство модели.
		/// </summary>
		/// <param name="obj">Объект для вставки</param>
		/// <returns>Идентификатор вставленного объекта</returns>
		/// <remarks>
		/// Если объект не присутствует в списке, то он будет просто добавлен в пространство
		/// </remarks>
		public ObjectId PopObjectToModelSpace(DBObject obj)
		{
			if (_objectTable.Contains(obj))
				_objectTable.Remove(obj);
			if (obj is Entity)
				return DBRoutine.AddToModelSpace(obj as Entity);
			return ObjectId.Null;
		}

		/// <summary>
		/// Убрать объект из списка очищаемых и добавить в пространство модели.
		/// </summary>
		/// <param name="obj">Объект для вставки</param>
		/// <param name="trans">Текущая транзакция AutoCAD</param>
		/// <returns>Идентификатор вставленного объекта</returns>
		/// <remarks>
		/// Если объект не присутствует в списке, то он будет просто добавлен в пространство
		/// </remarks>
		public ObjectId PopObjectToModelSpace(DBObject obj, Transaction trans)
		{
			if (_objectTable.Contains(obj))
				_objectTable.Remove(obj);
			if (obj is Entity)
				return DBRoutine.AddToModelSpace(obj as Entity, trans);
			return ObjectId.Null;
		}

		/// <summary>
		/// Убрать объект из списка очищаемых и добавить в пространство чертежа.
		/// </summary>
		/// <param name="obj">Объект для вставки</param>
		/// <returns>Идентификатор вставленного объекта</returns>
		/// <remarks>
		/// Если объект не присутствует в списке, то он будет просто добавлен в пространство
		/// </remarks>
		public ObjectId PopObjectToPaperSpace(DBObject obj)
		{
			if (_objectTable.Contains(obj))
				_objectTable.Remove(obj);
			if (obj is Entity)
				return DBRoutine.AddToPaperSpace(obj as Entity);
			return ObjectId.Null;
		}

		/// <summary>
		/// Убрать объект из списка очищаемых и добавить в пространство чертежа.
		/// </summary>
		/// <param name="obj">Объект для вставки</param>
		/// <param name="trans">Текущая транзакция AutoCAD</param>
		/// <returns>  Идентификатор вставленного объекта  </returns>
		/// <remarks>
		/// Если объект не присутствует в списке, то он будет просто добавлен в пространство
		/// </remarks>
		public ObjectId PopObjectToPaperSpace(DBObject obj, Transaction trans)
		{
			if (_objectTable.Contains(obj))
				_objectTable.Remove(obj);
			if (obj is Entity)
				return DBRoutine.AddToPaperSpace(obj as Entity, trans);
			return ObjectId.Null;
		}

		/// <summary>
		/// Убрать объект из списка очищаемых и добавить в текущее пространство.
		/// </summary>
		/// <param name="obj">Объект для вставки</param>
		/// <returns> Идентификатор вставленного объекта  </returns>
		/// <remarks>
		/// Если объект не присутствует в списке, то он будет просто добавлен в пространство
		/// </remarks>
		public ObjectId PopObjectToCurrentSpace(DBObject obj)
		{
			if (_objectTable.Contains(obj))
				_objectTable.Remove(obj);
			if (obj is Entity)
				return DBRoutine.AddToCurrentSpace(obj as Entity);
			return ObjectId.Null;
		}

		/// <summary>
		/// Убрать объект из списка очищаемых и добавить в текущее пространство.
		/// </summary>
		/// <param name="obj">Объект для вставки</param>
		/// <param name="trans">Текущая транзакция AutoCAD</param>
		/// <returns>  Идентификатор вставленного объекта  </returns>
		/// <remarks>
		/// Если объект не присутствует в списке, то он будет просто добавлен в пространство
		/// </remarks>
		public ObjectId PopObjectToCurrentSpace(DBObject obj, Transaction trans)
		{
			if (_objectTable.Contains(obj))
				_objectTable.Remove(obj);
			if (obj is Entity)
				return DBRoutine.AddToCurrentSpace(obj as Entity, trans);
			return ObjectId.Null;
		}

        /// <summary>
        /// Убрать объект из списка очищаемых.
        /// </summary>
        /// <param name="obj">Объект для удаления из списка.</param>
		public void Remove(DBObject obj)
		{
			if (_objectTable.Contains(obj))
				_objectTable.Remove(obj);
		}

		/// <summary>
		/// Вызвать Dispose для всех зарегистрированных объектов
		/// </summary>
		public void Collect()
		{
			foreach (DBObject obj in _objectTable)
				if (obj != null && !obj.IsDisposed) obj.Dispose();
			_objectTable.Clear();
		}

		/// <summary>
		/// Вызвать освобождение ресурсов у указанного объекта
		/// </summary>
		/// <param name="obj">Объект для освобождения ресурсов</param>
		/// <remarks>
		/// Этот метод безопасние прямого вызова Dispose, так как выполняет все необходимые проверки,
		/// и только в случае успеха вызывает освобождение ресурсов
		/// </remarks>
        public static void Dispose(DBObject obj)
        {
            if (obj != null && !obj.IsDisposed && obj.IsNewObject && !obj.ObjectId.IsValid)
                obj.Dispose();
        }

		/// <summary>
		/// Вызвать освобождение ресурсов у указанного объекта
		/// </summary>
		/// <param name="obj">Объект для освобождения ресурсов</param>
		/// <remarks>
		/// Этот метод безопасние прямого вызова Dispose, так как выполняет все необходимые проверки,
		/// и только в случае успеха вызывает освобождение ресурсов
		/// </remarks>
        public static void Dispose(IDisposable obj)
        {
            if (obj != null)
                obj.Dispose();
        }

		/// <summary>
		/// Вызвать освобождение ресурсов у указанного списка объектов
		/// </summary>
		/// <param name="objs">Спискок объектов для освобождения ресурсов</param>
		/// <remarks>
		/// Этот метод безопасние прямого вызова Dispose, так как выполняет все необходимые проверки,
		/// и только в случае успеха вызывает освобождение ресурсов
		/// </remarks>
        public static void Dispose(IList objs)
        {
            if (objs == null) return;
            foreach (object obj in objs)
            {
				if (obj is DBObject)
				{
					DBObject dbObj = obj as DBObject;
					if (dbObj.IsDisposed || !dbObj.IsNewObject || dbObj.ObjectId.IsValid) continue;
					dbObj.Dispose();
				}
				if (obj is IDisposable)
				{
					IDisposable dbObj = obj as IDisposable;
					dbObj.Dispose();
				}
            }
        }

		/// <summary>
		/// Вызвать освобождение ресурсов у указанного списка объектов
		/// </summary>
		/// <param name="objs">Спискок объектов для освобождения ресурсов</param>
		/// <remarks>
		/// Этот метод безопасние прямого вызова Dispose, так как выполняет все необходимые проверки,
		/// и только в случае успеха вызывает освобождение ресурсов
		/// </remarks>
        public static void Dispose(DBObjectCollection objs)
        {
            if (objs == null) return;
            foreach (DBObject obj in objs)
            {
                if (obj == null || obj.IsDisposed || !obj.IsNewObject || obj.ObjectId.IsValid)
                    continue;
                obj.Dispose();
            }
        }
		#region IDisposable Members

		public void Dispose()
		{
			Collect();
		}

		#endregion
	}
}
