// Copyright (c) 2012 Сергей Попов, Виталий Сайдин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Sergey Popov, Vitaliy Saydin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections.Generic;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;

namespace MAcadFramework.Aggregates
{
	/// <summary>
	/// Настройки вставки блока в чертеж
	/// </summary>
	public sealed class BlockImporterOption
	{

		#region Статические инициализаторы класса
		/// <summary>
		/// Вставить блок с использование джига из указанного файла
		/// </summary>
		/// <param name="blockName">Имя блока.</param>
		/// <param name="fileName">Имя файла</param>
		/// <returns>Опции для вставки</returns>
		public static BlockImporterOption CreateOptionsWithJig(string blockName, string fileName)
		{
			BlockImporterOption bio = new BlockImporterOption(blockName, fileName);
			bio.UseJig = true;
			return bio;
		}

		/// <summary>
		/// Вставить блок с использование джига в из нашей базы
		/// </summary>
		/// <param name="blockName">Имя блока.</param>
		/// <returns>Опции для вставки</returns>
		public static BlockImporterOption CreateOptionsWithJig(string blockName)
		{
			return CreateOptionsWithJig(blockName, string.Empty);
		}


		/// <summary>
		/// Вставить блок с указанием размещения из указанного файла
		/// </summary>
		/// <param name="blockName">Имя блока</param>
		/// <param name="fileName">Имя файла</param>
		/// <param name="pt">Точка вставки блока</param>
		/// <param name="mt">Матрица трансформации</param>
		/// <returns>Опции для вставки</returns>
		public static BlockImporterOption CreateOptionsDirectlyInsert(string blockName, string fileName, Point3d pt, Matrix3d mt)
		{
			BlockImporterOption bio = new BlockImporterOption(blockName, fileName);
			bio.UseJig = false;
			bio.Transformation = mt;
			bio.Origin = pt;
			return bio;
		}

		/// <summary>
		/// Вставить блок с указанием размещения из указанного файла
		/// </summary>
		/// <param name="blockName">Имя блока</param>
		/// <param name="fileName">Имя файла</param>
		/// <param name="mt">Матрица трансформации</param>
		/// <returns>Опции для вставки</returns>
		public static BlockImporterOption CreateOptionsDirectlyInsert(string blockName, string fileName, Matrix3d mt)
		{
			BlockImporterOption bio = new BlockImporterOption(blockName, fileName);
			bio.UseJig = false;
			bio.Transformation = mt;
			bio.Origin = Point3d.Origin;
			return bio;
		}

		/// <summary>
		/// Вставить блок с указанием размещения из указанного файла
		/// </summary>
		/// <param name="blockName">Имя блока</param>
		/// <param name="fileName">Имя файла</param>
		/// <param name="pt">Точка вставки блока</param>
		/// <returns>Опции для вставки</returns>
		public static BlockImporterOption CreateOptionsDirectlyInsert(string blockName, string fileName, Point3d pt)
		{
			return CreateOptionsDirectlyInsert(blockName, fileName, pt, Matrix3d.Identity);
		}


		/// <summary>
		/// Вставить блок с указанием размещения в из нашей базы
		/// </summary>
		/// <param name="blockName">Имя блока</param>
		/// <param name="pt">Точка вставки блока</param>
		/// <returns>Опции для вставки</returns>
		public static BlockImporterOption CreateOptionsDirectlyInsert(string blockName, Point3d pt)
		{
			return CreateOptionsDirectlyInsert(blockName, string.Empty, pt);
		}

		#endregion Статические инициализаторы класса

		/// <summary>
		/// Слой на который вставлять блок
		/// </summary>
		private string _layer = string.Empty;

		/// <summary>
		/// Пространство для вставки блока
		/// </summary>
		private string _layoutName = string.Empty;


		/// <summary>
		/// Свойства блока
		/// </summary>
		private readonly BlockProperties _properies = new BlockProperties();

        /// <summary>
        /// Атрибуты блока
        /// </summary>
        private readonly BlockAttributes _attributes = new BlockAttributes();
		
		/// <summary>
		/// Из какого файла вcтавляем
		/// </summary>
		private string _fileName = string.Empty;

		/// <summary>
		/// Имя блока для вставки
		/// </summary>
		private string _blockName = string.Empty;

		/// <summary>
		/// Перезаписывать ли определение блока при вставки из другого файла
		/// </summary>
		private bool _forceOverrideBlock;

		/// <summary>
		/// Использовать ли Jig при вставке, игнорирует AddToDatabase = false
		/// </summary>
		private bool _useJig;

		/// <summary>
		/// Добавлять ли в базу данных, игнорирует UseJig = true
		/// </summary>
		private bool _addToDatabase;

		/// <summary>
		/// Добавлять ли в базу данных, игнорирует UseJig = true
		/// </summary>
		private IntPtr _handle;

		/// <summary>
		/// Точка вставки блока
		/// </summary>
		private Point3d _origin = Point3d.Origin;

		/// <summary>
		/// Начальная трансформация блока
		/// </summary>
		private Matrix3d _transformation = Matrix3d.Identity;

		private Transaction _transaction;

        /// <summary>
        /// Инициализирует новый экземпляр класса BlockImporterOption.
        /// </summary>
        /// <param name="blockName">Название блока.</param>
		public BlockImporterOption(string blockName)
		{
			_blockName = blockName;
		}

        /// <summary>
        /// Инициализирует новый экземпляр класса BlockImporterOption.
        /// </summary>
        /// <param name="blockName">Название блока.</param>
        /// <param name="fileName">Название файла.</param>
		public BlockImporterOption(string blockName, string fileName)
		{
			_blockName = blockName;
			_fileName = fileName;
		}

		/// <summary>
		/// Получить или задать имя блока для вставки
		/// </summary>
		/// <value>BlockName</value>
		public string BlockName
		{
			get { return _blockName; }
			set { _blockName = value; }
		}

		/// <summary>
		/// Получить или задать флаг использования Jig
		/// </summary>
		/// <value>Флаг использования Jig</value>
		public bool UseJig
		{
			get { return _useJig; }
			set { _useJig = value; }
		}

		/// <summary>
		/// Получить или задать точку вставки блока
		/// </summary>
		/// <value>Точка вставки блока</value>
		public Point3d Origin
		{
			get { return _origin; }
			set { _origin = value; }
		}

		/// <summary>
		/// Получить или задать начальную трансформацию блока
		/// </summary>
		/// <value>Начальная трансформация блока</value>
		public Matrix3d Transformation
		{
			get { return _transformation; }
			set { _transformation = value; }
		}

		/// <summary>
		/// Получить или задать флаг занесения блока в базу (Игнорируется)
		/// </summary>
		/// <value>Флаг занесения блока в базу</value>
		/// <remarks>Этот параметр не используется в данный момент.</remarks>
		public bool AddToDatabase
		{
			get { return _addToDatabase; }
			set { _addToDatabase = value; }
		}

		/// <summary>
		/// Из какого файла вcтавляем
		/// </summary>
		/// <value> Имя файла в котором находится блок. </value>
		public string FileName
		{
			get { return _fileName; }
			set { _fileName = value; }
		}

		/// <summary>
		/// Получить или задать флаг перезаписи блока в базе (Игнорируется)
		/// </summary>
		/// <value>Флаг перезаписи блока в базе</value>
		/// <remarks>Этот параметр не используется в данный момент.</remarks>
		public bool ForceOverrideBlock
		{
			get { return _forceOverrideBlock; }
			set { _forceOverrideBlock = value; }
		}

        /// <summary>
        /// Получить или задать параметры блока динамического блока (Properties)
        /// </summary>
        /// <value>Параметры блока</value>
		/// <remarks>
		/// При заполнении словаря соблюдайте осторожность с типами параметров. AutoCAD очень капризный.
		/// Для простоты воспользуйтесь свойством <see cref="Properies"/>, в котором учтено большенство операций.
		/// </remarks>
		public Dictionary<string, object> BlockParams
		{
			get { return Properies.RawTable; }
			set { Properies.RawTable = value; }
		}

		/// <summary>
		/// Получить или задать слой для вставки блока
		/// </summary>
		/// <value>Слой для вставки блока</value>
		public string Layer
		{
			get { return _layer; }
			set { _layer = value; }
		}

		/// <summary>
		/// Получить или задать Транзакцию для использования
		/// </summary>
		/// <value>Открытая транзакция AutoCAD</value>
		public Transaction Transaction
		{
			get { return _transaction; }
			set { _transaction = value; }
		}

		

        /// <summary>
        /// Получить или задать имя лайоута для вставки.
        /// </summary>
		/// <value>Имя лайоута для вставки</value>
		public string LayoutName
		{
			get { return _layoutName; }
			set { _layoutName = value; }
		}

		/// <summary>
		/// Доступ к свойствам динамического блока
		/// </summary>
		/// <value>Свойства динамического блока</value>
		/// <remarks>
		/// Для получения подробной информации смотрите <see cref="BlockProperties"/>
		/// </remarks>
		public BlockProperties Properies
		{
			get { return _properies; }
		}

		/// <summary>
		///ДОступ к атрибутам блока
		/// </summary>
		/// <value>Атрибуты блока</value>
		/// <remarks>
		/// Словарь атрибутов блока состоит из имени атрибута (tag) и занчение атрибута (value)
		/// </remarks>
        public BlockAttributes Attributes
		{
			get { return _attributes; }
		}
	}
}
