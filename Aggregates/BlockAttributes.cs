// Copyright (c) 2012 Сергей Попов, Виталий Сайдин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Sergey Popov, Vitaliy Saydin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections.Generic;
using Celsio.Data;

namespace MAcadFramework.Aggregates
{
    /// <summary>
    /// Класс создающий словарь атрибутов динамического блока
    /// </summary>
    public class BlockAttributes
    {
        private const string AttributeTextName = "TextString";
        private const string AttributeVisibilityName = "Visible";
        private readonly Dictionary<string, Dictionary<string, object>> _attrExtendPropertyMap = new Dictionary<string, Dictionary<string, object>>();

        /// <summary>
        /// Проверяет наличие инструкций для указаного атрибута
        /// </summary>
        /// <param name="tag">Имя атрибута</param>
        public bool Contains(string tag)
        {
            return _attrExtendPropertyMap.ContainsKey(tag);
        }

        /// <summary>
        /// Получить или задать значение атрибута по имени
        /// </summary>
        /// <param name="tag">Имя атрибута</param>
        public string this[string tag]
        {
            get
            {
                if (_attrExtendPropertyMap.ContainsKey(tag))
                {
                    Dictionary<string, object> extendProperty = _attrExtendPropertyMap[tag];
                    if (extendProperty.ContainsKey(AttributeTextName))
                    {
                        string data = extendProperty[AttributeTextName] as string;
                        return !string.IsNullOrEmpty(data) ? data : string.Empty;
                    }
                }
                return string.Empty;
            }
            set { SetExtendedProperty(tag, AttributeTextName, value); }
        }

        /// <summary>
        /// Получить рассширенные свойства атрибута
        /// </summary>
        /// <param name="tag">Имя атрибута</param>
        /// <returns>Расширенные свойства</returns>
        /// <remarks>Расширенные свойства будут с помощью рефлексии установлены в соответствующее свойство AttributeReference. 
        /// Пара состоит из имени свойства и значения. Контроль типов на этапе установки не проводится!</remarks>
        public Dictionary<string, object> GetExtendedProperty(string tag)
        {
            return _attrExtendPropertyMap[tag];
        }

        /// <summary>
        /// Задать рассширенные свойства атрибута
        /// </summary>
        /// <param name="tag">Имя атрибута</param>
        /// <param name="extendedProperty">Расширенные свойства</param>
        /// <remarks>Расширенные свойства будут с помощью рефлексии установлены в соответствующее свойство AttributeReference. 
        /// Пара состоит из имени свойства и значения. Контроль типов на этапе установки не проводится!</remarks>
        public void SetExtendedProperty(string tag, Pair<string, object>  extendedProperty)
        {
            if (extendedProperty == null) throw new ArgumentNullException("extendedProperty");
            SetExtendedProperty(tag, extendedProperty.First, extendedProperty.Second);
        }

        /// <summary>
        /// Задать рассширенные свойства атрибута
        /// </summary>
        /// <param name="tag">Имя атрибута</param>
        /// <param name="propName">Имя свойства</param>
        /// <param name="value">Значение свойства</param>
        /// <remarks>Расширенные свойства будут с помощью рефлексии установлены в соответствующее свойство AttributeReference. 
        /// Пара состоит из имени свойства и значения. Контроль типов на этапе установки не проводится!</remarks>
        public void SetExtendedProperty(string tag, string propName, object value)
        {
            if (!_attrExtendPropertyMap.ContainsKey(tag))
                _attrExtendPropertyMap[tag] = new Dictionary<string, object>();
            _attrExtendPropertyMap[tag][propName] = value;

        }

        /// <summary>
        /// Установить видимость атрибута
        /// </summary>
        /// <param name="tag">Имя атрибута</param>
        /// <param name="state">Флаг видимости атрибута</param>
        public void SetAttributeVisibility(string tag, bool state)
        {
            SetExtendedProperty(tag, AttributeVisibilityName, state);
        }

        /// <summary>
        /// Установить видимость атрибута
        /// </summary>
        /// <param name="tag">Имя атрибута</param>
        /// <param name="text">Текст атрибута</param>
        public void SetAttributeText(string tag, string text)
        {
            SetExtendedProperty(tag, AttributeTextName, text);
        }
    }
}
