// Copyright (c) 2012 Сергей Попов, Виталий Сайдин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Sergey Popov, Vitaliy Saydin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections.Generic;

namespace MAcadFramework.Aggregates
{
	/// <summary>
	/// Класс создающий словарь свойств динамического блока
	/// </summary>
	public class BlockProperties
	{
		private Dictionary<string, object> _rawTable = new Dictionary<string, object>();

		/// <summary>
		/// Получить или задать словарь свойств динамического блока
		/// </summary>
		/// <value> Словарь свойств динамического блока. </value>
		/// <remarks>
		/// При установки нового словаря, старый будет полностью переписан
		/// </remarks>
		public Dictionary<string, object> RawTable
		{
			get { return _rawTable; }
			set { _rawTable = value; }
		}

		/// <summary>
		/// Задать линейное свойство динамического блока.
		/// </summary>
		/// <param name="name">Имя свойства.</param>
		/// <param name="value">Значение свойства.</param>
		public void SetDimensionParam(string name, double value)
		{
			_rawTable[name] = value;
		}

		/// <summary>
		/// Задать переключатель отображения бока относительно оси (Flip).
		/// </summary>
		/// <param name="name">Имя свойства.</param>
		/// <param name="flip">Значение переключателя.</param>
		/// <remarks>
		/// Свойство Flip принимает в себя <see cref="short"/>, булево значение задано для удобства,
		/// и будет приведено к <see cref="short"/>.
		/// </remarks>
		public void SetFlipParam(string name, bool flip)
		{
			short value = Convert.ToInt16(flip);
			_rawTable[name] = value;
		}

		/// <summary>
		/// Задать визуальное отображение бока (Visibility).
		/// </summary>
		/// <param name="value">Название визуального состояния.</param>
		public void SetVisibleParam(string value)
		{
		    _rawTable["Visibility"] = value;
		}
	}
}
