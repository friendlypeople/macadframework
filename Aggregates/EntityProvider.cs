// Copyright (c) 2012 Сергей Попов, Виталий Сайдин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Sergey Popov, Vitaliy Saydin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using Autodesk.AutoCAD.DatabaseServices;
using MAcadFramework.Adapters;
using MAcadFramework.Routines;

namespace MAcadFramework.Aggregates
{
	/// <summary>
	/// Класс помошник для работы с примитивами
	/// </summary>
	public static class EntityProvider
	{
		/// <summary>
		/// Клонирование объекта с приведением типа
		/// </summary>
		/// <typeparam name="TYPE">Тип к которому приводиться объект</typeparam>
		/// <param name="id">Идентификатор объекта AutoCAD</param>
		/// <returns>Клонированный объект или null</returns>
		public static TYPE GetCloneObject<TYPE>(ObjectId id) where TYPE : DBObject
		{
			TYPE co = GetObjectPointer<TYPE>(id);
			return GetCloneObject(co);
		}

		/// <summary>
		/// Клонирование объекта с приведением типа
		/// </summary>
		/// <typeparam name="TYPE">Тип к которому приводиться объект</typeparam>
		/// <param name="handle">Дескриптор объекта AutoCAD</param>
		/// <returns>Клонированный объект или null</returns>
		public static TYPE GetCloneObject<TYPE>(Handle handle) where TYPE : DBObject
		{
			ObjectId id = DBRoutine.GetObjectId(handle);
			return GetCloneObject<TYPE>(id);
		}

		/// <summary>
		///  Клонирование объекта с приведением типа
		/// </summary>
		/// <typeparam name="TYPE">Тип к которому приводиться объект</typeparam>
		/// <param name="obj">Дескриптор объекта AutoCAD</param>
		/// <returns>Клонированный объект или null</returns>
		public static TYPE GetCloneObject<TYPE>(TYPE obj) where TYPE : DBObject
		{
			if (obj == null) return null;
			return obj.Clone() as TYPE;
		}


		/// <summary>
		/// Получение примитва AutoCAD для чтения (транзакция заккрыта!)
		/// </summary>
		/// <typeparam name="TYPE">Тип примитива AutoCAD.</typeparam>
		/// <param name="objId">Идентификатор объекта AutoCAD</param>
		/// <returns>Примитв AutoCAD</returns>
		/// <remarks>В случае ошибки происходит генерация исключений</remarks>
		public static TYPE GetObjectPointer<TYPE>(ObjectId objId) where TYPE : DBObject
		{
			if (objId == ObjectId.Null || objId.IsNull || !objId.IsResident || !objId.IsValid)
				throw new Exception("EntityProvider::GetObjectPointer не верный идентификатор объекта (ObjectId)");

			using (Transaction tr = DBRoutine.StartTransaction(true))
			{
				DBObject obj = tr.GetObject(objId, OpenMode.ForRead);
				if (obj is TYPE)
				{
					TYPE ent = obj as TYPE;
					tr.Commit();
					return ent;
				}
				tr.Abort();
				throw new Exception("EntityProvider::GetObjectPointer тип объекта полученного не совпадает с указанным (" + typeof(TYPE).Name + ")");
			}

		}

		/// <summary>
		/// Получение примитва AutoCAD для чтения (транзакция заккрыта!)
		/// </summary>
		/// <typeparam name="TYPE">Тип примитива AutoCAD.</typeparam>
		/// <param name="objId">Идентификатор объекта AutoCAD</param>
		/// <returns>Примитв AutoCAD</returns>
		/// <remarks>В случае ошибки возвращается null</remarks>
		public static TYPE GetObjectPointerNoException<TYPE>(ObjectId objId) where TYPE : DBObject
		{
			TYPE ent = null;
			if (objId == ObjectId.Null || objId.IsNull || !objId.IsResident || !objId.IsValid)
				return ent;

			using (Transaction tr = DBRoutine.StartTransaction())
			{
				if (tr != null)
				{
					DBObject obj = tr.GetObject(objId, OpenMode.ForRead);
					if (obj is TYPE)
					{
						ent = obj as TYPE;
						tr.Commit();
						return ent;
					}
					tr.Abort();
				}
			}
			return ent;
		}

		/// <summary>
		/// Получение примитва AutoCAD для чтения (транзакция заккрыта!)
		/// </summary>
		/// <typeparam name="TYPE">Тип примитива AutoCAD.</typeparam>
		/// <param name="handle">Дескриптор объекта AutoCAD</param>
		/// <returns>Примитв AutoCAD</returns>
		/// <remarks>В случае ошибки происходит генерация исключений</remarks>
		public static TYPE GetObjectPointer<TYPE>(Handle handle) where TYPE : DBObject
		{
			ObjectId id = DBRoutine.GetObjectId(handle);
			return GetObjectPointer<TYPE>(id);
		}

		/// <summary>
		/// Получение примитва AutoCAD для чтения (транзакция заккрыта!)
		/// </summary>
		/// <typeparam name="TYPE">Тип примитива AutoCAD.</typeparam>
		/// <param name="handle">Дескриптор объекта AutoCAD</param>
		/// <returns>Примитв AutoCAD</returns>
		/// <remarks>В случае ошибки возвращается null</remarks>
		public static TYPE GetObjectPointerNoException<TYPE>(Handle handle) where TYPE : DBObject
		{
			ObjectId id = DBRoutine.GetObjectId(handle);
			return GetObjectPointerNoException<TYPE>(id);
		}

		#region Получение оболочки над объектом

		/// <summary>
		/// Получение открытой на транзакции оболочки
		/// </summary>
		/// <typeparam name="TYPE">Тип примитива AutoCAD.</typeparam>
		/// <param name="objId">Идентификатор объекта AutoCAD</param>
		/// <param name="mode">Режим открытия документа</param>
		/// <returns>Оболочка над объектом</returns>
		/// <remarks>В случае ошибки возвращается null</remarks>
		public static EntityAdapter<TYPE> GetEntityAdapterNoException<TYPE>(ObjectId objId, OpenMode mode) where TYPE : DBObject
		{
			return EntityAdapter<TYPE>.Create(objId, mode);
		}

		/// <summary>
		/// Получение открытой на транзакции оболочки над примитовом AutoCAD
		/// </summary>
		/// <typeparam name="TYPE">Тип примитива AutoCAD.</typeparam>
		/// <param name="handle">Дескриптор объекта AutoCAD</param>
		/// <param name="mode">Режим открытия документа</param>
		/// <returns>Оболочка над объектом</returns>
		/// <remarks>В случае ошибки возвращается null</remarks>
		public static EntityAdapter<TYPE> GetEntityAdapterNoException<TYPE>(Handle handle, OpenMode mode) where TYPE : DBObject
		{
			ObjectId id = DBRoutine.GetObjectId(handle);
			return GetEntityAdapterNoException<TYPE>(id, mode);
		}

		/// <summary>
		/// Получение открытой на транзакции оболочки над примитовом AutoCAD
		/// </summary>
		/// <typeparam name="TYPE">Тип примитива AutoCAD.</typeparam>
		/// <param name="obj">Примитив AutoCAD</param>
		/// <param name="mode">Режим открытия документа</param>
		/// <returns>Оболочка над объектом</returns>
		/// <remarks>В случае ошибки возвращается null</remarks>
		public static EntityAdapter<TYPE> GetEntityAdapterNoException<TYPE>(TYPE obj, OpenMode mode) where TYPE : DBObject
		{
			return GetEntityAdapterNoException<TYPE>(obj.Id, mode);
		}

		/// <summary>
		/// Получение открытой на транзакции оболочки
		/// </summary>
		/// <typeparam name="TYPE">Тип примитива AutoCAD.</typeparam>
		/// <param name="objId">Идентификатор объекта AutoCAD</param>
		/// <param name="mode">Режим открытия документа</param>
		/// <returns>Оболочка над объектом</returns>
		/// <remarks>В случае ошибки происходит генерация исключений</remarks>
		public static EntityAdapter<TYPE> GetEntityAdapter<TYPE>(ObjectId objId, OpenMode mode) where TYPE : DBObject
		{
			EntityAdapter<TYPE> oo = EntityAdapter<TYPE>.Create(objId, mode);
			if (oo == null)
				throw new Exception("EntityProvider::GetEntityAdapter не удалось создать оболочку над объектом");
			return oo;
		}

		/// <summary>
		/// Получение открытой на транзакции оболочки над примитовом AutoCAD
		/// </summary>
		/// <typeparam name="TYPE">Тип примитива AutoCAD.</typeparam>
		/// <param name="handle">Дескриптор объекта AutoCAD</param>
		/// <param name="mode">Режим открытия документа</param>
		/// <returns>Оболочка над объектом</returns>
		/// <remarks>В случае ошибки возвращается null</remarks>
		public static EntityAdapter<TYPE> GetEntityAdapter<TYPE>(Handle handle, OpenMode mode) where TYPE : DBObject
		{
			ObjectId id = DBRoutine.GetObjectId(handle);
			return GetEntityAdapter<TYPE>(id, mode);
		}

		/// <summary>
		/// Получение открытой на транзакции оболочки над примитовом AutoCAD
		/// </summary>
		/// <typeparam name="TYPE">Тип примитива AutoCAD.</typeparam>
		/// <param name="obj">Примитив AutoCAD</param>
		/// <param name="mode">Режим открытия документа</param>
		/// <returns>Оболочка над объектом</returns>
		/// <remarks>В случае ошибки возвращается null</remarks>
		public static EntityAdapter<TYPE> GetEntityAdapter<TYPE>(TYPE obj, OpenMode mode) where TYPE : DBObject
		{
			return GetEntityAdapter<TYPE>(obj.Id, mode);
		}

		#endregion
	}
}
