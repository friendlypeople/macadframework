// Copyright (c) 2012 Сергей Попов, Виталий Сайдин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Sergey Popov, Vitaliy Saydin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Celsio.Commons.Interfaces;
using Celsio.Serializers.AbnormalSerializer;
using MAcadFramework.Routines;

namespace MAcadFramework.Aggregates
{
#if !Acad2008
	internal class HandleRecord
	{
		private Handle _handle;
		private bool _isErased;

		public Handle Handle
		{
			get { return _handle; }
			set { _handle = value; }
		}

		public bool IsErased
		{
			get { return _isErased; }
			set { _isErased = value; }
		}
		public HandleRecord()
		{
			_handle = default(Handle);
			_isErased = false;
		}

		public HandleRecord(Handle handle)
		{
			_handle = handle;
			_isErased = false;
		}
		public HandleRecord(Handle handle, bool isErased)
		{
			_handle = handle;
			_isErased = isErased;
		}
	}

	/// <summary>
	/// Класс хранения ссылок. Умеет переключаться между новым выбранным
	/// объектом и старым если новый был удален и обратно при отмене удаления
	/// </summary>
	internal sealed class WiseHandleKeeper
	{
		private readonly List<HandleRecord> _handleStack = new List<HandleRecord>(10);

		private bool IsContains(Handle handle)
		{
			foreach (HandleRecord hr in _handleStack)
			{
				if (hr.Handle == handle)
					return true;
			}
			return false;
		}

		private HandleRecord GetAt(Handle handle)
		{
			foreach (HandleRecord hr in _handleStack)
			{
				if (hr.Handle == handle)
					return hr;
			}
			return default(HandleRecord);
		}

		private void MoveFirstValidToTop()
		{
			HandleRecord rec = new HandleRecord();
			foreach (HandleRecord hr in _handleStack)
			{
				if (!hr.IsErased)
				{
					rec = hr;
					//break;
				}
			}
			_handleStack.Remove(rec);
			_handleStack.Add(rec);
		}

		/// <summary>
		/// Поместить дескриптор на верх списка
		/// </summary>
		/// <param name="handle">The handle.</param>
		public void Push(Handle handle)
		{
			// Если это один из наших объектов, то метим его
			if (IsContains(handle))
			{
				HandleRecord hr = GetAt(handle);
				hr.IsErased = false;
				_handleStack.Remove(hr);
				_handleStack.Add(hr);
			}
			_handleStack.Add(new HandleRecord(handle));
		}


		/// <summary>
		/// Получить текущий используемый дескриптор
		/// </summary>
		/// <returns></returns>
		public Handle Current
		{
			get
			{
				if (_handleStack.Count > 0)
					return _handleStack[_handleStack.Count - 1].Handle;
				return default(Handle); 
			}
		}

		/// <summary>
		/// Удаление объекта
		/// </summary>
		/// <param name="handle">Дескриптор удаляемого объекта.</param>
		public void Erase(Handle handle)
		{
			// Если это один из наших объектов, то метим его
			if (IsContains(handle))
			{
				HandleRecord hr = GetAt(handle);
				hr.IsErased = true;
			}
			// Если объект не текущий использованый то нас это не касается
			if (Current != handle)  return; 
			
			// Иначе ищем чем заменить
			MoveFirstValidToTop();
		}

		/// <summary>
		/// Востановление объекта
		/// </summary>
		/// <param name="handle">The handle.</param>
		public void Restore(Handle handle)
		{
			if (IsContains(handle))
			{
				HandleRecord hr = GetAt(handle);
				hr.IsErased = false;
				_handleStack.Remove(hr);
				_handleStack.Add(hr);
			}
		}

		public void Clear()
		{
			_handleStack.Clear();
		}
	}

	/// <summary>
	/// Класс обертка над дескриптором AutoCAD
	/// </summary>
	/// <remarks>
	/// Упращает работу с дескриптором AutoCAD реализую часто встречающиеся операции.
	/// Позволяет отслеживать модификацию и удаление объекта, используя событийную модель.
	/// <para>
	/// При использовании методов SmartHandle.Set предыдущий дескриптор не удаляется а помещается в хранилище.
	/// Это сделано для того, что бы при удалении объекта связанного с текущим дескриптором и востановлении
	/// предыдущего объекта, текущим дескриптором стал дескриптор востановленного объекта.
	/// Такая ситуация возникает при использовании пользователем сочетания клавиш Ctrl+Z, и позволяет
	/// избегать повторного указания объекта. При сипользовании Redo активным станет востоновленный дескриптор.
	/// </para>
	/// <para>
	/// Метод SetOnlyOne позволяет ослеживать удаление / востановления только одонго дескриптора.
	/// Этот метод явялет рекомендуемым для использования
	/// </para>
	/// <para>
	/// Обратите внимание на то, что делегаты не сериализуются, и, что после десериализации их нужно
	/// инициализировать заново
	/// </para>
	/// </remarks>
	[AbnormalSerialize]
	public sealed class SmartHandle : IXmlSerializable, IEquatable<SmartHandle>, IComparer<SmartHandle>, IHandle<long>, IDisposable
	{
		[SerializeDeclarate]
		private Handle				_handle = default(Handle);

		private readonly XmlAbnormalSerializer _as = new XmlAbnormalSerializer(typeof(SmartHandle));

		private Database			_db;
	
		private readonly WiseHandleKeeper _keeper = new WiseHandleKeeper();

		/// <summary>
		/// Делегат связанный с событием удаленя / востановления объекта
		/// </summary>
        /// <param name="obj">Удаленный объект</param>
		/// <param name="isErased">если указано <c>true</c> то объект был удален.</param>
		public delegate void EraseObjectHandler(DBObject obj, bool isErased);

		/// <summary>
		/// Делегат связанный с событием модификации объекта
		/// </summary>
        /// <param name="obj">Измененный объект</param>
        public delegate void ModifiedObjectHandler(DBObject obj);

		/// <summary>
		/// Событие срабатывающее при удалении объекта
		/// </summary>
		/// <remarks>
		/// Используйте делегат <see cref="EraseObjectHandler"/> для работы с событием
		/// </remarks>
		public event EraseObjectHandler OnEraseObject;

		/// <summary>
		/// Событие срабатывающее при модификации объекта
		/// </summary>
		/// <remarks>
		/// Используйте делегат <see cref="ModifiedObjectHandler"/> для работы с событием
		/// </remarks>
		public event ModifiedObjectHandler OnModifiedObject;

        /// <summary>
        /// Событие срабатывающее при отмене действия для объекта
        /// </summary>
        /// <remarks>
        /// Используйте делегат <see cref="ModifiedObjectHandler"/> для работы с событием
        /// </remarks>
        public event ModifiedObjectHandler OnUndoingObject;

		#region Конструкторы

		/// <summary>
		/// Создание нового экземпляра класса <see cref="SmartHandle"/>.
		/// </summary>
		/// <param name="handle">Дескриптор объекта AutoCAD</param>
		public SmartHandle(Handle handle)
		{
			Initialize(handle);
		}
		/// <summary>
		/// Создание нового экземпляра класса <see cref="SmartHandle"/>.
		/// </summary>
		/// <remarks>
		/// Этот конструктор используется при дессериализации объекта
		/// </remarks>
		public SmartHandle()
		{
			Initialize(default(Handle));
		}

		/// <summary>
		/// Создание нового экземпляра класса <see cref="SmartHandle"/>.
		/// </summary>
		/// <param name="handle">Дескриптор AutoCAD в числовой форме</param>
		public SmartHandle(long handle)
		{
			Initialize(new Handle(handle));
		}

		/// <summary>
		/// Создание нового экземпляра класса <see cref="SmartHandle"/>.
		/// </summary>
		/// <param name="id">Идентификатор объекта AutoCAD</param>
		public SmartHandle(ObjectId id)
		{
			Initialize(id.Handle);
		}

		#endregion

		#region Закрытые методы

		private void OnEraseObjectInvoker(DBObject obj, bool isErased)
		{
            if (OnEraseObject != null) OnEraseObject(obj, isErased);
		}

        private void OnModifiedObjectInvoker(DBObject obj)
		{
            if (OnModifiedObject != null) OnModifiedObject(obj);
		}

        private void OnUndoObjectInvoker(DBObject obj)
        {
            if (OnUndoingObject != null) OnUndoingObject(obj);
        }

	    private void Initialize(Handle handle)
		{
			_keeper.Push(handle);
			_db = Application.DocumentManager.MdiActiveDocument.Database;
			System.Diagnostics.Debug.Assert(_db != null, "SHandle class", "Ошибка доступа к базе данных AutoCAD");
            //_db.DatabaseToBeDestroyed += DatabaseToBeDestroyedHandler;
            //AttachHandlers();
		}


        //private bool CheckHandlerState(object sender, ObjectErasedEventArgs e, out DBObject obj)
        //{
        //    obj = null;
        //    if (sender != null)
        //    {
        //        Database db = sender as Database;
        //        if (db == null || db != _db) return false;
        //    }

        //    if (e == null || e.DBObject == null) return false;
        //    obj = e.DBObject;
        //    if (obj.Id.IsNull) return false;
        //    return true;
        //}

        //private bool CheckHandlerState(object sender, ObjectEventArgs e, out DBObject obj)
        //{
        //    obj = null;
        //    if (sender != null)
        //    {
        //        Database db = sender as Database;
        //        if (db == null || db != _db) return false;
        //    }

        //    if (e == null || e.DBObject == null) return false;
        //    obj = e.DBObject;
        //    if (obj.Id.IsNull) return false;
        //    return true;
        //}

        private void AttachHandlers()
        {
            if (!IsValid) return;
            using (EditorRoutine.LockDoc())
            {
                using (Transaction trans = DBRoutine.StartTransaction(_db))
                {
                    ObjectId id = DBRoutine.GetObjectId(_keeper.Current, _db);
                    DBObject obj = trans.GetObject(id, OpenMode.ForWrite);
                    obj.Modified += Object_Modified;
                    obj.ModifyUndone += Object_ModifyUndone;
                    obj.Erased += Object_Erased;
                    trans.Commit();
                }
            }
        }

        private void DetachHandlers()
        {
            if (!IsValid) return;
            using (EditorRoutine.LockDoc())
            {
                using (Transaction trans = DBRoutine.StartTransaction(_db))
                {
                    ObjectId id = DBRoutine.GetObjectId(_keeper.Current, _db);
                    DBObject obj = trans.GetObject(id, OpenMode.ForWrite);
                    obj.Modified -= Object_Modified;
                    obj.ModifyUndone -= Object_ModifyUndone;
                    obj.Erased -= Object_Erased;
                    trans.Commit();
                }
            }
            //if (_db != null)
            //{
            //    _db.ObjectErased -= ObjectErasedHandler;
            //    _db.ObjectModified -= ObjectModifiedHandler;
            //    _db.DatabaseToBeDestroyed -= DatabaseToBeDestroyedHandler;
            //}
        }

        void Object_Modified(object sender, EventArgs e)
        {
            DBObject obj = sender as DBObject;
            if (obj != null)
            {
                if (!obj.IsUndoing && !obj.IsEraseStatusToggled && obj.Handle.Equals(_keeper.Current))
                    OnModifiedObjectInvoker(obj);
            }
        }

        void Object_ModifyUndone(object sender, EventArgs e)
        {
            DBObject obj = sender as DBObject;
            if (obj != null)
            {
                if (!obj.IsEraseStatusToggled && obj.Handle.Equals(_keeper.Current))
                    OnUndoObjectInvoker(obj);
            }
        }

        void Object_Erased(object sender, ObjectErasedEventArgs e)
        {
            DBObject obj = sender as DBObject;
            if (obj != null)
            {
                if (obj.Handle.Equals(_keeper.Current))
                    OnEraseObjectInvoker(obj, e.Erased);
            }
        }

		#endregion

		#region Делегаты связи с базой данных AutoCAD

        ///// <summary>
        ///// Обработка при удалении/востановлении объекта из/в БД
        ///// </summary>
        //private void ObjectErasedHandler(object sender, ObjectErasedEventArgs e)
        //{
        //    DBObject obj;
        //    if (!CheckHandlerState(sender, e, out obj))
        //        return;

        //    if (e.Erased)
        //    {
        //        // Удаление объекта
        //        Handle h = _keeper.Current;
        //        if (obj.Handle.Equals(h))
        //            OnEraseObjectInvoker(true);
        //        _keeper.Erase(obj.Id.Handle);
				
        //    }
        //    else
        //    {
        //        // Восставновление объекта
        //        _keeper.Restore(obj.Id.Handle);

        //        Handle h = _keeper.Current;

        //        if (obj.Handle.Equals(h))
        //            OnEraseObjectInvoker(false);
        //    }
        //}

        //private void ObjectModifiedHandler(object sender, ObjectEventArgs e)
        //{
        //    // Вот здесь и будем отслеживать изменения наших объектов
        //    DBObject obj;
        //    if (!CheckHandlerState(sender, e, out obj))
        //        return;
        //    Handle h = _keeper.Current;
        //    if (obj.Handle.Equals(h)) OnModifiedObjectInvoker();
        //}


        ///// <summary>
        ///// Удаление базы данных
        ///// </summary>
        //private void DatabaseToBeDestroyedHandler(object sender, EventArgs e)
        //{
        //    DetachHandlers();
        //    _db.DatabaseToBeDestroyed -= DatabaseToBeDestroyedHandler;
        //}

	    #endregion

		#region Открытые методы

		/// <summary>
		/// Получить ссылку на закрытый объект для чтения данных
		/// </summary>
		/// <typeparam name="TObject">Тип объекта, производные от DBObject</typeparam>
		/// <returns>Ссылка на объект</returns>
		public TObject GetObjectPointer<TObject>() where TObject : DBObject
		{
			return EntityProvider.GetObjectPointer<TObject>(_keeper.Current);
		}

		/// <summary>
		/// Получить копию объекта не добавленную в базу данных
		/// </summary>
		/// <typeparam name="TObject">Тип объекта, производные от DBObject</typeparam>
		/// <returns>Копия объекта</returns>
		public TObject GetCloneObject<TObject>() where TObject : DBObject
		{
			return EntityProvider.GetCloneObject<TObject>(_keeper.Current);
		}


		#endregion

		#region Открытые свойства

		/// <summary>
		/// Получить или задать дескриптор объекта AutoCAD
		/// </summary>
		/// <value> Дескриптор объекта AutoCAD </value>
		public Handle Handle
		{
			get {  return _keeper.Current; }
			set { _keeper.Push(value); }
		}

        /// <summary>
        /// Получить или задать абсолютное значение
        /// </summary>
        public long Value
        {
            get { return Handle.Value; }
            set { SetOnlyOne(new Handle(value)); }
        }

		/// <summary>
		/// Получить идентификатор объекта AutoCAD
		/// </summary>
		/// <value> Идентификатор объекта AutoCAD </value>
		public ObjectId Id
		{
			get { return DBRoutine.GetObjectId(_keeper.Current); }
		}
		/// <summary>
		/// Проверить дескриптор на действительность
		/// </summary>
		/// <value>
		///   <c>true</c> если дескриптор действителен (не удален), иначе<c>false</c>.
		/// </value>
		public bool IsValid
		{
			get
            {
                ObjectId id = DBRoutine.GetObjectId(_keeper.Current, _db);
                return DBRoutine.CheckId(id);
            }
		}

        ///// <summary>
        ///// База данных, к которой привязан дескриптор
        ///// </summary>
        //public Database Database
        //{
        //    get { return _db; }
        //}

	    /// <summary>
		/// Установить новый отслеживаемый дескриптор
		/// </summary>
		/// <param name="id">Идентификатор объекта AutoCAD</param>
		/// <remarks>
		/// Отличие метода SetOnlyOne от метода Set смотрите в описании класса <see cref="SmartHandle"/>
		/// </remarks>
		public void Set(ObjectId id)
		{
            Set(id.Handle);
		}

        /// <summary>
        /// Установить новый отслеживаемый дескриптор и базу данных, в которой инициализирован дескриптор
        /// </summary>
        /// <param name="id">Идентификатор объекта AutoCAD</param>
        /// <param name="database">База AutoCAD</param>
        /// <remarks>
        /// Отличие метода SetOnlyOne от метода Set смотрите в описании класса <see cref="SmartHandle"/>
        /// </remarks>
        public void Set(ObjectId id, Database database)
        {
            Set(id.Handle, database);
        }

		/// <summary>
		/// Установить новый отслеживаемый дескриптор
		/// </summary>
		/// <param name="handle">Дескриптор объекта AutoCAD</param>
		/// <remarks>
		/// Отличие метода SetOnlyOne от метода Set смотрите в описании класса <see cref="SmartHandle"/>
		/// </remarks>
		public void Set(Handle handle)
		{
            Set(handle, _db);
		}

        /// <summary>
        /// Установить новый отслеживаемый дескриптор и базу данных, в которой инициализирован дескриптор
        /// </summary>
        /// <param name="handle">Дескриптор объекта AutoCAD</param>
        /// <param name="database">База AutoCAD</param>
        /// <remarks>
        /// Отличие метода SetOnlyOne от метода Set смотрите в описании класса <see cref="SmartHandle"/>
        /// </remarks>
        public void Set(Handle handle, Database database)
        {
            DetachHandlers();
            _keeper.Push(handle);
            _db = database;
            AttachHandlers();
            //if (_db != database)
            //{
            //    DetachHandlers();
            //    _db = database;
            //    AttachHandlers();
            //}
        }

		/// <summary>
		/// Установить единственный дескриптор для отслеживания
		/// </summary>
		/// <param name="id">Идентификатор объекта AutoCAD</param>
		/// <remarks>
		/// Отличие метода SetOnlyOne от метода Set смотрите в описании класса <see cref="SmartHandle"/>
		/// </remarks>
		public void SetOnlyOne(ObjectId id)
		{
            SetOnlyOne(id.Handle);
		}

        /// <summary>
        /// Установить единственный дескриптор для отслеживания и базу данных, в которой инициализирован дескриптор
        /// </summary>
        /// <param name="id">Идентификатор объекта AutoCAD</param>
        /// <param name="database">База AutoCAD</param>
        /// <remarks>
        /// Отличие метода SetOnlyOne от метода Set смотрите в описании класса <see cref="SmartHandle"/>
        /// </remarks>
        public void SetOnlyOne(ObjectId id, Database database)
        {
            SetOnlyOne(id.Handle, database);
        }

	    /// <summary>
		/// Установить единственный дескриптор для отслеживания
		/// </summary>
		/// <param name="handle">Дескриптор объекта AutoCAD</param>
		/// <remarks>
		/// Отличие метода SetOnlyOne от метода Set смотрите в описании класса <see cref="SmartHandle"/>
		/// </remarks>
		public void SetOnlyOne(Handle handle)
		{
            SetOnlyOne(handle, _db);
		}

        /// <summary>
        /// Установить единственный дескриптор для отслеживания и базу данных, в которой инициализирован дескриптор
        /// </summary>
        /// <param name="handle">Дескриптор объекта AutoCAD</param>
        /// <param name="database">База AutoCAD</param>
        /// <remarks>
        /// Отличие метода SetOnlyOne от метода Set смотрите в описании класса <see cref="SmartHandle"/>
        /// </remarks>
        public void SetOnlyOne(Handle handle, Database database)
        {
            SetWeak(handle, database);
            AttachHandlers();
        }

        /// <summary>
        /// Установить единственный дескриптор для отслеживания и базу данных, в которой инициализирован дескриптор
        /// </summary>
        /// <param name="sh">Обертка над дескриптором AutoCAD</param>
        /// <remarks>
        /// Отличие метода SetOnlyOne от метода Set смотрите в описании класса <see cref="SmartHandle"/>
        /// </remarks>
        public void SetOnlyOne(SmartHandle sh)
        {
            SetOnlyOne(sh.Handle, sh._db);
        }

        /// <summary>
        /// Установить единственный дескриптор(без отслеживания изменений) и базу данных, в которой инициализирован дескриптор
        /// </summary>
        /// <param name="id">Идентификатор объекта AutoCAD</param>
        public void SetWeak(ObjectId id)
        {
            SetWeak(id.Handle);
        }

        /// <summary>
        /// Установить единственный дескриптор(без отслеживания изменений) и базу данных, в которой инициализирован дескриптор
        /// </summary>
        /// <param name="id">Идентификатор объекта AutoCAD</param>
        /// <param name="database">База AutoCAD</param>
        public void SetWeak(ObjectId id, Database database)
        {
            SetWeak(id.Handle, database);
        }

        /// <summary>
        /// Установить единственный дескриптор(без отслеживания изменений) и базу данных, в которой инициализирован дескриптор
        /// </summary>
        /// <param name="handle">Дескриптор объекта AutoCAD</param>
        public void SetWeak(Handle handle)
        {
            SetWeak(handle, _db);
        }

        /// <summary>
        /// Установить единственный дескриптор(без отслеживания изменений) и базу данных, в которой инициализирован дескриптор
        /// </summary>
        /// <param name="handle">Дескриптор объекта AutoCAD</param>
        /// <param name="database">База AutoCAD</param>
        public void SetWeak(Handle handle, Database database)
        {
            DetachHandlers();
            _keeper.Clear();
            _keeper.Push(handle);
            _db = database;
        }

        /// <summary>
        /// Установить единственный дескриптор(без отслеживания изменений) и базу данных, в которой инициализирован дескриптор
        /// </summary>
        /// <param name="sh">Обертка над дескриптором AutoCAD</param>
        public void SetWeak(SmartHandle sh)
        {
            SetWeak(sh.Handle, sh._db);
        }

	    #endregion

		#region  Операторы преобразования


		/// <summary>
		/// Преобразование из SHandle в Handle
		/// </summary>
		/// <param name="sh">Обертка над дескриптором</param>
		/// <returns>Дескриптор объекта AutoCAD</returns>
		public static implicit operator Handle(SmartHandle sh)
		{
			return sh._keeper.Current;
		}
		#endregion

		#region IXmlSerializable Members

		public System.Xml.Schema.XmlSchema GetSchema() { return null; }

		public void ReadXml(XmlReader reader)
		{
			_as.Deserialize(reader, this);
			_keeper.Push(_handle);
            AttachHandlers();
		}

		public void WriteXml(XmlWriter writer)
		{
			_handle = _keeper.Current;
			_as.Serialize(writer, this);
		}

		#endregion

		#region IEquatable<SHandle> Members

		public bool Equals(SmartHandle other)
		{
			if (other == null)
				return false;

			return _keeper.Current == other._keeper.Current;
		}

		#endregion

		#region IComparer<SHandle> Members

		public int Compare(SmartHandle x, SmartHandle y)
		{
			if (x == null || y == null)
				return 0;
			return x._keeper.Current.Value.CompareTo(y._keeper.Current.Value);
		}

		#endregion

	    public void Dispose()
	    {
            Close();
	    }

        /// <summary>
        /// Закрыть дескриптор.
        /// Внешние обработчики изменения и удаления объекта отлючать вручную!
        /// </summary>
        public void Close()
        {
            DetachHandlers();
        }
	}
#endif
}
