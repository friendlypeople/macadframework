// Copyright (c) 2012 Сергей Попов, Виталий Сайдин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Sergey Popov, Vitaliy Saydin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using Autodesk.AutoCAD.DatabaseServices;

namespace MAcadFramework.Aggregates
{
    /// <summary>
    /// Класс импорта объектов из одной базы данных в другую
    /// </summary>
    public class ObjectsImporter
    {
        private readonly Database _srcDatabase;
        private readonly Database _dstDatabase;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="srcDatabase">Откуда</param>
        /// <param name="dstDatabase">Куда</param>
        public ObjectsImporter(Database srcDatabase, Database dstDatabase)
        {
            _srcDatabase = srcDatabase;
            _dstDatabase = dstDatabase;
        }

        /// <summary>
        /// Полный перенос
        /// </summary>
        /// <returns>Карта id оперенесенных объектов</returns>
        public IdMapping FullTransfer()
        {
            Transaction srcTrans = _srcDatabase.TransactionManager.StartTransaction();
            try
            {
                BlockTable srcBlockTable = (BlockTable)srcTrans.GetObject(_srcDatabase.BlockTableId, OpenMode.ForRead, false);
                BlockTableRecord srcModelSpace = (BlockTableRecord)srcTrans.GetObject(srcBlockTable[BlockTableRecord.ModelSpace], OpenMode.ForRead, false);

                // Коллекция всех id объектов ModelSpace
                ObjectIdCollection allObjectsIdCollection = new ObjectIdCollection();
                // Заполняем таблицы
                foreach (ObjectId id in srcModelSpace)
                {
                    allObjectsIdCollection.Add(id);
                }

                // Копируем объекты
                IdMapping mapping = PartialTransfer(allObjectsIdCollection);

                srcTrans.Commit();
                return mapping;
            }
            catch
            {
                srcTrans.Abort();
                throw;
            }
        }

        /// <summary>
        /// Частичный перенос
        /// </summary>
        /// <param name="srcCollection">Коллекция id объектов, которых необходимо перенести</param>
        /// <returns>Карта id оперенесенных объектов</returns>
        public IdMapping PartialTransfer(ObjectIdCollection srcCollection)
        {
            Transaction dstTrans = _dstDatabase.TransactionManager.StartTransaction();
            try
            {
                // Получение необходимых таблиц и записей этих таблиц
                BlockTable dstBlockTable = (BlockTable)dstTrans.GetObject(_dstDatabase.BlockTableId, OpenMode.ForRead, false);
                ObjectId dstModelSpaceId = dstBlockTable[BlockTableRecord.ModelSpace];

                // Копируем объекты
                IdMapping mapping = new IdMapping();
                _srcDatabase.WblockCloneObjects(srcCollection, dstModelSpaceId, mapping, DuplicateRecordCloning.Ignore, false);

                dstTrans.Commit();
                return mapping;
            }
            catch
            {
                dstTrans.Abort();
                throw;
            }
        }
    }
}
