// Copyright (c) 2012 Сергей Попов, Виталий Сайдин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Sergey Popov, Vitaliy Saydin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using Autodesk.AutoCAD.ApplicationServices;

namespace MAcadFramework
{
    /// <summary>
    /// Класс системных переменных
    /// </summary>
	public static class SystemVariable
	{
		// Если вы используете системные переменные, то просьба
		// добавлять их в этот список

		#region Список системных переменных AutoCAD
		
		/// <summary>
		/// Сист. переменная удаления исходных кривых в операциях создания регионов
		/// </summary>
		public const string DelObj = "DELOBJ";


		/// <summary>
		/// Сист. переменная показывающая текущее пронстранство
		/// 0 - Пространство листа
		/// 1 - Пространство модели
		/// Возвращаемое значение short
		/// </summary>
		public const string TileMode = "TILEMODE";

		#endregion

		/// <summary>
		/// Получение системной переменной AutoCAD
		/// </summary>
		/// <param name="varName">Имя систменой переменной</param>
		/// <returns>Значение системной переменной</returns>
		public static object GetSystemVariable(string varName)
		{
			return Application.GetSystemVariable(varName);
		}

		/// <summary>
		/// Задание системной переменной AutoCAD
		/// </summary>
		/// <param name="varName">Имя системной переменной</param>
		/// <param name="value">Значение системной переменной</param>
		public static void SetSystemVariable(string varName, object value)
		{
			Application.SetSystemVariable(varName, value);
		}

		/// <summary>
		/// Получение системной переменной AutoCAD
		/// </summary>
		/// <typeparam name="TYPE">Приведенный тип системной переменной</typeparam>
		/// <param name="varName">Имя системной переменной</param>
		/// <returns>Значение системной переменной</returns>
		public static TYPE GetSystemVariable<TYPE>(string varName)
		{
			return (TYPE)Application.GetSystemVariable(varName);
		}
	}

	/// <summary>
	/// Класс для работы с системной переменной AutoCAD.
	/// </summary>
	/// <remarks>
	/// При вызове Close или Dispose востанавливается изначальное значение
	/// </remarks>
	/// <example>
	/// <code><![CDATA[
	///
	/// // Изменяем значение переменной SystemVariable.DelObj
	/// using(SystemVariableLocker.Use(SystemVariable.DelObj, 1))
	/// {
	///		// Выполняем действия с измененной переменной
	/// }
	/// // Востановилось старое значение
	/// ]]></code>
	/// </example>
	public sealed class SystemVariableLocker : IDisposable
	{
		#region Поля класса

		private bool _isDisposed;
		private readonly object _varLock;
		private readonly string _varName;
		
		#endregion

		/// <summary>
		/// Сменить значение системной переменной AutoCAD
		/// </summary>
		/// <param name="varName">Имя систменой переменной</param>
		/// <param name="value">Значение системной переменной</param>
		/// <returns>Класс блокировки системной переменной</returns>
		public static SystemVariableLocker Use(string varName, object value)
		{
			return new SystemVariableLocker(varName, value);
		}

		/// <summary>
		/// Инициализация нового экзепляра класса <see cref="SystemVariableLocker"/>.
		/// </summary>
		/// <param name="varName">Имя систменой переменной</param>
		/// <param name="value">Значение системной переменной</param>
		private SystemVariableLocker(string varName, object value)
		{
			_varName = varName;
			_varLock = SystemVariable.GetSystemVariable(_varName);
			SystemVariable.SetSystemVariable(_varName, value);
		}

		/// <summary>
		/// Закрытие и востановление исходного значения системной переменной
		/// </summary>
		public void Close()
		{
			if (!_isDisposed)
			{
				_isDisposed = true;
				SystemVariable.SetSystemVariable(_varName, _varLock);
			}
		}
		#region IDisposable Members

		public void Dispose()
		{
			Close();
		}

		#endregion
	}
}
