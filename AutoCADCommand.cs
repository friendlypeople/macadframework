// Copyright (c) 2012 Сергей Попов, Виталий Сайдин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Sergey Popov, Vitaliy Saydin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using Autodesk.AutoCAD.ApplicationServices;
using MAcadFramework.Routines;

namespace MAcadFramework
{
	/// <summary>
	/// Класс для запуска команды AutoCAD
	/// </summary>
	public static class AutoCADCommand
	{
		/// <summary>
		/// Строка выполняемой команды
		/// </summary>
		private static string _commandLine = string.Empty;
		private static CommandEventHandler _onEndedCommandHandler;
		private static CommandEventHandler _onFailedCommandHandler;
		private static CommandEventHandler _onCancelledCommandHandler;


		/// <summary>
		/// Выполнение команды акада
		/// </summary>
		/// <param name="command">Строка команды AutoCAD для исполнения</param>
		public static void Run(string command)
		{
			Document doc = EditorRoutine.Document;
			_commandLine = command;
			doc.SendStringToExecute(command, true, false, false);
		}

		/// <summary>
		/// Запуск команды автокада с реакцией на завершение
		/// </summary>
		/// <param name="command">Строка команды</param>
		/// <param name="onEndCommand">Функция реакции на завершение</param>
		public static void Run(string command, CommandEventHandler onEndCommand)
		{
			Document doc = EditorRoutine.Document;

			// Сохраняем параметры команды и реакции
			_onEndedCommandHandler = onEndCommand;
			_commandLine = command;

			// Реакция на завершение команды (Разные варианты завершения)
			// Только в случае успешного завершения нашей команды вызываем реакцию onEndCommand
			doc.CommandEnded		+= DocCommandEnded;
			doc.CommandFailed		+= DocCommandFailed;
			doc.CommandCancelled	+= DocCommandCancelled;
			doc.SendStringToExecute(command, true, false, false);
		}

		/// <summary>
		/// Запуск команды автокада с реакцией на завершение
		/// </summary>
		/// <param name="command">Строка команды</param>
		/// <param name="onEndCommand">Делегат на событие успешного завершения команды</param>
		/// <param name="onCommandFailed">Делегат на событие ошибки выполнения команды</param>
		/// <param name="onCommandCancelled">Делегат на событие отмены выполнения команды.</param>
		public static void Run(string command, CommandEventHandler onEndCommand, CommandEventHandler onCommandFailed, CommandEventHandler onCommandCancelled)
		{
			Document doc = EditorRoutine.Document;

			// Сохраняем параметры команды и реакции
			_onEndedCommandHandler = onEndCommand;
			_onCancelledCommandHandler = onCommandCancelled;
			_onFailedCommandHandler = onCommandFailed;
			_commandLine = command;

			// Реакция на завершение команды (Разные варианты завершения)
			// Только в случае успешного завершения нашей команды вызываем реакцию onEndCommand
			doc.CommandEnded += DocCommandEnded;
			doc.CommandFailed += DocCommandFailed;
			doc.CommandCancelled += DocCommandCancelled;
			doc.SendStringToExecute(command, true, false, false);
		}

		/// <summary>
		/// Если команда прервана
		/// </summary>
		private static void DocCommandCancelled(object sender, CommandEventArgs e)
		{
			Document doc = sender as Document;
			if (doc == null || !IsOurCommand(e)) return;

			if (_onCancelledCommandHandler != null)
			{
				_onCancelledCommandHandler(sender, e);
			}
			// Обнуляем параметры команды
			ClearCommand(doc);
		}

		/// <summary>
		/// Если команда завершена неудачей
		/// </summary>
		private static void DocCommandFailed(object sender, CommandEventArgs e)
		{
			Document doc = sender as Document;
			if (doc == null || !IsOurCommand(e)) return;

			if (_onFailedCommandHandler != null)
			{
				_onFailedCommandHandler(sender, e);
			}

			// Обнуляем параметры команды
			ClearCommand(doc);
		}

		/// <summary>
		/// Если команда завершена успешно
		/// </summary>
		private static void DocCommandEnded(object sender, CommandEventArgs e)
		{
			// Наша команда?
			Document doc = sender as Document;
			if (doc == null || !IsOurCommand(e)) return;

			if (_onEndedCommandHandler != null)
			{
				_onEndedCommandHandler(sender, e);
			}
			ClearCommand(doc);
			
		}

		/// <summary>
		/// Проверка выполнена ли наша команда
		/// </summary>
		private static bool IsOurCommand(CommandEventArgs e)
		{
			return _commandLine.Contains(e.GlobalCommandName);
		}

		/// <summary>
		/// Обнуляем параметры команды
		/// </summary>
		private static void ClearCommand(Document doc)
		{
			// Обнуляем параметры команды
			_onEndedCommandHandler = null;
			_onFailedCommandHandler = null;
			_onCancelledCommandHandler = null;
			_commandLine = string.Empty;

			// Отключаем все обработчики
			doc.CommandEnded -= DocCommandEnded;
			doc.CommandFailed -= DocCommandFailed;
			doc.CommandCancelled -= DocCommandCancelled;
		}
	}
}
