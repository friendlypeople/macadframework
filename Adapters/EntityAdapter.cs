// Copyright (c) 2012 Сергей Попов, Виталий Сайдин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Sergey Popov, Vitaliy Saydin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using Autodesk.AutoCAD.DatabaseServices;
using MAcadFramework.Routines;

namespace MAcadFramework.Adapters
{

	/// <summary>
	/// Класс оболочка на примитивом AutoCAD
	/// </summary>
	/// <typeparam name="TYPE">Тип примитива AutoCAD, наследованный от <see cref="DBObject"/></typeparam>
	/// <remarks>
	/// Возможно использование как с внутренней транзакцией (создается по умолчанию), так и с внешней,
	/// что позволяет обрабатывать несколько объектов на одной транзакции.
	/// Если для открытия использована внешняя транзакция, то методы Close, Commit, Abort не выполняются!
	/// </remarks>
	/// <example>
	/// <code><![CDATA[
	///
	/// // Открываем объект на запись используя внутреннюю транзакцию
	/// using(EntityAdapter<Polyline> ea = EntityAdapter<Polyline>.Create(objectId, OpenMode.ForWrite))
	/// {
	///		// Доступ к содержимому объекта через оболочку
	///		double area = ea.Object.Area;
	///		// Подтверждаем изменения
	///		ea.Commit();
	/// }
	/// ]]></code>
	/// </example>
	public sealed class EntityAdapter<TYPE> : IDisposable where TYPE : DBObject
	{
		private Transaction _trans;
		private TYPE _obj;
		private readonly bool _useExternalTransaction;
		private bool _isClosed;

		private OpenMode _mode = OpenMode.ForRead;


		#region Инициализация

		/// <summary>
		/// Создание открытого на "внутренней" транзакции объекта
		/// </summary>
		/// <param name="objId">Идентификатор объекта AutoCAD</param>
		/// <param name="mode">Режим открытия объекта</param>
		/// <returns>Оболочка над открытым объектом или null</returns>
		public static EntityAdapter<TYPE> Create(ObjectId objId, OpenMode mode)
		{
			Transaction tr = DBRoutine.StartTransaction();
			if (tr != null && DBRoutine.CheckId(objId, true))
			{
				TYPE obj = tr.GetObject(objId, mode) as TYPE;
				if (obj != null)
					return new EntityAdapter<TYPE>(tr, obj, false, mode);
			}
			return null;
		}

		/// <summary>
		/// Создание открытого на "внешней" транзакции объекта
		/// </summary>
		/// <param name="objId">Идентификатор объекта AutoCAD</param>
		/// <param name="trans">Текущая транзакция AutoCAD</param>
		/// <param name="mode">Режим открытия объекта</param>
		/// <returns>Оболочка над открытым объектом или null</returns>
		public static EntityAdapter<TYPE> Create(ObjectId objId, Transaction trans, OpenMode mode)
		{
			if (trans != null && DBRoutine.CheckId(objId, true))
			{
				TYPE obj = trans.GetObject(objId, mode) as TYPE;
				if (obj != null)
					return new EntityAdapter<TYPE>(trans, obj, true, mode);
			}
			return null;
		}

		/// <summary>
		/// Инициализация нового экзепляра <see cref="EntityAdapter&lt;TYPE&gt;"/> класса.
		/// </summary>
		/// <param name="tr">Текущая транзакция AutoCAD</param>
		/// <param name="obj">Объект над которым образуем оболочку</param>
		/// <param name="useExternalTransaction">Если<c>true</c> [используем "внешнюю" транзакцию].</param>
		/// <param name="mode">Режим открытия объекта</param>
		private EntityAdapter(Transaction tr, TYPE obj, bool useExternalTransaction, OpenMode mode)
		{
			_trans = tr;
			_mode = mode;
			_useExternalTransaction = useExternalTransaction;
			_obj = obj;
		}
		#endregion


		#region Смены состояния объекта


		/// <summary>
		/// Сменить состояние на "Чтение" (OpenMode.ForRead)
		/// </summary>
		/// <returns>
		/// 	<c>true</c> если операции выполнена успешна, иначе <c>false</c>
		/// </returns>
		public bool DowngradeOpen()
		{
			try
			{
				if (_obj.IsReadEnabled) return true;
				if (_obj.IsWriteEnabled)
				{
					_obj.DowngradeOpen();
					_mode = OpenMode.ForRead;
					return true;
				}
			}
			catch (Exception ex)
			{
				EditorRoutine.WriteMessage("Ошибка открытия объекта на чтение: " + ex.Message);
			}
			return false;
		}

		/// <summary>
		/// Сменить состояние на "Запись" (OpenMode.ForWrite)
		/// </summary>
		/// <returns>
		/// 	<c>true</c> если операции выполнена успешна, иначе <c>false</c>
		/// </returns>
		public bool UpgradeOpen()
		{
			try
			{
				if (_obj.IsWriteEnabled) return true;
				if (_obj.IsReadEnabled)
				{
					_obj.UpgradeOpen();
					_mode = OpenMode.ForWrite;
					return true;
				}
			}
			catch (Exception ex)
			{
				EditorRoutine.WriteMessage("Ошибка открытия объекта на запись: " + ex.Message);
			}
			return false;
		}
		#endregion

		/// <summary>
		/// Получить идентификатор объекта
		/// </summary>
		/// <value> Идентификатор объекта</value>
		public ObjectId ObjectId
		{
			get { return _obj.ObjectId; }
		}

		/// <summary>
		/// Получить дескриптор объекта
		/// </summary>
		/// <value>Дескриптор объекта</value>
		public Handle Handle
		{
			get { return _obj.ObjectId.Handle; }
		}


		/// <summary>
		/// Закрытие оболочки с потверждением изменений
		/// </summary>
		public void Close()
		{
			if (_isClosed || _useExternalTransaction) return;
			_trans.Commit();
			FinalizeSelf();
		}

		/// <summary>
		/// Закрытие оболочки с потверждением изменений
		/// </summary>
		public void Commit()
		{
			Close();
		}

		/// <summary>
		/// Закрытие оболочки с отменой изменений
		/// </summary>
		public void Abort()
		{
			if (_isClosed || _useExternalTransaction) return;
			_trans.Abort();
			FinalizeSelf();
		}

		/// <summary>
		/// Получить текущее состояние объекта
		/// </summary>
		/// <value>Текущее состояние объекта</value>
		public OpenMode Mode
		{
			get { return _mode; }
		}

		/// <summary>
		/// Получить примитив AutoCAD
		/// </summary>
		/// <value>Примитив AutoCADt</value>
		public TYPE Object
		{
			get { return _obj; }
		}

		/// <summary>
		/// Получить транзакцию на которой открыт примитив
		/// </summary>
		/// <value>Транзакция на которой открыт примитив</value>
		public Transaction Transaction
		{
			get { return _trans; }
		}

		private void FinalizeSelf()
		{
			_isClosed = true;
			_trans.Dispose();
			_trans = null;
			_obj = null;
		}

        /// <summary>
        /// Выполняет определяемые приложением задачи, связанных с удалением, высвобождением или сбросом неуправляемых ресурсов.
        /// </summary>
		public void Dispose() { Close(); }
	}
}
