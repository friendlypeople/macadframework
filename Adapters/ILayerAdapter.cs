// Copyright (c) 2012 Сергей Попов, Виталий Сайдин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Sergey Popov, Vitaliy Saydin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using Autodesk.AutoCAD.Colors;
using Autodesk.AutoCAD.DatabaseServices;

namespace MAcadFramework.Adapters
{
	/// <summary>
	/// Интерфейс описывающий оболочку над слоем
	/// </summary>
	public interface ILayerAdapter
	{
		/// <summary>
		/// Активировать слой.
		/// </summary>
		/// <remarks>
		/// Активация слоя устанавливает параметры слоя <see cref="IsOff"/>, <see cref="IsFrozen"/> и <see cref="IsLocked"/>
		/// в значение <c>false</c>.
		/// </remarks>
		void Activate();
		/// <summary>
		/// Дизактивировать слой.
		/// </summary>
		/// <remarks>
		/// Активация слоя устанавливает параметры слоя <see cref="IsOff"/>, <see cref="IsFrozen"/> и <see cref="IsLocked"/>
		/// в значение <c>true</c>.
		/// </remarks>
		void Deactivate();
		/// <summary>
		/// Получить или задать значение, указывающее, выключен ли слой.
		/// </summary>
		/// <value><c>true</c> если данный слой выключен; иначе, <c>false</c>.</value>
		bool IsOff { get; set; }

		/// <summary>
		/// Получить десриптор слоя.
		/// </summary>
		/// <value>Дескриптор слоя</value>
		Handle Handle { get; }
		/// <summary>
		/// Получает или задает значение, указывающее, является ли данный экземпляр замороженные.
		/// </summary>
		/// <value><c>true</c> если слой заморожен; иначе, <c>false</c>.</value>
		bool IsFrozen { get; set; }
		/// <summary>
		/// Получить или задать значение, указывающее, является ли данный слой заблокированным.
		/// </summary>
		/// <value><c>true</c> если слой заблокирован; иначе, <c>false</c>.</value>
		bool IsLocked { get; set; }
		/// <summary>
		/// Получить или задать значение, указывающее, является ли данный слой скрытым.
		/// </summary>
		/// <value><c>true</c> если данный слой скрыт; иначе, <c>false</c>.</value>
		bool IsHidden { get; set; }
		/// <summary>
		/// Получить имя слоя.
		/// </summary>
		/// <value>Имя слоя</value>
		string Name { get; }
		/// <summary>
		/// Получить или установить цвет.
		/// </summary>
		/// <value>Цвет слоя.</value>
		Color Color { get; set; }
		/// <summary>
		/// Получить значение, указывающее, является ли данный слой стертым.
		/// </summary>
		/// <value><c>true</c> если данный слой стерт; иначе, <c>false</c>.</value>
		bool IsErased { get; }
		/// <summary>
		/// Установить текущий слой.
		/// </summary>
		/// <returns><c>true</c> если операция успешна; иначе, <c>false</c></returns>
		bool SetCurrent();
		/// <summary>
		/// Получить флаг валидности.
		/// </summary>
		/// <value><c>true</c> если слой валидный; иначе, <c>false</c>.</value>
		bool IsValid { get; }
	}
}
