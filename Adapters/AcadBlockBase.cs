// Copyright (c) 2012 Сергей Попов, Виталий Сайдин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Sergey Popov, Vitaliy Saydin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.ComponentModel;
using System.Text;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Celsio.Runtime.Converters;
using MAcadFramework.Aggregates;
using MAcadFramework.Routines;

namespace MAcadFramework.Adapters
{
	/// <summary>
	/// Базовый класс облегчеющий создание оболочки для блока
	/// </summary>
	/// <example>
	/// Пример создание оболочки с использованием базового класса <see cref="BlockAdaptorBase"/>
	/// <code><![CDATA[
	/// public class UboltBlockAdaptor : BlockAdaptorBase
	///{
	///		// Индикатор отражения скобы относительно вертикали
	///		public bool IsHorizontalFlip
	///		{
	///			get
	///			{
    ///				short res = GetPropertyValue<short>("Flip state");
	///				return res != 0;
	///			}
	///			set { SetPropertyValue("Flip state", value ? (short)1 : (short)0); }
	///		}
	///
	///		// Получить или задать отступ скобы от точки вставки
	///		public double Distance
	///		{
	///			get { return Math.Round(GetPropertyValue<double>("Distance")); }
	///			set { SetPropertyValue("Distance", value); }
	///		}
	///
	///		// Индикатор отражения скобы относительно горизонтали
	///		public bool IsVerticalFlip
	///		{
	///			get
	///			{
    ///				short res = GetPropertyValue<short>("Flip state Side");
	///				return res != 0;
	///			}
	///			set { SetPropertyValue("Flip state Side", value ? (short)1 : (short)0); }
	///		}
	///
	///		public override string BlockName
	///		{
	///			get { return scdcIpc.WorkBlocks.ThreeD.Ubolt; }
	///		}
	///}
	/// ]]></code>
	/// </example>
	/// <example>
	/// Пример перечисления ассоциированного с визуальным отображением. Так в атрибуте <see cref="DescriptionAttribute"/>
	/// задается имя визуального отображения блока. Это позволяет использовать Enum вместо строк для смены отображения
	/// блока
	/// <code><![CDATA[
	///public enum PipeType
	///{
	///	[Description("Тип не задан.")]
	///	Undefined = -1,
	/// 
	///	[Description("Основная")]
	///	Main = 0,
	/// 
	///	[Description("Примыкающая")]
	///	Verging = 1,
	/// 
	///	[Description("Перепадная")]
	///	Swing = 2
	///}
	/// ]]></code>
	/// </example>
	public abstract class BlockAdaptorBase : IBlockAdapter
	{
		protected ObjectId	_id = ObjectId.Null;
		protected bool		_isValid;

		/// <summary>
		/// Получить состояние параметра отображения
		/// </summary>
		/// <param name="flipStateName">Имя параметра отображения</param>
		/// <returns><c>true</c>, если объект отображен относительно оси, если состояние исходное, <c>false</c></returns>
		protected bool GetFlipedState(string flipStateName)
		{
			short res = GetPropertyValue<short>(flipStateName);
			return res != 0;
		}

		/// <summary>
		/// Установить состояние параметра отображения
		/// </summary>
		/// <param name="flipStateName">Имя параметра отображения</param>
		/// <param name="state">Если <c>true</c>, то объект отображен относительно оси.</param>
		protected void SetFlipedState(string flipStateName, bool state)
		{
			short val = 0;
			if (state) val = 1;
			SetPropertyValue(flipStateName, val);
		}

		/// <summary>
		/// Получить значение атрибута, конвертированное в тип T
		/// </summary>
		/// <typeparam name="T">Тип, к которому будет приведено значение атрибута</typeparam>
		/// <param name="tag">Имя атрибута</param>
		/// <returns>Приведенное значение атрибута</returns>
		/// <remarks>
		/// <see cref="Exception"/> выбрасывается в двух случаях: при отсутствии атрибута с заданным именем и
		/// при невозможности привести значение атрибута к заданному типу.
		/// </remarks>
		protected T GetAttributeValue<T>(string tag)
		{
			try
			{
				object val = BlockRoutine.GetAttributeValue(_id, tag);
				return TypeConverterProvider.SpecificConvertFromString<T>(val as string);
			}
			catch (Exception e)
			{
				throw new Exception("AcadBlockPresentationBase::GetAttributeValue " + tag, e);
			}
		}

		/// <summary>
		/// Записать значение атрибута, конвертированное из типа T
		/// </summary>
		/// <typeparam name="T">Тип, из которого значение атрибута будет приведено в строку</typeparam>
		/// <param name="tag">Имя атрибута</param>
		/// <param name="value">Значение для внесения в атрибут</param>
		protected void SetAttributeValue<T>(string tag, T value)
		{
			try
			{
				string val = TypeConverterProvider.SpecificConvertToString(value);
				BlockRoutine.SetAttributeValue(_id, tag, val);
			}
			catch (Exception e)
			{
				throw new Exception("AcadBlockPresentationBase::SetAttributeValue " + tag, e);
			}
		}

		/// <summary>
		/// Получить значение свойства, конвертированное в тип T
		/// </summary>
		/// <typeparam name="T">Тип, к которому будет приведено значение свойства</typeparam>
		/// <param name="tag">Имя свойства</param>
		/// <returns>Приведенное значение свойства</returns>
		/// <remarks>
		/// <see cref="Exception"/> выбрасывается в двух случаях: при отсутствии свойства с заданным именем и
		/// при невозможности привести значение свойства к заданному типу.
		/// </remarks>
		protected T GetPropertyValue<T>(string tag)
		{
			try
			{
				object val = BlockRoutine.GetPropertyValue(_id, tag);
				return (T)val;
			}
			catch (Exception e)
			{
				throw new Exception("AcadBlockPresentationBase::GetPropertyValue::Get " + tag, e);
			}
		}

		/// <summary>
		/// Записать значение свойства, конвертированное из типа T
		/// </summary>
		/// <typeparam name="T">Тип, из которого значение свойства будет приведено в строку</typeparam>
		/// <param name="tag">Имя свойства</param>
		/// <param name="value">Значение для внесения в свойство</param>
		protected void SetPropertyValue<T>(string tag, T value)
		{
			try
			{
				BlockRoutine.SetPropertyValue(_id, tag, value);
			}
			catch (Exception e)
			{
				throw new Exception("AcadBlockPresentationBase::SetPropertyValue::Get " + tag, e);
			}
		}


		/// <summary>
		/// Установить визуальное состояние объекта, используя ассоциированное перечисление (Enum)
		/// </summary>
		/// <typeparam name="T">Тип перечисления</typeparam>
		/// <param name="value">Значение перечисления ассоциированного с визуальным состоянием объекта</param>
		/// <remarks> Пример ассоциированного перечисления смотрите в описании класса <see cref="BlockAdaptorBase"/></remarks>
		protected void SetVisibility<T>(T value)
		{
			try
			{
				string val = TypeConverterProvider.SpecificEnumConvertToString(value);
				SetPropertyValue("Visibility", val);
			}
			catch (Exception e)
			{
				throw new Exception("AcadBlockPresentationBase::SetVisibility", e);
			}
		}
		/// <summary>
		/// Получить визуальное состояние объекта, используя ассоциированное перечисление (Enum)
		/// </summary>
		/// <typeparam name="T">Тип перечисления</typeparam>
		/// <returns>Перечисление ассоциированное с визуальным состоянием объекта</returns>
		/// <remarks> Пример ассоциированного перечисления смотрите в описании класса <see cref="BlockAdaptorBase"/></remarks>
		protected T GetVisibility<T>()
		{
			try
			{
				string val = GetPropertyValue<string>("Visibility");
				return TypeConverterProvider.SpecificEnumConvertFromString<T>(val);
			}
			catch (Exception e)
			{
				throw new Exception("AcadBlockPresentationBase::GetVisibility", e);
			}
		}


		/// <summary>
		/// Прочесть точку, занесенную в свойства блока
		/// </summary>
		/// <param name="px">Имя координаты x точки.</param>
		/// <param name="py">Имя координаты y точки.</param>
		/// <returns>Считанная точка в системе координат модели</returns>
		/// <remarks>
		/// В динамическом блоке точка заноситься как два значения X и Y. Этот метод облегчает чтение пары
		///  и возвращает точку в системе координат модели.
		/// </remarks>
		protected Point3d ReadPointFromBlock(string px, string py)
		{
			Point3d tempPoint = ReadPointFromBlockWithoutTransform(px, py);
			return tempPoint.TransformBy(Transformation);
		}

		/// <summary>
		/// Прочесть точку, занесенную в свойства блока
		/// </summary>
		/// <param name="px">Имя координаты x точки.</param>
		/// <param name="py">Имя координаты y точки.</param>
		/// <returns>Считанная точка в системе координат блока</returns>
		/// <remarks>
		/// В динамическом блоке точка заноситься как два значения X и Y. Этот метод облегчает чтение пары
		///  и возвращает точку в системе координат блока.
		/// </remarks>
		protected Point3d ReadPointFromBlockWithoutTransform(string px, string py)
		{
			double pX = GetPropertyValue<double>(px);
			double pY = GetPropertyValue<double>(py);
			return new Point3d(pX, pY, 0);
		}


		#region IAcadBlockPresentation Members

		/// <summary>
		/// Получить идентификатор блока
		/// </summary>
		/// <value> Идентификатор блока AutoCAD  </value>
		public ObjectId ObjectId
		{
			get { return _id; }
		}

		/// <summary>
		/// Проверить действительность оболочки
		/// </summary>
		/// <value> <c>true</c> если оболочка действительна; иначе <c>false</c>.  </value>
		public bool IsValid
		{
			get { return _isValid; }
		}

		/// <summary>
		/// Инициализаци оболочки
		/// </summary>
		/// <param name="id">Идентификатор блока AutoCAD</param>
		public void Initialize(ObjectId id)
		{
			if (DBRoutine.CheckId(id))
			{
				_id = id;
				_isValid = true;
			}
		}

		/// <summary>
		/// Получить имя блока
		/// </summary>
		/// <value>Имя блока </value>
		public abstract string BlockName { get; }

		/// <summary>
		/// Получить или задать точку вставки блока
		/// </summary>
		/// <value> Точку вставки блока </value>
		public Point3d Origin
		{
			get { return BlockRoutine.GetBlockPosition(_id.Handle); }
			set { BlockRoutine.SetBlockPosition(_id.Handle, value); }
		}

		/// <summary>
		/// Матрица трансформации блока
		/// </summary>
		/// <value>Матрица трансформации </value>
		public Matrix3d Transformation
		{
			get { return BlockRoutine.GetBlockTransform(_id.Handle); }
			set { BlockRoutine.SetBlockTransform(_id.Handle, value); }
		}

		/// <summary>
		/// Получить систему координат блока
		/// </summary>
		/// <value>Система координат блока</value>
		public CoordinateSystem3d BlockCS
		{
			get
			{
				Matrix3d matrix =  Transformation;
				return new CoordinateSystem3d (
												Point3d.Origin.TransformBy(matrix),
                                                Vector3d.XAxis.TransformBy(matrix),
												Vector3d.YAxis.TransformBy(matrix));
			}

		}

		#region Хэширование

		private string GetPropertyString()
		{
			StringBuilder sb = new StringBuilder();
			try
			{
				using (EntityAdapter<BlockReference> oo = EntityProvider.GetEntityAdapter<BlockReference>(_id, OpenMode.ForRead))
				{
					if (oo.Object.IsDynamicBlock)
					{
						DynamicBlockReferencePropertyCollection col = oo.Object.DynamicBlockReferencePropertyCollection;
						foreach (DynamicBlockReferenceProperty p in col)
						{
							sb.Append(p.Value.ToString());
						}
					}
				}
			}
			catch (Exception ex)
			{
				EditorRoutine.WriteMessage(ex.ToString());
			}
			return sb.ToString();
		}

		private string GetAttributeString()
		{
			StringBuilder sb = new StringBuilder();
			try
			{
				using (Transaction trans = DBRoutine.StartTransaction())
				{
					BlockReference br = trans.GetObject(_id, OpenMode.ForRead) as BlockReference;
					if (br != null && br.IsDynamicBlock)
					{
						foreach (ObjectId id in br.AttributeCollection)
						{
							AttributeReference ar = trans.GetObject(id, OpenMode.ForRead) as AttributeReference;
							if (ar == null) continue;
							sb.Append(ar.TextString);
						}
					}
					trans.Commit();
				}
			}
			catch (Exception ex)
			{
				EditorRoutine.WriteMessage(ex.ToString());
			}
			return sb.ToString();
		}

		/// <summary>
		/// Получить хэш код свойств блока
		/// </summary>
		/// <returns>Хэш код свойств блока</returns>
		/// <remarks>
		/// Эти хэш коды помагают оследить в какой части блока произошли изменения: свойства, атрибуты или геометрия
		/// </remarks>
		public int GetBlockPropertyHashCode()
		{
			return GetPropertyString().GetHashCode();
		}

		/// <summary>
		/// Получить хэш код атрибутов блока
		/// </summary>
		/// <returns>Хэш код атрибутов блока</returns>
		/// <remarks>
		/// Эти хэш коды помагают оследить в какой части блока произошли изменения: свойства, атрибуты или геометрия
		/// </remarks>
		public int GetBlockAttributeHashCode()
		{
			return GetAttributeString().GetHashCode();
		}

		/// <summary>
		/// Получить хэш код атрибутов + свойств блока
		/// </summary>
		/// <returns>
		/// Хэш код атрибутов + свойств блока 
		/// </returns>
		/// <remarks>
		/// Не совпадает с хэшем блока AutoCAD взятого с BlockReference!
		/// </remarks>
		public override int GetHashCode()
		{
			string str = GetAttributeString() + GetPropertyString();
			return str.GetHashCode();
		}
		#endregion

		#endregion
	}
}
