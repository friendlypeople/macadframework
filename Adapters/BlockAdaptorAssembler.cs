// Copyright (c) 2012 Сергей Попов, Виталий Сайдин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Sergey Popov, Vitaliy Saydin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using Autodesk.AutoCAD.DatabaseServices;
using MAcadFramework.Aggregates;
using MAcadFramework.Routines;

namespace MAcadFramework.Adapters
{
	/// <summary>
	/// Класс отвечающий за сборку блока AutoCAD и его программной реализации
	/// </summary>
	/// <typeparam name="T">Тип реализации</typeparam>
	/// <example>
	/// Пример использования получения программной оболочки
	///  <code><![CDATA[
	/// // Дескриптор блока в базе AutoCAD
	/// Handle handle = GetHandleOfMyBlock();
	/// 
	/// MyBlockAdapter block = PresentationAssembler<MyBlockAdapter>.Assembly(handle);
	/// if (block.IsValid)
	/// {
	///		// Здесь работаю с оболочкой
	/// }
	/// else
	/// {
	///		// Обработка ошибки получения оболочки
	/// }
	/// ]]></code>
	/// </example>
	public sealed class BlockAdaptorAssembler<T> where T : IBlockAdapter, new()
	{
		/// <summary>
		/// Инициализировать оболочку существующим блоком. Проверка проходит по имени блока.
		/// </summary>
		/// <param name="id">Идентификатор объекта AutoCAD.</param>
		/// <returns>Оболочка над блоком</returns>
		public static T Assembly(ObjectId id)
		{
			T obj = new T();
			if (!DBRoutine.CheckId(id)) return obj;
			using (Transaction tr = DBRoutine.StartTransaction())
			{
				try
				{
					BlockReference br = EntityProvider.GetObjectPointer<BlockReference>(id);
					if (br != null)
					{
						if (br.IsDynamicBlock)
						{
							BlockTableRecord btr = EntityProvider.GetObjectPointer<BlockTableRecord>(br.DynamicBlockTableRecord);
							if (btr.Name.Equals(obj.BlockName))
								obj.Initialize(id);
						}
						else
						{
							if (br.Name.Equals(obj.BlockName))
								obj.Initialize(id);
						}
						tr.Commit();
					}
				}

				catch (Exception ex)
				{
					tr.Abort();
					EditorRoutine.WriteMessage(ex.ToString());
				}
			}
			return obj;
		}

		/// <summary>
		/// Инициализировать оболочку существующим блоком. Проверка проходит по имени блока.
		/// </summary>
		/// <param name="handle">Дескриптор объекта AutoCAD.</param>
		/// <returns>Оболочка над блоком</returns>
		public static T Assembly(Handle handle)
		{
			ObjectId id = DBRoutine.GetObjectId(handle);
			return Assembly(id);
		}
	}
}
