// Copyright (c) 2012 Сергей Попов, Виталий Сайдин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Sergey Popov, Vitaliy Saydin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections.Generic;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Interop;
using MAcadFramework.Routines;

namespace MAcadFramework.Adapters
{
	/// <summary>
	/// Класс оболочка над лайоутом
	/// </summary>
	public sealed class LayoutAdapter
	{
		#region Поля

		private readonly string		_name = String.Empty;
		private readonly ObjectId	_id = ObjectId.Null;
		private bool				_isValid;

		#endregion


		private LayoutAdapter(ObjectId id, string name)
		{
			_id = id;
			_name = name;
			_isValid = true;

		}

		/// <summary>
		/// Добавить объект на лайоут
		/// </summary>
		/// <param name="obj">Примитив для добавления</param>
		/// <returns>Идентификатор объекта добавленого на лайоут</returns>
		public ObjectId AddToPaperSpace(Entity obj)
		{
			return AddToPaperSpace(obj, DBRoutine.WorkingDatabase);
		}


		/// <summary>
		/// Добавить объект на лайоут
		/// </summary>
		/// <param name="obj">Примитив для добавления</param>
		/// <param name="tr">Текущая открытая транзакция AutoCAD</param>
		/// <returns>Идентификатор объекта добавленого на лайоут</returns>
		public ObjectId AddToPaperSpace(Entity obj, Transaction tr)
		{
			ObjectId addedObjectId = ObjectId.Null;
			try
			{
				// Объект Layout на чтение
				Layout layout = tr.GetObject(_id, OpenMode.ForRead) as Layout;
				if (layout != null)
				{
					// ID таблицы блоков для лайоута
					ObjectId btrId = layout.BlockTableRecordId;
					// Запись таблицы блоков, описывающая пространство лайоута
					BlockTableRecord btr = (BlockTableRecord)tr.GetObject(btrId, OpenMode.ForWrite, false);
					// Добавляем объект
					addedObjectId = btr.AppendEntity(obj);
					// Завершительные действия с вновь созданным объектом в транзакциях
					tr.AddNewlyCreatedDBObject(obj, true);
					// При внейшней базе возможно здесь ошибка
					tr.TransactionManager.QueueForGraphicsFlush();
				}
			}
			catch (Exception exc)
			{
				EditorRoutine.WriteMessage("Невозможно добавить объект в пространство листа " + _name);
				EditorRoutine.WriteMessage(exc.ToString());
			}
			return addedObjectId;
		}

		/// <summary>
		/// Добавить объект на лайоут
		/// </summary>
		/// <param name="obj">Примитив для добавления</param>
		/// <param name="extDatabase">База данных в которой находится лайоут</param>
		/// <returns>Идентификатор объекта добавленого на лайоут</returns>
		public ObjectId AddToPaperSpace(Entity obj, Database extDatabase)
		{
			// Рабочая база
			ObjectId addedObjectId = ObjectId.Null;
			if (extDatabase == null)
			{
				EditorRoutine.WriteMessage("Переданный параметр extDatabase == null");
				return addedObjectId;
			}
			TransactionManager tm = extDatabase.TransactionManager;
			using (Transaction myT = tm.StartTransaction())
			{
				addedObjectId = AddToPaperSpace(obj, myT);
				myT.Commit();
				return addedObjectId;
			}
		}

		/// <summary>
		/// Получить имя лайоута
		/// </summary>
		/// <value>Имя лайоута</value>
		public string Name
		{
			get { return _name; }
		}

		/// <summary>
		/// Получить идентификатор лайоута
		/// </summary>
		/// <value>Идентификатор лайоута</value>
		public ObjectId LayoutId
		{
			get { return _id; }
		}

		/// <summary>
		/// Получить флаг валидности
		/// </summary>
		/// <value> <c>true</c> если оболочка валидна, иначе <c>false</c>. </value>
		
		public bool IsValid
		{
			get { return _isValid; }
		}

		/// <summary>
		/// Получить текущий лайоут
		/// </summary>
		/// <value>Текущий лайоут</value>
		public static LayoutAdapter Current
		{
			get
			{
				try
				{
					LayoutManager layoutManager = LayoutManager.Current;
					string layoutName = layoutManager.CurrentLayout;

					ObjectId idLayout = layoutManager.GetLayoutId(layoutName);
					return new LayoutAdapter(idLayout, layoutName);
				}
				catch (Exception exc)
				{
					EditorRoutine.WriteMessage(exc.ToString());
				}
				return null;
			}
		}


		/// <summary>
		/// Создать оболочку на основании имени лайоута
		/// </summary>
		/// <param name="layoutName">Имя лайоута</param>
		/// <returns>Класс оболочка созданная на лайоуте или null</returns>
		public static LayoutAdapter Create(string layoutName)
		{
			try
			{
				LayoutManager layoutManager = LayoutManager.Current;

				ObjectId idLayout = layoutManager.GetLayoutId(layoutName);
				if (!DBRoutine.CheckId(idLayout))
					idLayout = layoutManager.CreateLayout(layoutName);

				return new LayoutAdapter(idLayout, layoutName);
			}
			catch (Exception exc)
			{
				EditorRoutine.WriteMessage(exc.ToString());
			}
			return null;
		}

		/// <summary>
		/// Проверить наличие лайоута в базе
		/// </summary>
		/// <param name="layoutName">Имя лайоута</param>
		/// <returns> <c>true</c> если операции выполнена успешна, иначе <c>false</c> </returns>
		public static bool CheckExists(string layoutName)
		{
			LayoutManager layoutManager = LayoutManager.Current;
			ObjectId idLayout = layoutManager.GetLayoutId(layoutName);
			return DBRoutine.CheckId(idLayout);
		}

		/// <summary>
		/// Сделать лайоут активным
		/// </summary>
		public void Activate()
		{
			try
			{
				// Менеджер лайоутов
				LayoutManager layoutManager = LayoutManager.Current;
				layoutManager.CurrentLayout = _name;
				EditorRoutine.Regen();
			}
			catch (Exception exc)
			{
				EditorRoutine.WriteMessage(exc.ToString());
			}
		}

		/// <summary>
		/// Очистить лайоут
		/// </summary>
		public void Clear()
		{
			try
			{
				List<Handle> objs = GetObjects();
				DBRoutine.EraseObjects(objs);
			}
			catch (Exception exc)
			{
				EditorRoutine.WriteMessage(exc.ToString());
			}
		}

		/// <summary>
		/// Удалить лайоут
		/// </summary>
		public void Delete()
		{
			try
			{
				LayoutManager layoutManager = LayoutManager.Current;
				layoutManager.DeleteLayout(_name);
				_isValid = false;
			}
			catch (Exception exc)
			{
				EditorRoutine.WriteMessage(exc.ToString());
			}
		}

		/// <summary>
		/// Зумирование лайоута
		/// </summary>
		public void ZoomExtents()
		{
			// Установка текущим лайоута по имени
			Activate();
			// Зуммирование на пространстве модели
			AcadApplication app = (AcadApplication)Autodesk.AutoCAD.ApplicationServices.Application.AcadApplication;
			app.ZoomExtents();
		}

		/// <summary>
		/// Получение объектов с лайоута
		/// </summary>
		/// <returns>Список объектов с лайуота</returns>
		public List<Handle> GetObjects()
		{
			// Рабочая база
			Database db = DBRoutine.WorkingDatabase;
			TransactionManager tm = db.TransactionManager;
			// Запускаем транзакцию
			using (Transaction myT = tm.StartTransaction())
			{
				List<Handle> objs = new List<Handle>(25);
				try
				{
					// Таблица блоков (на чтение)
					Layout layout = tm.GetObject(_id, OpenMode.ForRead) as Layout;
					if (layout != null)
					{
						BlockTableRecord btr = tm.GetObject(layout.BlockTableRecordId, OpenMode.ForRead, false) as BlockTableRecord;
						if (btr != null)
						{
							foreach (ObjectId id in btr)
								objs.Add(id.Handle);
						}
					}
					myT.Commit();
				}
				catch (Exception ex)
				{
					myT.Abort();
					EditorRoutine.WriteMessage(ex.ToString());
				}
				return objs;
			}
		}

	}
}
