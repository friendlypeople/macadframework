// Copyright (c) 2012 Сергей Попов, Виталий Сайдин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Sergey Popov, Vitaliy Saydin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;

namespace MAcadFramework.Adapters
{
	/// <summary>
    /// Класс оболчка Extents3d на плоскости
	/// </summary>
	public sealed class PlaneExtentsAdapter
	{
		private readonly Extents3d _extents;

        /// <summary>
        /// Инициализирует новый экземпляр класса Extents3d на плоскости.
        /// </summary>
        /// <param name="pt1">The PT1.</param>
        /// <param name="pt2">The PT2.</param>
		public PlaneExtentsAdapter(Point3d pt1, Point3d pt2)
		{
			_extents = new Extents3d(pt1, new Point3d(pt2.X, pt2.Y, pt1.Z));
		}

        /// <summary>
        /// Получает максимальную точку.
        /// </summary>
        /// <value>Максимальная точка.</value>
		public Point3d MaxPoint
		{
			get { return _extents.MaxPoint; }
		}

        /// <summary>
        /// Получает минимальную точку.
        /// </summary>
        /// <value>Минимальная точка.</value>
		public Point3d MinPoint
		{
			get { return _extents.MinPoint; }
		}

		/// <summary>
		/// Получить ширину границы
		/// </summary>
		/// <value>Ширина границы</value>
		public double Width
		{
			get { return MaxPoint.X - MinPoint.X; }
		}

		/// <summary>
		/// Получить высоту границы
		/// </summary>
		/// <value>Высота границы</value>
		public double Height
		{
			get { return MaxPoint.Y - MinPoint.Y; }
		}

		/// <summary>
		/// Центральная точка границы
		/// </summary>
		/// <value>Centre</value>
		public Point3d Centre
		{
			get
			{
				return new Point3d(MinPoint.X + Width / 2, MinPoint.Y + Height / 2 , MinPoint.Z);
			}
		}

		/// <summary>
		/// Получить левую нижнюю точку
		/// </summary>
		/// <value>Левая нижняя точка</value>
		public Point3d LeftBottom
		{
			get { return MinPoint; }
		}

		/// <summary>
		/// Получить левую верхнюю точку
		/// </summary>
		/// <value>Левая верхняя точка</value>
		public Point3d LeftTop
		{
			get { return new Point3d(MinPoint.X, MaxPoint.Y, MinPoint.Z); }
		}

		/// <summary>
		/// Получить правую нижнюю точку
		/// </summary>
		/// <value>Левая нижняя точка</value>
		public Point3d RightBottom
		{
			get { return new Point3d(MaxPoint.X, MinPoint.Y, MinPoint.Z); }
			
		}

		/// <summary>
		/// Получить правую верхнюю точку
		/// </summary>
		/// <value>Левая верхняя точка</value>
		public Point3d RightTop
		{
			get { return MaxPoint; }
		}

		/// <summary>
		/// Получить класс границы 3d AutoCAD
		/// </summary>
		/// <value>Класс границы 3d AutoCAD</value>
		public Extents3d Extents3d
		{
			get { return _extents; }
		}
	}

    /// <summary>
    /// Класс оболчка границы 3d AutoCAD, позволяющая получать доступ
	/// к разным плоскостям ограничивающего пространство куба
    /// </summary>
	/// <remarks>
	/// Сейчас реализован доступ только к фронтальной плоскости, как к наиболее используемой.
	/// Ждем с нетерпением желающих реализовать доступ к остальным плоскостям куба.
	/// </remarks>
	public sealed class Extents3dAdapter
	{
		private readonly Extents3d _extents;
		private readonly PlaneExtentsAdapter _frontPlane;

        /// <summary>
        /// Инициализирует новый экземпляр оболчки класса границы 3d AutoCAD.
        /// </summary>
        /// <param name="ext">Граница.</param>
		public Extents3dAdapter(Extents3d ext)
		{
			_extents = ext;
			_frontPlane = new PlaneExtentsAdapter(ext.MinPoint, ext.MaxPoint);
		}

		/// <summary>
		/// Получить границы фронтальной плоскости
		/// </summary>
		/// <value>FrontPlane</value>
		public PlaneExtentsAdapter FrontPlane
		{
			get { return _frontPlane; }
		}

        /// <summary>
        /// Получает максимальную точку.
        /// </summary>
        /// <value>Максимальная точка.</value>
		public Point3d MaxPoint
		{
			get { return _extents.MaxPoint; }
		}

        /// <summary>
        /// Получает минимальную точку.
        /// </summary>
        /// <value>Минимальная точка.</value>
		public Point3d MinPoint
		{
			get { return _extents.MinPoint; }
		}

		/// <summary>
		/// Получить класс границы 3d AutoCAD
		/// </summary>
		/// <value>Класс границы 3d AutoCAD</value>
		public Extents3d Extents3d
		{
			get { return _extents; }
		}

		/// <summary>
		/// Получить класс границы 2d AutoCAD
		/// </summary>
		/// <value>Класс границы 2d AutoCAD</value>
		public Extents2d Extents2d
		{
			get
			{
				return new Extents2d(new Point2d(MinPoint.X, MinPoint.Y),
				                     new Point2d(MaxPoint.X, MaxPoint.Y));
			}
		}
	}

}
