// Copyright (c) 2012 Сергей Попов, Виталий Сайдин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Sergey Popov, Vitaliy Saydin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using Autodesk.AutoCAD.Colors;
using Autodesk.AutoCAD.DatabaseServices;
using MAcadFramework.Aggregates;
using MAcadFramework.Routines;

namespace MAcadFramework.Adapters
{
	/// <summary>
    /// Класс для предотвращения ссылки на нулевой объект
    /// при ошибки инициализации класса LayerAdapter
    /// </summary>
    internal sealed class NullableLayer : ILayerAdapter
    {
        /// <summary>
        /// Получить флаг валидности.
        /// </summary>
        /// <value><c>true</c> если слой валидный; иначе, <c>false</c>.</value>
        public bool IsValid { get { return true; } }

		/// <summary>
		/// Активировать слой.
		/// </summary>
		/// <remarks>
		/// Активация слоя устанавливает параметры слоя <see cref="IsOff"/>, <see cref="IsFrozen"/> и <see cref="IsLocked"/>
		/// в значение <c>false</c>.
		/// </remarks>
        public void Activate() { }

		/// <summary>
		/// Деактивировать слой.
		/// </summary>
        public void Deactivate() { }

        /// <summary>
        /// Получить или задать значение, указывающее, выключен ли слой.
        /// </summary>
        /// <value><c>true</c> если слой выключен; иначе, <c>false</c>.</value>
        public bool IsOff
        {
            get { return false; }
            set { }
        }

        /// <summary>
        /// Получить или задать значение, указывающее, является ли слой скрытым.
        /// </summary>
        /// <value><c>true</c> если этот слой скрыт; иначе, <c>false</c>.</value>
        public bool IsHidden
        {
            get { return false; }
            set { }
        }

		/// <summary>
		/// Получить десриптор слоя.
		/// </summary>
		/// <value>Дескриптор слоя</value>
        public Handle Handle
        {
            get { return default(Handle); }
        }

        /// <summary>
        /// Получить или задать значение, указывающее, является ли данный слой замороженным.
        /// </summary>
        /// <value><c>true</c> если данный слой заморожен; иначе, <c>false</c>.</value>
        public bool IsFrozen
        {
            get { return false; }
            set { }
        }

        /// <summary>
        /// Получить или задать значение, указывающее, является ли данный слой заблокированым.
        /// </summary>
        /// <value><c>true</c> если слой заблокирован; иначе, <c>false</c>.</value>
        public bool IsLocked
        {
            get { return false; }
            set { }
        }

        /// <summary>
        /// Получить имя.
        /// </summary>
        /// <value>The name.</value>
        public string Name { get { return "0"; } }

        /// <summary>
        /// Получить или задать цвет.
        /// </summary>
        /// <value>The color.</value>
        public Color @Color
        {
            get { return Color.FromColorIndex(ColorMethod.ByLayer, 7); }
            set { }
        }

        /// <summary>
        /// Получить значение, указывающее, является ли данный слой стертым.
        /// </summary>
        /// <value><c>true</c> если данный слой стерт; иначе, <c>false</c>.</value>
        public bool IsErased { get { return true; } }

		/// <summary>
		/// Установить текущий слой.
		/// </summary>
		/// <returns><c>true</c> если операция успешна; иначе, <c>false</c></returns>
        public bool SetCurrent() { return false; }
    }

    /// <summary>
    /// Класс представления слоя
    /// </summary>
    public sealed class LayerAdapter : ILayerAdapter
    {
        private ObjectId _layerId = ObjectId.Null;
        private readonly string _layerName = String.Empty;

        /// <summary>
        /// Инициализирует новый экземпляр класса LayerAdapter.
        /// </summary>
        /// <param name="layerId">The layer id.</param>
        /// <param name="name">The name.</param>
        public LayerAdapter(ObjectId layerId, string name)
        {
            _layerName = name;
            _layerId = layerId;
        }

        /// <summary>
        /// Создать слой с указанным именем.
        /// </summary>
        /// <param name="name">Имя создаваемого слоя</param>
        /// <returns>Интерфейс оболочки</returns>
		/// <remarks>
		/// Если слой существует, то вернется оболочка на него. Если не существует, то будет
		/// создан и добавлен в базу.
		/// Если в процессе создания или доступа к слою произойдет ошибка, то флаг <see cref="IsValid"/>
		/// в оболочке будет установлен в значение <c>false</c>, а вызовы методов не приведут к ошибкам.
		/// </remarks>
        public static ILayerAdapter Create(string name)
        {
            using (EditorRoutine.LockDoc())
            {
                using (Transaction trans = DBRoutine.StartTransaction())
                {
                    // Получаем таблицу слоев
                    LayerTable layersTable =
                        (LayerTable)trans.GetObject(DBRoutine.WorkingDatabase.LayerTableId, OpenMode.ForWrite, false);
                    if (layersTable == null)
                    {
                        trans.Abort();
                        return new NullableLayer();
                    }

                    // Если есть в таблице слоев слой с именем...
                    LayerTableRecord layer;
                    if (!layersTable.Has(name))
                    {
                        layer = new LayerTableRecord();
                        layer.Name = name;
                        layersTable.Add(layer);
                        // Завершительные действия с транзакциями по закрытию вновь созданного объекта
                        trans.TransactionManager.AddNewlyCreatedDBObject(layer, true);
                    }
                    else
                    {
                        layer = trans.GetObject(layersTable[name], OpenMode.ForWrite) as LayerTableRecord;
                        if (layer == null)
                        {
                            trans.Abort();
                            return new NullableLayer();
                        }
                    }
                    trans.Commit();
                    return new LayerAdapter(layer.Id, layer.Name);
                }
            }
        }

        /// <summary>
        /// Получить значение валидности.
        /// </summary>
        /// <value><c>true</c> если слой валиднй; иначе, <c>false</c>.</value>
        public bool IsValid { get { return true; } }

        /// <summary>
        /// Активировать слой.
        /// </summary>
		/// <remarks>
		/// Активация слоя устанавливает параметры слоя <see cref="IsOff"/>, <see cref="IsFrozen"/> и <see cref="IsLocked"/>
		/// в значение <c>false</c>.
		/// </remarks>
        public void Activate()
        {
            using (EditorRoutine.LockDoc())
            {
                using (EntityAdapter<LayerTableRecord> ea = EntityProvider.GetEntityAdapter<LayerTableRecord>(_layerId, OpenMode.ForWrite))
                {
                    if (ea.Object.IsOff) ea.Object.IsOff = false;
                    if (ea.Object.IsFrozen) ea.Object.IsFrozen = false;
                    if (ea.Object.IsLocked) ea.Object.IsLocked = false;

                }
            }
        }

        /// <summary>
        /// Дизактивировать слой.
        /// </summary>
		/// <remarks>
		/// Активация слоя устанавливает параметры слоя <see cref="IsOff"/>, <see cref="IsFrozen"/> и <see cref="IsLocked"/>
		/// в значение <c>true</c>.
		/// </remarks>
        public void Deactivate()
        {
            using (EditorRoutine.LockDoc())
            {
                using (
                    EntityAdapter<LayerTableRecord> ea = EntityProvider.GetEntityAdapter<LayerTableRecord>(_layerId, OpenMode.ForWrite))
                {
                    if (!ea.Object.IsOff) ea.Object.IsOff = true;
                    if (!ea.Object.IsFrozen) ea.Object.IsFrozen = true;
                    if (!ea.Object.IsLocked) ea.Object.IsLocked = true;

                }
            }
        }

        /// <summary>
        /// Получить или задать значение, указывающее, выключен ли слой.
        /// </summary>
        /// <value><c>true</c> если данный слой выключен; иначе, <c>false</c>.</value>
        public bool IsOff
        {
            get
            {
                LayerTableRecord ltr = EntityProvider.GetObjectPointer<LayerTableRecord>(_layerId);
                return ltr.IsOff;
            }
            set
            {
                using (EditorRoutine.LockDoc())
                {
                    using (EntityAdapter<LayerTableRecord> ea = EntityProvider.GetEntityAdapter<LayerTableRecord>(_layerId, OpenMode.ForWrite))
                    {
                        ea.Object.IsOff = value;
                    }
                }
            }
        }

        /// <summary>
        /// Получить или задать значение, указывающее, является ли данный слой скрытым.
        /// </summary>
        /// <value><c>true</c> если данный слой скрыт; иначе, <c>false</c>.</value>
        public bool IsHidden
        {
            get
            {
                LayerTableRecord ltr = EntityProvider.GetObjectPointer<LayerTableRecord>(_layerId);
                return ltr.IsHidden;
            }
            set
            {
                using (EditorRoutine.LockDoc())
                {
                    using (EntityAdapter<LayerTableRecord> ea = EntityProvider.GetEntityAdapter<LayerTableRecord>(_layerId, OpenMode.ForWrite))
                    {
                        ea.Object.IsHidden = value;
                    }
                }
            }
        }

        /// <summary>
        /// Получить десриптор слоя.
        /// </summary>
        /// <value>Дескриптор слоя</value>
        public Handle Handle
        {
            get { return _layerId.Handle; }
        }

        /// <summary>
        /// Получить дескриптор слоя
        /// </summary>
        /// <value>Дескриптор слоя AutoCAD</value>
        public ObjectId LayerId
        {
            get { return _layerId; }
        }

        /// <summary>
        /// Получает или задает значение, указывающее, является ли данный экземпляр замороженные.
        /// </summary>
        /// <value><c>true</c> если слой заморожен; иначе, <c>false</c>.</value>
        public bool IsFrozen
        {
            get
            {
                LayerTableRecord ltr = EntityProvider.GetObjectPointer<LayerTableRecord>(_layerId);
                return ltr.IsFrozen;
            }
            set
            {
                using (EditorRoutine.LockDoc())
                {
                    using (EntityAdapter<LayerTableRecord> ea = EntityProvider.GetEntityAdapter<LayerTableRecord>(_layerId, OpenMode.ForWrite))
                    {
                        ea.Object.IsFrozen = value;
                    }
                }
            }
        }

        /// <summary>
        /// Получить или задать значение, указывающее, является ли данный слой заблокированным.
        /// </summary>
        /// <value><c>true</c> если слой заблокирован; иначе, <c>false</c>.</value>
        public bool IsLocked
        {
            get
            {
                LayerTableRecord ltr = EntityProvider.GetObjectPointer<LayerTableRecord>(_layerId);
                return ltr.IsLocked;
            }
            set
            {
                using (EditorRoutine.LockDoc())
                {
                    using (EntityAdapter<LayerTableRecord> ea = EntityProvider.GetEntityAdapter<LayerTableRecord>(_layerId, OpenMode.ForWrite))
                    {
                        ea.Object.IsLocked = value;
                    }
                }
            }
        }

        /// <summary>
        /// Получить имя слоя.
        /// </summary>
        /// <value>Имя слоя</value>
        public string Name
        {
            get { return _layerName; }
        }

        /// <summary>
        /// Получить или установить цвет.
        /// </summary>
        /// <value>The color.</value>
        public Color Color
        {
            get
            {
                LayerTableRecord ltr = EntityProvider.GetObjectPointer<LayerTableRecord>(_layerId);
                return ltr.Color;
            }
            set
            {
                using (EditorRoutine.LockDoc())
                {
                    using (EntityAdapter<LayerTableRecord> ea = EntityProvider.GetEntityAdapter<LayerTableRecord>(_layerId, OpenMode.ForWrite))
                    {
                        ea.Object.Color = value;
                    }
                }
            }
        }

        /// <summary>
        /// Получить значение, указывающее, стерт ли данный слой.
        /// </summary>
        /// <value><c>true</c> если слой стерт; иначе, <c>false</c>.</value>
        public bool IsErased
        {
            get
            {
                LayerTableRecord ltr = EntityProvider.GetObjectPointer<LayerTableRecord>(_layerId);
                return ltr.IsErased;
            }
        }

		/// <summary>
		/// Установить текущий слой.
		/// </summary>
		/// <returns><c>true</c> если операция успешна; иначе, <c>false</c></returns>
        public bool SetCurrent()
        {
            using (EditorRoutine.LockDoc())
            {
                if (DBRoutine.CheckId(_layerId))
                {
                    DBRoutine.WorkingDatabase.Clayer = _layerId;
                    return true;
                }
                return false;
            }
        }
    }

}
