// Copyright (c) 2012 Сергей Попов, Виталий Сайдин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Sergey Popov, Vitaliy Saydin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using Autodesk.AutoCAD.Runtime;
using System.Collections.Generic;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using MAcadFramework.Adapters;
using MAcadFramework.Aggregates;

namespace MAcadFramework.Routines
{
	/// <summary>
	/// Класс облегчающий работу с солидами, реализуя часто встречающиеся операции
	/// </summary>
    public static class Solid3dRoutine
    {
		/// <summary>
		/// Получить солиды из блока.
		/// </summary>
		/// <param name="id">Идентификатор блока AutoCAD</param>
		/// <returns> Список извлеченных блоков </returns>
		/// <remarks>При выполнении данной операции солиды, извлеченные из блока не добавляются в базу,
		/// а возвращается указатели на не связанные с базой объекты</remarks>
        public static List<Solid3d> ExportSolidsFromBlock(ObjectId id)
        {
            BlockReference br = EntityProvider.GetObjectPointer<BlockReference>(id.Handle);
            return ExportSolidsFromBlock(br);
        }

		/// <summary>
		/// Получить солиды из блока.
		/// </summary>
		/// <param name="id">Идентификатор блока AutoCAD</param>
		/// <param name="trans">Открытая транзакция AutoCAD</param>
		/// <returns> Список извлеченных блоков </returns>
		/// <remarks>
		/// При выполнении данной операции солиды, извлеченные из блока не добавляются в базу,
		/// а возвращается указатели на не связанные с базой объекты
		/// </remarks>
        public static List<Solid3d> ExportSolidsFromBlock(ObjectId id, Transaction trans)
        {
            BlockReference br = EntityProvider.GetObjectPointer<BlockReference>(id.Handle);
            return ExportSolidsFromBlock(br, trans);
        }

		/// <summary>
		/// Получить солиды из блока.
		/// </summary>
		/// <param name="bRef">Cсылка на блок AutoCAD</param>
		/// <returns> Список извлеченных блоков </returns>
		/// <remarks>
		/// При выполнении данной операции солиды, извлеченные из блока не добавляются в базу,
		/// а возвращается указатели на не связанные с базой объекты
		/// </remarks>
        public static List<Solid3d> ExportSolidsFromBlock(BlockReference bRef)
        {
            List<Solid3d> retList = new List<Solid3d>();
            using (SafelyCreater sc = new SafelyCreater())
            {
                DBObjectCollection entitySet = new DBObjectCollection();
                bRef.Explode(entitySet);
                sc.Push(entitySet);
                DBRoutine.EraseObject(bRef.Id);

                foreach (DBObject ent in entitySet)
                {
                    if (ent is Solid3d)
                    {
                        retList.Add(ent as Solid3d);
                        sc.Remove(ent);
                    }
                }
            }
            return retList;
        }

        /// <summary>
        /// Получить солиды из блока.
		/// <param name="bRef">Cсылка на блок AutoCAD</param>
		/// <param name="trans">Открытая транзакция AutoCAD</param>
        /// </summary>
		/// <returns> Список извлеченных блоков </returns>
		/// <remarks>
		/// При выполнении данной операции солиды, извлеченные из блока не добавляются в базу,
		/// а возвращается указатели на не связанные с базой объекты
		/// </remarks>
        public static List<Solid3d> ExportSolidsFromBlock(BlockReference bRef, Transaction trans)
        {
            List<Solid3d> retList = new List<Solid3d>();
            using (SafelyCreater sc = new SafelyCreater())
            {
                DBObjectCollection entitySet = new DBObjectCollection();
                bRef.Explode(entitySet);
                sc.Push(entitySet);
                DBRoutine.EraseObject(bRef.Id, trans);

                foreach (DBObject ent in entitySet)
                {
                    if (ent is Solid3d)
                    {
                        retList.Add(ent as Solid3d);
                        sc.Remove(ent);
                    }
                }
            }
            return retList;
        }

		/// <summary>
		/// Расчет объема солида
		/// </summary>
		/// <param name="handle">Дескриптор объекта AutoCAD</param>
		/// <returns>Объем солида</returns>
        public static double CalculateVolume(Handle handle)
        {
            try
            {
                Solid3d model = EntityProvider.GetObjectPointer<Solid3d>(handle);
                // Суммируем объемы (м3)
                return model.MassProperties.Volume / 1000000000;
            }
            catch (System.Exception ex)
            {
                EditorRoutine.WriteMessage(ex.ToString());
            }
            return 0;
        }

		/// <summary>
		/// Создание солида цилиндра в пространстве модели по двум точкам и радиусу
		/// </summary>
		/// <param name="pt1">Начало цилиндра</param>
		/// <param name="pt2">Конец цилиндра</param>
		/// <param name="radius">Радиус цилиндра</param>
		/// <param name="layerName">Имя слоя для размещения солида</param>
		/// <returns>Идентификатор созданного солида </returns>
        public static ObjectId MakeCylinder(Point3d pt1, Point3d pt2, double radius, string layerName)
        {
            // Вектор направления экструзии для цилиндра
            Vector3d extrusionVector = pt2 - pt1;
            // Окружность образующая цилиндр
            Circle circ = new Circle(pt1, extrusionVector, radius);

            // Коллекция объектов. Нужна для получения региона.
            DBObjectCollection circCol = new DBObjectCollection();
            circCol.Add(circ);

            // Получение объектов региона по коллекции объектов выше.
            DBObjectCollection regionCol = Region.CreateFromCurves(circCol);
            // Преобразование к типу RX
            RXObject rx = regionCol[0];
            // Преобразование к типу Region
            Region reg = (Region)rx;

            // Создание солида.
            Solid3d cyl = new Solid3d();
            // Вытягиваем регион по нашему вектору из 2х точек.
            cyl.Extrude(reg, extrusionVector.Length, 0);

            // Подчистим за собой. Удалим ненужные кружки, регионы
			SafelyCreater.Dispose(reg);
			SafelyCreater.Dispose(circ);

            if (!string.IsNullOrEmpty(layerName))
            {
                LayersRoutine.MakeLayer(layerName);
                cyl.Layer = layerName;
            }
            return DBRoutine.AddToModelSpace(cyl);
        }

		/// <summary>
		/// Создание цилиндра в пространстве модели по точке центра и вектору экструзии
		/// </summary>
		/// <param name="centerPt">Точка центра цилиндра</param>
		/// <param name="extrusionVector">Вектор экструзии</param>
		/// <param name="height">Высота цилиндра</param>
		/// <param name="radius">Радиус цилиндра</param>
		/// <param name="layerName">Имя слоя.</param>
		/// <returns> Идентификатор созданного солида</returns>
        public static ObjectId MakeCylinder(Point3d centerPt, Vector3d extrusionVector, double height, double radius, string layerName)
        {
            // Нормированный вектор экструзии
            Vector3d normExtrVector = extrusionVector.GetNormal();

            // Начальная и конечная точки цилиндра
            Point3d pt1 = centerPt - normExtrVector * height / 2;
            Point3d pt2 = centerPt + normExtrVector * height / 2;

            return MakeCylinder(pt1, pt2, radius, layerName);
        }
        
        /// <summary>
        /// Создание коробки солида в пространстве модели по 2м точкам и геометрии Box'а
        /// </summary>
        /// <param name="pt1">Начальная точка</param>
        /// <param name="pt2">Конечная точка</param>
        /// <param name="height">Высота</param>
        /// <param name="width">Ширина</param>
		/// <returns> Идентификатор созданного солида</returns>
        public static ObjectId MakeBox(Point3d pt1, Point3d pt2, double height, double width, string layerName)
        {
            // Вектор направления экструзии
            Vector3d extrusionVector = pt2 - pt1;

            // Создаем Box в начале системы координат
            Solid3d box = new Solid3d();
            box.CreateBox(extrusionVector.Length, width, height);

            // Новая тройка векторов XYZ
            Vector3d newVecX = extrusionVector.GetNormal();
            Vector3d newVecY = extrusionVector.GetNormal().GetPerpendicularVector();
            Vector3d newVecZ = newVecY.RotateBy(Math.PI / 2, extrusionVector);

            // Матрица преобразования
            Matrix3d mat = Matrix3d.AlignCoordinateSystem(
                /*from*/  Point3d.Origin, Vector3d.XAxis, Vector3d.YAxis, Vector3d.ZAxis,
                /*to*/    pt1 + newVecX * extrusionVector.Length / 2, newVecX, newVecY, newVecZ);

            // Преобразуем Box матрицей
            box.TransformBy(mat);

            if (!string.IsNullOrEmpty(layerName))
            {
                LayersRoutine.MakeLayer(layerName);
                box.Layer = layerName;
            }

            // Добавляем в модель
            return DBRoutine.AddToModelSpace(box);
        }

        /// <summary>
        /// Создание сферы
        /// </summary>
        /// <param name="pt">Координаты центра сферы</param>
        /// <param name="radius">Радиус сферы</param>
        /// <returns>Ссылка на созданный солид</returns>
		/// <remarks>
		/// При выполнении данной операции созданная сфера не добавляется в базу,
		/// а возвращается ссылка на не связанный с базой объект
		/// </remarks>
        public static Solid3d MakeSphere(Point3d pt, double radius)
        {
            // Создаем сферу в начале системы координат
            Solid3d sphere = new Solid3d();
            sphere.CreateSphere(radius);

            // Матрица преобразования
            Matrix3d mat = Matrix3d.Displacement(pt - Point3d.Origin);

            // Преобразуем сферу матрицей
            sphere.TransformBy(mat);

            return sphere;
        }

        /// <summary>
        /// Получить регионы с полилинии
        /// </summary>
        /// <param name="curve">Полилиния</param>
        /// <param name="regions">Список регионов полученных с кривой</param>
        /// <returns> <c>true</c> если операции выполнена успешна, иначе <c>false</c>  </returns>
        public static bool BuildRegionFromCurve(Curve curve, List<Region> regions)
        {
            if (curve == null || !curve.Closed) return false;

            DBObjectCollection dBobjs = new DBObjectCollection();
            dBobjs.Add(curve);
            DBObjectCollection tempReg = Region.CreateFromCurves(dBobjs);
            foreach (DBObject obj in tempReg)
                regions.Add((Region)obj);
            return regions.Count > 0;
        }

        /// <summary>
        /// Получить регионы с полилиний.
        /// </summary>
        /// <param name="regions">Регионы.</param>
        /// <param name="curves">Полилинии.</param>
        /// <returns></returns>
        public static bool BuildRegionFromCurves(List<Region> regions, params Curve[] curves)
        {
            if (curves == null || curves.Length == 0) return false;

            DBObjectCollection dBobjs = new DBObjectCollection();
            foreach (Curve crv in curves) dBobjs.Add(crv);

            DBObjectCollection tempReg = Region.CreateFromCurves(dBobjs);
            foreach (DBObject obj in tempReg)
                regions.Add((Region)obj);
            return regions.Count > 0;
        }

        /// <summary>
        /// Логические опреации над солидами
        /// </summary>
        /// <param name="srcSolid">Дескриптор исходного солида</param>
		/// <param name="tarSolid">Дескриптор солида для логической операции</param>
        /// <param name="type">Тип операции</param>
        /// <returns> <c>true</c> если операции выполнена успешна, иначе <c>false</c> </returns>
        public static bool BooleanOperation(Handle srcSolid, Handle tarSolid, BooleanOperationType type)
        {
            using (EntityAdapter<Solid3d> ea1 = EntityProvider.GetEntityAdapter<Solid3d>(srcSolid, OpenMode.ForWrite))
            {
                if (ea1 == null) return false;
                using (EntityAdapter<Solid3d> ea2 = EntityProvider.GetEntityAdapter<Solid3d>(tarSolid, OpenMode.ForWrite))
                {
                    if (ea2 == null) return false;
                    ea1.Object.BooleanOperation(type, ea2.Object);
                }
            }
            return true;
        }

        /// <summary>
        /// Вычитание одного солида из другого
        /// </summary>
        /// <param name="srcSolid">Солид из которого будет произведено вычитание</param>
        /// <param name="tarSolid">Вычитаемый солид</param>
        /// <returns> <c>true</c> если операции выполнена успешна, иначе <c>false</c> </returns>
        public static bool SubtractSolid(Handle srcSolid, Handle tarSolid)
        {
            using (EntityAdapter<Solid3d> ea1 = EntityProvider.GetEntityAdapter<Solid3d>(srcSolid, OpenMode.ForWrite))
            {
                if (ea1 == null) return false;
                using (EntityAdapter<Solid3d> ea2 = EntityProvider.GetEntityAdapter<Solid3d>(tarSolid, OpenMode.ForWrite))
                {
                    if (ea2 == null) return false;
                    if (ea1.Object.CheckInterference(ea2.Object))
                    {
                        try
                        {
                            ea1.Object.BooleanOperation(BooleanOperationType.BoolSubtract, ea2.Object);
                        }
                        catch
                        {
                            ea1.Abort();
                            ea2.Abort();
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        /// <summary>
        /// Вычитание одного солида из другого
        /// </summary>
        /// <param name="srcSolid">Солид из которого будет произведено вычитание, добавлен в базу</param>
        /// <param name="tarSolid">Вычитаемый солид, вычитаем клона</param>
        /// <returns> <c>true</c> если операции выполнена успешна, иначе <c>false</c> </returns>
        public static void SubtractSolid(Handle srcSolid, Solid3d tarSolid)
        {
            try
            {
                using (EntityAdapter<Solid3d> ea = EntityProvider.GetEntityAdapter<Solid3d>(srcSolid, OpenMode.ForWrite))
                {
                    if (ea.Object.CheckInterference(tarSolid))
                    {
                        Solid3d cms = EntityProvider.GetCloneObject(tarSolid);
                        ea.Object.BooleanOperation(BooleanOperationType.BoolSubtract, cms);
                        SafelyCreater.Dispose(cms);
                    }
                }

            }
            catch (System.Exception ex)
            {
                EditorRoutine.WriteMessage(ex.ToString());
            }
        }

        /// <summary>
        /// Вычитание одного солида из другого
        /// </summary>
        /// <param name="srcSolid">Солид из которого будет произведено вычитание, добавлен в базу</param>
        /// <param name="tarSolid">Вычитаемый солид, вычитаем клона</param>
        /// <param name="trans">Текущая транзакция AutoCAD</param>
        public static void SubtractSolid(Handle srcSolid, Solid3d tarSolid, Transaction trans)
        {
            try
            {
                Solid3d sol = trans.GetObject(DBRoutine.GetObjectId(srcSolid), OpenMode.ForWrite) as Solid3d;
                if (sol == null) return;
                if (sol.CheckInterference(tarSolid))
                {
                    Solid3d cms = EntityProvider.GetCloneObject(tarSolid);
                    sol.BooleanOperation(BooleanOperationType.BoolSubtract, cms);
                    SafelyCreater.Dispose(cms);
                }
            }
            catch (System.Exception ex)
            {
                EditorRoutine.WriteMessage(ex.ToString());
            }
        }

        /// <summary>
        /// Выдавить солид
        /// </summary>
        /// <param name="ent">Примитив, являющейся основой для выдавливания</param>
        /// <param name="height">Высота выдавливания</param>
        /// <returns>Полученный солид</returns>
		/// <remarks>
		/// При выполнении данной операции созданный солид не добавляется в базу,
		/// а возвращается ссылка на не связанный с базой объект
		/// </remarks>
        public static Solid3d Extrude(Entity ent, double height)
        {
            Solid3d ssol = new Solid3d();
            DBObjectCollection dBobjs = new DBObjectCollection();
            DBObjectCollection regions = null;
            dBobjs.Add(ent);
            try
            {
                regions = Region.CreateFromCurves(dBobjs);
                if (regions.Count < 1)
                    return null;
                ssol.Extrude((Region)regions[0], height, 0);
                SafelyCreater.Dispose(regions);
            }
            catch
            {
                SafelyCreater.Dispose(regions);
                return null;
            }
            return ssol;
        }

		/// <summary>
		/// Логические опреации над солидами
		/// </summary>
		/// <param name="srcSolid">Исходный солид</param>
		/// <param name="tarSolid">Солид для логической операции</param>
		/// <param name="type">Тип операции</param>
        /// <returns> <c>true</c> если операции выполнена успешна, иначе <c>false</c> </returns>
        public static bool BooleanOperation(Solid3d srcSolid, Solid3d tarSolid, BooleanOperationType type)
        {
            using (EntityAdapter<Solid3d> ea1 = EntityProvider.GetEntityAdapter(srcSolid, OpenMode.ForWrite))
            {
                if (ea1 == null) return false;
                using (EntityAdapter<Solid3d> ea2 = EntityProvider.GetEntityAdapter(tarSolid, OpenMode.ForWrite))
                {
                    if (ea2 == null) return false;
                    try
                    {
                        ea1.Object.BooleanOperation(type, ea2.Object);
                    }
                    catch { return false; }
                }
            }
            return true;
        }

        /// <summary>
        /// Разбить солиды на примитивные составляющие, являющиеся образующими регионов - кривые
        /// </summary>
        /// <param name="solid">Солид для разбивки</param>
        /// <returns>Набор объектов полученный в процессе разбивки</returns>
		/// <remarks>
		/// Объекты полученные в процессе разбиения солида не добавляются в базу, Поэтому для них необходимо вызывать
		/// метод Dispose() или <see cref="MAcadFramework.Aggregates.SafelyCreater"/>
		/// </remarks>
        public static DBObjectCollection ExplodeSolid(Solid3d solid)
        {
            // Делаем копию солида для разбивки
            Solid3d solidForExplode = solid.Clone() as Solid3d;
            DBObjectCollection retCollection = new DBObjectCollection();
            if (solidForExplode == null) return retCollection;

            // Разбиваем солид
            DBObjectCollection solidExpObjects = new DBObjectCollection();
            solidForExplode.Explode(solidExpObjects);

            // Цикл по составляющим объектам
            foreach (DBObject expObject in solidExpObjects)
            {
                // Если объект - регион, то 
                if (expObject is Region)
                {
                    // Разбиваем ф-ией разбития региона
                    DBObjectCollection explodedRegObjs = GeometryRoutine.ExplodeRegion(expObject as Region);
                    // Сохраняем в выходной коллекции
                    foreach (DBObject ourObj in explodedRegObjs)
                    {
                        retCollection.Add(ourObj);
                    }
                }
                // Если поверхность, то разбиваем на регионы и как выше описано
                else if (expObject is Autodesk.AutoCAD.DatabaseServices.Surface)
                {
                    Autodesk.AutoCAD.DatabaseServices.Surface surf = expObject as Autodesk.AutoCAD.DatabaseServices.Surface;
                    // Конвертируем поверхность в регионы
                    try
                    {
                        //Entity[] regions = surf.ConvertToRegion();
                        DBObjectCollection objects = new DBObjectCollection();
                        surf.Explode(objects);
                        if (objects != null)
                        {
                            // Далее разбиваем регионы
                            foreach (DBObject obj in objects)
                            {
                                if (obj is Region)
                                {
                                    DBObjectCollection explodedRegObjs = GeometryRoutine.ExplodeRegion(obj as Region);
                                    foreach (DBObject ourObj in explodedRegObjs)
                                    {
                                        retCollection.Add(ourObj);
                                    }
                                }
                                else if (obj is Curve)
                                {
                                    retCollection.Add(obj);
                                    continue;
                                }
                                SafelyCreater.Dispose(obj);
                            }
                        }
                    }
                    catch (System.Exception exc)
                    {
                        EditorRoutine.WriteMessage("Solid3dRoutine.ExplodeSolid.exc: " + exc.Message + "\n" + exc.StackTrace);
                        System.Diagnostics.Debug.WriteLine("Solid3dRoutine.ExplodeSolid.exc: " + exc.Message + "\n" + exc.StackTrace);
                    }
                }
            }
            SafelyCreater.Dispose(solidExpObjects);

            return retCollection;
        }

        /// <summary>
        /// Получить набор точек на поверхности солида
        /// </summary>
        /// <param name="solid">Солид для работы</param>
        /// <returns>Набор точек на поверхности солида</returns>
        public static Point3dCollection GetSomePointsAtSolid(Solid3d solid)
        {
            // Результирующий набор точек
            Point3dCollection pts = new Point3dCollection();
            if (solid == null) return pts;

            // Разбиваем солид на примитивы
            DBObjectCollection solidExpObjects = ExplodeSolid(solid);
            // Цикл по всем составляющим
            foreach (DBObject obj in solidExpObjects)
            {
                Curve curve = obj as Curve;
                if (curve == null) continue;

                // Получаем с кривой 3 точки: Начало, Конец, По половине длины
                Point3d startPt = curve.StartPoint;
                Point3d endPt = curve.EndPoint;
                double startDist = curve.GetDistAtPoint(startPt);
                double endDist = curve.GetDistAtPoint(endPt);
                double midDist = (startDist + endDist) / 2;
                Point3d midPt = curve.GetPointAtDist(midDist);

                pts.Add(startPt);
                pts.Add(endPt);
                pts.Add(midPt);
            }
            SafelyCreater.Dispose(solidExpObjects);

            return pts;
        }

        /// <summary>
        /// Разрезать солид
        /// </summary>
        /// <param name="solid">Исходный солид</param>
        /// <param name="surface">Поверхность для рассечения</param>
        /// <param name="normals">Нормали</param>
        /// <returns></returns>
        public static Solid3d GetSolidSlice(Solid3d solid, Autodesk.AutoCAD.DatabaseServices.Surface surface, Vector3d[] normals)
        {
            if (solid == null || surface == null) return null;
            Solid3d cloneSolid = solid.Clone() as Solid3d;
            if (cloneSolid == null) return null;

            // Получаем 2 куска солида рассеченные поверхностью
            Solid3d otherSlice = cloneSolid.Slice(surface, true);

            Point3dCollection pointsOnSolid = GetSomePointsAtSolid(solid);

            // ВНИМАНИЕ!!! ДОДЕЛАТЬ
            SafelyCreater.Dispose(otherSlice);


            return cloneSolid;
        }
    }
}

