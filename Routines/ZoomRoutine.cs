// Copyright (c) 2012 Сергей Попов, Виталий Сайдин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Sergey Popov, Vitaliy Saydin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.Interop;
using Autodesk.AutoCAD.Interop.Common;
using MAcadFramework.Aggregates;

namespace MAcadFramework.Routines
{
	/// <summary>
	/// Класс облегчающий работу с зумированием чертежа, реализуя часто встречающиеся операции
	/// </summary>
	public static class ZoomRoutine
	{
		/// <summary>
		/// Применить, используя границы чертежа.
		/// </summary>
		/// <remarks>
		/// Этот метод можно применять для лайоутов, так как он использует
		/// COM классы для работы
		/// </remarks>
		public static void Zoom()
		{
			// Зуммирование на пространстве модели
			AcadApplication app = (AcadApplication)Application.AcadApplication;
			app.ZoomExtents();
		}

    	/// <summary>
        /// Применить используя дескриптор объекта AutoCAD.
        /// </summary>
        /// <param name="handle">Дескриптор объекта AutoCAD</param>
        /// <param name="scale">Масштаб.</param>
		/// <remarks>
		/// Этот метод использует механизм видов и не может быть
		/// использован для лайоутов
		/// </remarks>
        public static void Apply(Handle handle, double scale)
    	{
    	    Entity ent = EntityProvider.GetObjectPointerNoException<Entity>(handle);
    	    if (ent == null) return;
    	    Extents3d ext = ent.GeometricExtents;
    	    Point2d min2D = new Point2d(ext.MinPoint.X, ext.MinPoint.Y);
    	    Point2d max2D = new Point2d(ext.MaxPoint.X, ext.MaxPoint.Y);
            ViewTableRecord view = CreateView();

    	    view.CenterPoint = min2D + ((max2D - min2D)/2.0);
    	    view.Height = (max2D.Y - min2D.Y)/scale;
    	    view.Width = (max2D.X - min2D.X)/scale;
    	    EditorRoutine.Editor.SetCurrentView(view);
    	    SafelyCreater.Dispose(view);
    	}

        /// <summary>
        /// Создание вида, независимо от активного пространства.
        /// </summary>
	    private static ViewTableRecord CreateView()
	    {
	        ViewTableRecord view = new ViewTableRecord();

	        short res = SystemVariable.GetSystemVariable<short>(SystemVariable.TileMode);
	        if (res == 0)
	            view.IsPaperspaceView = true;
	        return view;
	    }

	    /// <summary>
        /// Применить используя границы области.
        /// </summary>
        /// <param name="ext">Границы области.</param>
		/// <remarks>
		/// Этот метод использует механизм видов и не может быть
		/// использован для лайоутов
		/// </remarks>
		public static void Apply(Extents3d ext)
		{
			Point2d min2D = new Point2d(ext.MinPoint.X, ext.MinPoint.Y);
			Point2d max2D = new Point2d(ext.MaxPoint.X, ext.MaxPoint.Y);
            ViewTableRecord view = CreateView();

			view.CenterPoint = min2D + ((max2D - min2D) / 2.0);
			view.Height = max2D.Y - min2D.Y;
			view.Width = max2D.X - min2D.X;
			EditorRoutine.Editor.SetCurrentView(view);
		}

        /// <summary>
        /// Применить используя min и max.
        /// </summary>
        /// <param name="min">Координаты левого нижнего угла</param>
		/// <param name="max">Координаты верхнего правого угла</param>
		/// <remarks>
		/// Этот метод использует механизм видов и не может быть
		/// использован для лайоутов
		/// </remarks>
		public static void Apply(Point3d min, Point3d max)
		{
			Point2d min2D = new Point2d(min.X, min.Y);
			Point2d max2D = new Point2d(max.X, max.Y);
            ViewTableRecord view = CreateView();

			view.CenterPoint = min2D + ((max2D - min2D) / 2.0);
			view.Height = max2D.Y - min2D.Y;
			view.Width = max2D.X - min2D.X;
			EditorRoutine.Editor.SetCurrentView(view);
		}

        /// <summary>
        /// Применить используя центральную точку.
        /// </summary>
        /// <param name="cPoint">Центральная точка.</param>
        /// <param name="zoomHeight">Высота зума.</param>
		public static void Apply(Point3d cPoint, double zoomHeight)
		{
            ViewTableRecord view = CreateView();
			view.CenterPoint = new Point2d(cPoint.X, cPoint.Y);
			view.Height = zoomHeight;
			view.Width = zoomHeight;
			EditorRoutine.Editor.SetCurrentView(view);
		}

		/// <summary>
		/// Зумировать, использую точку как центр
		/// </summary>
		/// <param name="viewCenter">Центр зума</param>
		/// <remarks>
		/// Этот метод можно применять для лайоутов, так как он использует
		/// COM классы для работы
		/// </remarks>
		public static void Zoom(Point3d viewCenter)
		{
			const double zoomHeight = 100.0;
			Zoom(viewCenter, zoomHeight);
		}

		/// <summary>
		/// Зумировать, использую точку как центр
		/// </summary>
		/// <param name="viewCenter">Центр зума</param>
		/// <param name="zoomHeight">Высота экрана зума.</param>
		/// <remarks>
		/// Этот метод можно применять для лайоутов, так как он использует
		/// COM классы для работы
		/// </remarks>
		public static void Zoom(Point3d viewCenter, double zoomHeight)
		{
			AcadApplication app = (AcadApplication)Application.AcadApplication;
			app.ZoomCenter(viewCenter.ToArray(), zoomHeight);
		}

		/// <summary>
		/// Зумировать по объекту AutoCAD 
		/// </summary>
		/// <param name="id">Идентификатор объекта по которму нужно провести зум</param>
		/// <param name="zoomFactor">Масштаб зумирования.</param>
		/// <remarks>
		/// Этот метод можно применять для лайоутов, так как он использует
		/// COM классы для работы
		/// </remarks>
		public static void Zoom(ObjectId id, double zoomFactor)
		{
			Database db = HostApplicationServices.WorkingDatabase;
			AcadApplication app = (AcadApplication)Application.AcadApplication;
			object oMinpt = new object(), oMaxpt = new object();

			using (Transaction tr = db.TransactionManager.StartTransaction())
			{
				Entity en = (Entity)tr.GetObject(id, OpenMode.ForRead, false);
				AcadEntity enCom = en.AcadObject as AcadEntity;
				if (enCom != null) enCom.GetBoundingBox(out oMinpt, out oMaxpt);
				tr.Commit();
			}
			app.ZoomWindow(oMinpt, oMaxpt);
		}
	}
}
