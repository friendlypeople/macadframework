// Copyright (c) 2012 Сергей Попов, Виталий Сайдин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Sergey Popov, Vitaliy Saydin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Threading;
using Autodesk.AutoCAD.Colors;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.GraphicsSystem;
using Celsio.Patterns;
using MAcadFramework.Aggregates;

namespace MAcadFramework.Routines
{
	/// <summary>
	/// Класс осуществляющий мигающую подсветку примитивом AutoCAD
	/// </summary>
	/// <remarks>
	/// <para>
	/// Эффект достигается за счет смены подсвеченного состояния объекта (higlighting)
	/// и неподсвеченного (unhiglighting) на указанные промежутки времени.
	/// </para>
	/// <para>
	/// Этот метод вызывает блокировку вызвашего его потока на временной интервал равный
	/// (delay1 + delay2)*num, где num - количество миганий. <seealso cref="Run"/>
	/// </para>
	/// </remarks>
	/// <example>
	/// <code><![CDATA[
	/// // Запускаем мигание прмитива
	/// // количество миганий
	/// const int num = 5;
	/// // время влюченной подсветки
	/// const int delay1 = 200;
	/// // время выключенной подсветки
	/// const int delay2 = 200;
	/// 
	/// FlickHighlightEntity fhe = new FlickHighlightEntity(handle);
	/// fhe.Run(num, delay1, delay2);
	/// 
	/// ]]></code>
	/// </example>
	public class FlickHighlightEntity
	{
		/// <summary>
		/// Идентификатор примитива AutoCAD
		/// </summary>
		private readonly ObjectId _id;

		/// <summary>
		/// Создание экземпляра класса <see cref="FlickHighlightEntity"/>.
		/// </summary>
		/// <param name="handle">Дескриптор объекта AutoCAD</param>
		public FlickHighlightEntity(Handle handle)
		{
			_id = DBRoutine.GetObjectId(handle);
		}

		/// <summary>
		/// Создание экземпляра класса <see cref="FlickHighlightEntity"/>.
		/// </summary>
		/// <param name="id">Идентификатор объекта AutoCAD</param>
		public FlickHighlightEntity(ObjectId id)
		{
			_id = id;
		}

        /// <summary>
        /// Включить мигающую подсветку.
        /// </summary>
        /// <param name="num">Количество миганий.</param>
        /// <param name="delay1">Продолжительность подсвеченного состояния в миллисекундах.</param>
		/// <param name="delay2">Продолжительность выключенного состояния в миллисекундах.</param>
		public void Run(int num, int delay1, int delay2)
		{
			
			for (int i = 0; i < num; i++)
			{
				// Подсветить примитив
				using (EditorRoutine.LockDoc())
				{
					using (Transaction tr = DBRoutine.StartTransaction())
					{
						Entity en = (Entity)tr.GetObject(_id, OpenMode.ForWrite);
						ObjectId[] ids = new ObjectId[1];
						ids[0] = _id;
						SubentityId index = new SubentityId(SubentityType.Null, 0);
						FullSubentityPath path = new FullSubentityPath(ids, index);
						en.Highlight(path, true);
						tr.Commit();
					}
				}
				EditorRoutine.Update();
                // Ожидание delay1 миллисекунд
				Thread.Sleep(delay1);
                // Снять подсветку с примитива
				using (EditorRoutine.LockDoc())
				{
					using (Transaction tr = DBRoutine.StartTransaction())
					{
						Entity en = (Entity)tr.GetObject(_id, OpenMode.ForWrite);
						ObjectId[] ids = new ObjectId[1]; ids[0] = _id;
						SubentityId index = new SubentityId(SubentityType.Null, 0);
						FullSubentityPath path = new FullSubentityPath(ids, index);
						en.Unhighlight(path, true);
						tr.Commit();
					}
				}
				EditorRoutine.Update();
				// Ожидание delay2 миллисекунд
				Thread.Sleep(delay2);
			}
		}

	}

    /// <summary>
	/// Класс осуществляющий подсветку примитива 
    /// </summary>
	/// <remarks>
	/// <para>
	/// Примитив, подсвеченный (highlighting) с помощью этого класса, автоматически
	/// перейдет в неподсвеченное состояние (unhiglighting) через укзанный промежуток времени
	/// </para>
	/// <para>
	/// Этот метод не вызывает блокировку вызвашего его потока, а снятие подсветки происходит
	/// в отдельном потоке.
	/// </para>
	/// </remarks>
	/// <example>
	/// <code><![CDATA[
	/// // Запускаем подсветку прмитива
	/// // время влюченной подсветки
	/// const int delay = 3;
	/// 
	/// HiglightEntity he = new HiglightEntity(handle);
	/// he.Run(delay);
	/// 
	/// ]]></code>
	/// </example>
	public class HiglightEntity
	{
		/// <summary>
		/// Идентификатор примитива AutoCAD
		/// </summary>
		private readonly ObjectId _id;
		private int _delay;

		/// <summary>
		/// Создание экземпляра класса <see cref="HiglightEntity"/>.
		/// </summary>
		/// <param name="handle">Дескриптор объекта AutoCAD</param>
		public HiglightEntity(Handle handle)
		{
			_id = DBRoutine.GetObjectId(handle);
		}

		/// <summary>
		/// Создание экземпляра класса <see cref="HiglightEntity"/>.
		/// </summary>
		/// <param name="id">Идентификатор объекта AutoCAD</param>
		public HiglightEntity(ObjectId id)
		{
			_id = id;
		}

        /// <summary>
        /// Включить подсветку.
        /// </summary>
        /// <param name="delay">Задержка в секундах.</param>
		public void Run(int delay)
		{
			_delay = delay;
			Entity ent = EntityProvider.GetObjectPointerNoException<Entity>(_id);
			if (ent == null) return;
			ent.Highlight();
			Thread thr = new Thread(ExecuteThread);
			thr.Start();
		}

        /// <summary>
        /// Впомогательный поток, снимающий подсветку
        /// </summary>
		private void ExecuteThread()
		{
			Entity ent = EntityProvider.GetObjectPointerNoException<Entity>(_id);
			if (ent == null) return;
			ManualResetEvent mre = new ManualResetEvent(false);
			mre.WaitOne(Convert.ToInt32(_delay) * 1000, true);
			ent.Unhighlight();
		}
	}

    /// <summary>
    /// Класс, изменяющий стандартный стиль подсветки примитивов
	/// AutoCAD, на указанный
    /// </summary>
	/// <example>
	/// <code><![CDATA[
	/// using (AcGsHighlight hl = new AcGsHighlight())
    /// {
    ///		try
    ///		{
	///			hl.LineWeight = 2;
    ///         hl.Pattern = Autodesk.AutoCAD.GraphicsSystem.LinePattern.Solid;
	///         // Тут подсвечиваем объекты
	///         
    ///     }
    ///     catch (Exception ex)
	///     {
	///        // Обработка исключений
    ///     }
	///}
	/// ]]></code>
	/// </example>
    public class AcGsHighlight : SimpleDisposable
    {
		private readonly ushort			_originalColor;
		private readonly ushort			_originalLineweight;
		private readonly LinePattern	_originalLinePattern;

		/// <summary>
		/// Создание экземпляра класса <see cref="AcGsHighlight"/>.
		/// </summary>
        public AcGsHighlight()
        {
			_originalColor			= acedFunction.acgsGetHighlightColor();
			_originalLinePattern	= acedFunction.acgsGetHighlightLinePattern();
			_originalLineweight		= acedFunction.acgsGetHighlightLineWeight();
        }

        /// <summary>
		/// Получить или задать цвет подсветки.
        /// </summary>
		/// <value>Цвет подсветки</value>
		/// <remarks>
		/// поле ColorMethod у Color должно быть установлено в olorMethod.ByAci,
		/// в противном случае возбуждается исключение <see cref="ArgumentException"/>
		/// </remarks>
		/// <exception cref="ArgumentException" />
        public Color Color
        {
            get { return Color.FromColorIndex(ColorMethod.ByAci, (short)acedFunction.acgsGetHighlightColor()); }
            set
            {
                if (value.ColorMethod != ColorMethod.ByAci)
                    throw new ArgumentException("Invalid ColorMethod");
                acedFunction.acgsSetHighlightColor(Convert.ToUInt16(value.ColorIndex));
            }
        }

        /// <summary>
        /// Получить или установить шаблон линии.
        /// </summary>
        /// <value>Шаблон линии</value>
        public LinePattern Pattern
        {
            get { return acedFunction.acgsGetHighlightLinePattern(); }
            set { acedFunction.acgsSetHighlightLinePattern(value); }
        }

        /// <summary>
        /// Получить или установить толщину линии.
        /// </summary>
        /// <value>Толщина линии.</value>
        public ushort LineWeight
        {
            get { return Convert.ToUInt16(acedFunction.acgsGetHighlightLineWeight()); }
			set { acedFunction.acgsSetHighlightLineWeight(Convert.ToUInt16(value)); }
        }

        /// <summary>
        /// Восстановить исходную подсветку
        /// </summary>
        private void Restore()
        {
          acedFunction.acgsSetHighlightColor(_originalColor);
          acedFunction.acgsSetHighlightLineWeight(_originalLineweight);
		  acedFunction.acgsSetHighlightLinePattern(_originalLinePattern);
        }

        /// <summary>
        /// Вызывается при освобождении ресурсов (Dispose)
        /// </summary>
        protected override void OnDispose()
        {
            Restore();
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Вызывается при финализации (Finalize)
        /// </summary>
        protected override void OnFinalize() { }

    }
}
