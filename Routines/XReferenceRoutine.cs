// Copyright (c) 2012 Сергей Попов, Виталий Сайдин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Sergey Popov, Vitaliy Saydin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.IO;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using MAcadFramework.Aggregates;

namespace MAcadFramework.Routines
{
    /// <summary>
    /// Класс облегчающий работу со внешними ссылками чертежа
    /// </summary>
    public class XReferenceRoutine
    {
        /// <summary>
        /// Добавить в документ внешнюю ссылку
        /// </summary>
        /// <param name="filename">Путь к файлу</param>
        /// <param name="blockName">Имя блока</param>
        /// <returns>Id записи таблицы блоков</returns>
        public static ObjectId AddToDocument(string filename, string blockName)
        {
            if (string.IsNullOrEmpty(blockName)) return ObjectId.Null;
            ObjectId btrId = BlockRoutine.GetBlockId(blockName);
            // Если запись таблицы блока с таким именем уже существует, то не добавляем dwg-ссылку
            if (btrId.IsValid) return ObjectId.Null;
            Document document = Application.DocumentManager.MdiActiveDocument;

            try
            {
                ObjectId id = document.Database.AttachXref(filename, blockName);
                return id;
            }
            catch (Exception) { }
            return ObjectId.Null;
        }

        ///// <summary>
        ///// Добавить в документ внешнюю ссылку
        ///// </summary>
        ///// <param name="filename">Путь к файлу</param>
        ///// <returns>Id записи таблицы блоков</returns>
        //public static ObjectId AddToDocument(string filename)
        //{
        //    string blockName = Path.GetFileNameWithoutExtension(filename);
        //    return AddToDocument(filename, blockName);
        //}

        /// <summary>
        /// Получить путь к файлу внешней ссылки
        /// </summary>
        /// <param name="blockId">Id блока внешней ссылки</param>
        /// <returns>Путь к файлу</returns>
        public static string GetFilename(ObjectId blockId)
        {
            string filepath = string.Empty;
            using (Transaction tr = DBRoutine.StartTransaction())
            {
                try
                {
                    BlockTableRecord block = (BlockTableRecord)tr.GetObject(blockId, OpenMode.ForRead, false);
                    filepath = block.PathName;
                }
                catch { tr.Abort(); }
            }
            return filepath;
        }

        /// <summary>
        /// Получить путь к файлу внешней ссылки
        /// </summary>
        /// <param name="blockName">Имя блока внешней ссылки</param>
        /// <returns>Путь к файлу</returns>
        public static string GetFilename(string blockName)
        {
            ObjectId id = BlockRoutine.GetBlockId(blockName);
            return GetFilename(id);
        }

        /// <summary>
        /// Импортировать объекты
        /// </summary>
        /// <param name="filename">Имя файла</param>
        /// <returns>Коллекция полученных id объектов</returns>
        private static ObjectIdCollection ImportObjectsFromFile(string filename)
        {
            ObjectIdCollection col = new ObjectIdCollection();
            try
            {
                using (Database database = new Database(false, true))
                {
                    database.ReadDwgFile(filename, FileShare.Read, false, null);
                    ObjectsImporter importer = new ObjectsImporter(database, DBRoutine.WorkingDatabase);
                    IdMapping mapping = importer.FullTransfer();
                    foreach (IdPair pair in mapping)
                    {
                        if(pair.IsCloned)
                            col.Add(pair.Value);
                    }
                }
            }
            catch (Exception) { }
            return col;
        }

        /// <summary>
        /// Импортировать объекты
        /// </summary>
        /// <param name="blockId">Имя блока внешней ссылки</param>
        /// <returns>Коллекция полученных id объектов</returns>
        public static ObjectIdCollection ImportObjects(ObjectId blockId)
        {
            string filepath = GetFilename(blockId);
            return ImportObjectsFromFile(filepath);
        }

        /// <summary>
        /// Импортировать объекты
        /// </summary>
        /// <param name="blockName">Id блока внешней ссылки</param>
        /// <returns>Коллекция полученных id объектов</returns>
        public static ObjectIdCollection ImportObjects(string blockName)
        {
            string filepath = GetFilename(blockName);
            return ImportObjectsFromFile(filepath);
        }

        /// <summary>
        /// Создать внешнюю ссылку
        /// </summary>
        /// <param name="collection">Коллекция id объектов, из которых необходимо сделать внешнюю ссылку</param>
        /// <param name="filename">Путь к файлу с внешней ссылкой</param>
        /// <param name="blockName">Имя блока</param>
        /// <param name="recreate">Удалять объекты и заменять их внешней ссылкой</param>
        public static ObjectId CreateXref(ObjectIdCollection collection, string filename, string blockName, bool recreate)
        {
            try
            {
                Database srcDb = DBRoutine.WorkingDatabase;
                DocumentCollection docs = Application.DocumentManager;
                Document curDoc = docs.Add("acad.dwt");
                Database dstDb = curDoc.Database;

                using (curDoc.LockDocument())
                {
                    ObjectsImporter importer = new ObjectsImporter(srcDb, dstDb);
                    importer.PartialTransfer(collection);
                }
#if Acad2008
                dstDb.SaveAs(filename, DwgVersion.Current);
#else
				dstDb.SaveAs(filename, true, DwgVersion.Current, null);
#endif
                curDoc.CloseAndDiscard();

                ObjectId id = AddToDocument(filename, blockName);
                if (!recreate)
                    return id;

                DBRoutine.EraseObjects(collection);
                BlockImporterOption bio = BlockImporterOption.CreateOptionsDirectlyInsert(blockName, Point3d.Origin);
                BlockImporter.InsertBlock(bio);
                return id;
            }
            catch (Exception) { }

            return ObjectId.Null;
        }
    }
}
