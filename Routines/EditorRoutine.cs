// Copyright (c) 2012 Сергей Попов, Виталий Сайдин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Sergey Popov, Vitaliy Saydin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections.Generic;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.Runtime;
using Celsio.Win32;
using Celsio.Win32.NativeApi;
using MAcadFramework.Geometry;

namespace MAcadFramework.Routines
{
    /// <summary>
    /// Хелпер при работе с Editor(редактром)
    /// </summary>
	public static class EditorRoutine
	{
		/// <summary>
		/// Получить текущий (активный) документ
		/// </summary>
		/// <value>Текущий (активный) документ</value>
		public static Document @Document
		{
			get
			{
				Document doc = Application.DocumentManager.MdiActiveDocument;
				return doc;
			}
		}


		/// <summary>
		/// Получить базу данных чертежа
		/// </summary>
		/// <value>База данных чертежа AutoCAD</value>
		public static Database @Database
		{
			get {  return Document.Database; }
		}

		/// <summary>
		/// Получить доступ к эдитору чертежа
		/// </summary>
		/// <value>Эдитору чертежа AutoCAD</value>
		public static Editor @Editor
		{
			get { return Document.Editor; }
		}

		/// <summary>
		/// Проверка на выполнение интерактивной команды
		/// </summary>
		/// <returns><c>true</c> если команда не выполняется, иначе <c>false</c></returns>
		public static bool CheckEditor()
		{
			if (!@Editor.IsQuiescent)
			{
				WriteMessage("Интерактивная команда уже выполняется!");
				return false;
			}
			return true;
		}
		#region Выбор объектов

		/// <summary>
		/// Указать объекты
		/// </summary>
		/// <param name="promt">Запрос пользователю</param>
		/// <returns>массив id объектов</returns>
		public static ObjectId[] SelectObjects(string promt)
		{
			return SelectObjects(promt, null);
		}

		/// <summary>
		/// Указать объекты
		/// </summary>
		/// <param name="promt">Запрос пользователю</param>
		/// <param name="types">Фильр типов объектов</param>
		/// <returns>массив id объектов</returns>
		public static ObjectId[] SelectObjects(string promt, params Type[] types)
		{
			Document doc = Document;
			// Не любим орбит. Отменяем выполнение наших команд, если выполняется команда ..ORBIT
			string activeCommand = doc.CommandInProgress.ToUpper();
			if (activeCommand.Contains("ORBIT"))
				doc.SendStringToExecute("\x1B", false, false, false);

			using (LockDoc())
			{
				PromptSelectionOptions pso = new PromptSelectionOptions();
				pso.MessageForAdding = promt;
				pso.AllowDuplicates = false;
				pso.SelectEverythingInAperture = true;

				SelectionFilter sf = null;
				if (types != null)
				{
					List<TypedValue> tv = new List<TypedValue>(types.Length + 2);
                    tv.Add(new TypedValue(-4, "<OR"));
					for (int i = 0; i < types.Length; i++)
					{
						if (types[i].IsSubclassOf(typeof(DBObject)))
						{
							try
							{
								RXClass rx = RXObject.GetClass(types[i]);
								tv.Add(new TypedValue(0, rx.DxfName));
							}
							catch
							{
								continue;
							}
						}
					}
                    tv.Add(new TypedValue(-4, "OR>"));
					sf = new SelectionFilter(tv.ToArray());
				}

				PromptSelectionResult psr = sf != null ? doc.Editor.GetSelection(pso, sf) : doc.Editor.GetSelection(pso);
				if (psr != null && psr.Status == PromptStatus.OK)
				{
					return psr.Value.GetObjectIds();
				}
				return new ObjectId[0];
			}
		}

		/// <summary>
		/// Указание объекта
		/// </summary>
		/// <param name="promt">Запрос пользователю</param>
		/// <returns>Контейнер возвращаемых значений</returns>
		public static PromptEntityResult SelectObject(string promt)
		{
			return SelectObject(promt, null);
		}


		/// <summary>
		/// Указание объекта с фильтром по допустимым типам
		/// </summary>
		/// <param name="promt">Запрос пользователю</param>
		/// <param name="types">Список допустимых типов, или null</param>
		/// <returns>Контейнер возвращаемых значений</returns>
		public static PromptEntityResult SelectObject(string promt, params Type[] types)
		{
			Document doc = Document;			
			// Не любим орбит. Отменяем выполнение наших команд, если выполняется команда ..ORBIT
			// Наследие Сергея. Не знаю зачем он это сделал.
			string activeCommand = doc.CommandInProgress.ToUpper();
			if (activeCommand.Contains("ORBIT"))
				doc.SendStringToExecute("\x1B", false, false, false);

			using (doc.LockDocument())
			{
				PromptEntityOptions peo = new PromptEntityOptions(promt);
				peo.SetRejectMessage("Не верный тип объекта");

				if (types != null)
				{
					foreach (Type type in types)
						peo.AddAllowedClass(type, false);
				}

				PromptEntityResult res = doc.Editor.GetEntity(peo);
				if (res.Status == PromptStatus.OK)
					acedFunction.acedPostCommandPrompt();
				else
					doc.SendStringToExecute("\x1B", false, false, false);
				return res;
			}
		}
		#endregion

		#region Запрос значений

		/// <summary>
		/// Запрос строки у пользователя
		/// </summary>
		/// <param name="message">Строка запроса</param>
		/// <param name="defValue">Занчение по умолчанию (System.String)</param>
		/// <returns>Введеная стока или String.Empty</returns>
		public static string GetString(string message, string defValue)
		{
			Document doc = Document;
			
			// Не любим орбит. Отменяем выполнение наших команд, если выполняется команда ..ORBIT
			string activeCommand = doc.CommandInProgress.ToLower();
			if (activeCommand.Contains("ORBIT"))
				doc.SendStringToExecute("\x1B", false, false, false);

			using (doc.LockDocument())
			{
				PromptStringOptions pso = new PromptStringOptions(message);
				if (!String.IsNullOrEmpty(defValue))
				{
					pso.UseDefaultValue = true;
					pso.DefaultValue = defValue;
				}
				PromptResult pres = doc.Editor.GetString(pso);
				if (pres.Status == PromptStatus.OK)
				{
					acedFunction.acedPostCommandPrompt();
					return pres.StringResult;
				}
				doc.SendStringToExecute("\x1B", false, false, false);
				return String.Empty;
			}
		}

		/// <summary>
		/// Запрос строки у пользователя
		/// </summary>
		/// <param name="message">Строка запроса</param>
		/// <returns>Введеная стока или String.Empty</returns>
		public static string GetString(string message)
		{
			return GetString(message, string.Empty);
		}

		/// <summary>
		/// Запрос угла на чертеже
		/// </summary>
		/// <param name="prompt">Строка запроса</param>
		/// <returns>Введенное пользователем значение</returns>
		public static double GetAngle(string prompt)
		{
			Document doc = Document;
			// Не любим орбит. Отменяем выполнение наших команд, если выполняется команда ..ORBIT
			string activeCommand = doc.CommandInProgress.ToUpper();
			if (activeCommand.Contains("ORBIT"))
				doc.SendStringToExecute("\x1B", false, false, false);

			using (doc.LockDocument())
			{
				PromptDoubleResult res = doc.Editor.GetAngle(prompt);
				// Возвращаем угол
				return res.Value;
			}
		}

		/// <summary>
		/// Точка на чертеже в WCS
		/// </summary>
		/// <param name="prompt">Строка запроса</param>
		/// <returns>Полученная точка</returns>
		public static Point3d GetPoint(string prompt)
		{
			Document doc = Document;
			// Не любим орбит. Отменяем выполнение наших команд, если выполняется команда ..ORBIT
			string activeCommand = doc.CommandInProgress.ToUpper();
			if (activeCommand.Contains("ORBIT"))
				doc.SendStringToExecute("\x1B", false, false, false);

			using (doc.LockDocument())
			{
				PromptPointResult res = doc.Editor.GetPoint(prompt);
				if (res.Status == PromptStatus.OK)
				{
					// Возвращаем точку преобразованную из UCS в WCS
					return CSTrans.Ucs2Wcs(res.Value, false);
				}
			}
			return Point3d.Origin;
		}

		/// <summary>
		/// Точка на чертеже в WCS
		/// </summary>
		/// <param name="prompt">Строка запроса</param>
		/// <param name="basePoint">Базовая точка</param>
		/// <returns>Полученная точка</returns>
		public static Point3d GetPoint(string prompt, Point3d basePoint)
		{
			Document doc = Document;
			// Не любим орбит. Отменяем выполнение наших команд, если выполняется команда ..ORBIT
			string activeCommand = doc.CommandInProgress.ToUpper();
			if (activeCommand.Contains("ORBIT"))
				doc.SendStringToExecute("\x1B", false, false, false);

			using (doc.LockDocument())
			{
				PromptPointOptions ppo = new PromptPointOptions(prompt);
				ppo.UseBasePoint = true;
				ppo.BasePoint = basePoint;
				PromptPointResult res = doc.Editor.GetPoint(ppo);
				if (res.Status == PromptStatus.OK)
				{
					// Возвращаем точку преобразованную из UCS в WCS
					return CSTrans.Ucs2Wcs(res.Value, false);
				}
			}
			return Point3d.Origin;
		}

		/// <summary>
		/// Точка на чертеже в WCS
		/// </summary>
		/// <param name="prompt">Строка запроса</param>
		/// <param name="point">Полученная точка</param>
		/// <returns><c>true</c> если операции выполнена успешна, иначе <c>false</c> </returns>
		public static bool GetPoint(string prompt, out Point3d point)
		{
            point = Point3d.Origin;
			Document doc = Document;
			// Не любим орбит. Отменяем выполнение наших команд, если выполняется команда ..ORBIT
			string activeCommand = doc.CommandInProgress.ToUpper();
			if (activeCommand.Contains("ORBIT"))
				doc.SendStringToExecute("\x1B", false, false, false);

			using (doc.LockDocument())
			{
				PromptPointOptions ppo = new PromptPointOptions(prompt);
				//ppo.UseBasePoint = true;
				PromptPointResult res = doc.Editor.GetPoint(ppo);
				if (res.Status == PromptStatus.OK)
				{
					// Возвращаем точку преобразованную из UCS в WCS
					point = CSTrans.Ucs2Wcs(res.Value, false);
					return true;
				}
			}
			return false;
		}

		/// <summary>
		/// Точка на чертеже в WCS
		/// </summary>
		/// <param name="prompt">Строка запроса</param>
		/// <param name="basePoint">Базовая точка</param>
		/// <param name="point">Полученная точка</param>
		/// <returns> <c>true</c> если операции выполнена успешна, иначе <c>false</c> </returns>
		public static bool GetPoint(string prompt, Point3d basePoint, out Point3d point)
		{
            point = Point3d.Origin;
			Document doc = Document;
			// Не любим орбит. Отменяем выполнение наших команд, если выполняется команда ..ORBIT
			string activeCommand = doc.CommandInProgress.ToUpper();
			if (activeCommand.Contains("ORBIT"))
				doc.SendStringToExecute("\x1B", false, false, false);

			using (doc.LockDocument())
			{
				PromptPointOptions ppo = new PromptPointOptions(prompt);
				ppo.UseBasePoint = true;
				ppo.BasePoint = basePoint;
				PromptPointResult res = doc.Editor.GetPoint(ppo);
				if (res.Status == PromptStatus.OK)
				{
					// Возвращаем точку преобразованную из UCS в WCS
					point = CSTrans.Ucs2Wcs(res.Value, false);
					return true;
				}
			}
			return false;
		}

		/// <summary>
		/// Получение значения double
		/// </summary>
		/// <param name="prompt">Строка запроса</param>
		/// <returns>Полученное значение</returns>
		public static double GetDouble(string prompt)
		{
			Document doc = Document;
			// Не любим орбит. Отменяем выполнение наших команд, если выполняется команда ..ORBIT
			string activeCommand = doc.CommandInProgress.ToUpper();
			if (activeCommand.Contains("ORBIT"))
			{
				doc.SendStringToExecute("\x1B", false, false, false);
			}

			using (doc.LockDocument())
			{
				//PromptDoubleOptions pdo = new PromptDoubleOptions(prompt);
				PromptDoubleResult res = doc.Editor.GetDouble(prompt);
				return res.Value;
			}
		}

		/// <summary>
		/// Получение PromptDoubleResult
		/// </summary>
		/// <param name="prompt">Строка запроса</param>
		/// <returns>Полученное значение</returns>
		public static PromptDoubleResult GetDoubleResult(string prompt)
		{
			Document doc = Document;
			// Не любим орбит. Отменяем выполнение наших команд, если выполняется команда ..ORBIT
			string activeCommand = doc.CommandInProgress.ToUpper();
			if (activeCommand.Contains("ORBIT"))
			{
				doc.SendStringToExecute("\x1B", false, false, false);
			}

			using (doc.LockDocument())
			{
				//PromptDoubleOptions pdo = new PromptDoubleOptions(prompt);
				return doc.Editor.GetDouble(prompt);
			}
		}

		/// <summary>
		/// Получение значения double
		/// </summary>
		/// <param name="prompt">Строка запроса</param>
		/// <param name="defValue">Занчение по умолчанию (System.Double)</param>
		/// <returns>Полученное значение</returns>
		public static double GetDouble(string prompt, double defValue)
		{
			Document doc = Document;
			// Не любим орбит. Отменяем выполнение наших команд, если выполняется команда ..ORBIT
			string activeCommand = doc.CommandInProgress.ToUpper();
			if (activeCommand.Contains("ORBIT"))
			{
				doc.SendStringToExecute("\x1B", false, false, false);
			}

			using (doc.LockDocument())
			{
				PromptDoubleOptions pdo = new PromptDoubleOptions(prompt);
				pdo.UseDefaultValue = true;
				pdo.DefaultValue = defValue;
				PromptDoubleResult res = doc.Editor.GetDouble(prompt);
				return res.Value;
			}
		}

		#endregion

		/// <summary>
		/// Блокировка документа
		/// </summary>
		/// <returns>Класс блокировки документа</returns>
		public static DocumentLock LockDoc()
		{
			Document doc = Application.DocumentManager.MdiActiveDocument;
			return doc.LockDocument();
		}

		/// <summary>
		/// Выполнить регенерацию чертежа
		/// </summary>
		public static void Regen()
		{
			Editor.Regen();
		}

		/// <summary>
		/// Выполнить обновление чертежа
		/// </summary>
		public static void Update()
		{
			Editor.UpdateScreen();
		}

		/// <summary>
		/// Передать фокус редактору AutoCAD
		/// </summary>
		public static void Focus()
		{
			Document doc = Application.DocumentManager.MdiActiveDocument;

			User32.SetActiveWindow(doc.Window.Handle);
			User32.SetFocus(doc.Window.Handle);
		}

		/// <summary>
		/// Передать фокус редактору AutoCAD
		/// </summary>
		public static void Focus2()
		{
			IntPtr hwndView = acedFunction.AcadGetCurrentDwgView();
			if (hwndView != IntPtr.Zero)
				User32.SetFocus(hwndView);
		}

		/// <summary>
		/// Передать фокус редактору AutoCAD
		/// </summary>
		/// <param name="handle">Хэндл окна из которого происходит вызов!!!</param>
		public static void Focus(IntPtr handle)
		{
			uint pos = Win32Utils.MakeDWORD(0, 0);
			int ret = User32.PostMessage(handle, Messages.WM_LBUTTONDOWN, (IntPtr)0x0001, (IntPtr)pos);
			System.Diagnostics.Debug.Assert(ret != 0, "WM_LBUTTONDOWN != 0");
			ret = User32.PostMessage(handle, Messages.WM_LBUTTONUP, (IntPtr)0x0001, (IntPtr)pos);
			System.Diagnostics.Debug.Assert(ret != 0, "WM_LBUTTONUP != 0");
			// Разкомментитруйте строку если возникли проблемы.
			//System.Windows.Forms.Application.DoEvents();
		}


		/// <summary>
		/// Вывод текстового сообщения в консоль AutoCAD
		/// </summary>
		/// <param name="message">Текстовое сообщение</param>
		public static void WriteMessage(string message)
		{
			WriteRawMessage("\n" + message + "\n");
		}

        /// <summary>
        /// Вывод текстового сообщения в консоль AutoCAD
        /// </summary>
        /// <param name="message">Текстовое сообщение</param>
        public static void WriteRawMessage(string message)
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            if (doc == null) return;
            using (doc.LockDocument())
            {
                doc.Editor.WriteMessage(message);
                //acedPostCommandPrompt();
            }
        }

		//public static void SelectSubEntity(ObjectId entId)
		//{
		//    Document doc = Document;
		//    using (doc.LockDocument())
		//    {
		//        PointMonitorEventHandler pmeh = EditorPointMonitor;
		//        doc.Editor.PointMonitor += pmeh;
		//        doc.Editor.GetSelection();
		//        doc.Editor.PointMonitor -= pmeh;
		//    }
		//}

		//static void EditorPointMonitor(object sender, PointMonitorEventArgs e)
		//{
		//    FullSubentityPath[] fsp = e.Context.GetPickedEntities();
		//    WriteMessage(fsp.Length.ToString());
		//}
	}
}
