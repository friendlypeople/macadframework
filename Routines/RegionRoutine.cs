// Copyright (c) 2012 Сергей Попов, Виталий Сайдин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Sergey Popov, Vitaliy Saydin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections.Generic;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;

namespace MAcadFramework.Routines
{
	/// <summary>
	/// Класс облегчающий работу с регионами, реализуя часто встречающиеся операции
	/// </summary>
	public static class RegionRoutine
	{
		/// <summary>
		/// Создать регионы из кривой
		/// </summary>
		/// <param name="curve">Исходная кривая</param>
		/// <returns>Список созданных регионов</returns>
		public static List<Region> CreateRegions(Curve curve)
		{
			List<Region> list = new List<Region>(2);
			using (SystemVariableLocker.Use(SystemVariable.DelObj, 0))
			{
				DBObjectCollection col = new DBObjectCollection();
				col.Add(curve);
				DBObjectCollection tempReg = Region.CreateFromCurves(col);
				foreach (DBObject obj in tempReg)
					if (obj is Region) list.Add(obj as Region);
				return list;
			}
		}

		/// <summary>
		/// Создать регионы из кривых
		/// </summary>
		/// <param name="curves">Массив исходных кривых</param>
		/// <returns>Список созданных регионов</returns>
		public static List<Region> CreateRegions(params Curve[] curves)
		{
			List<Region> list = new List<Region>(2);
			using (SystemVariableLocker.Use(SystemVariable.DelObj, 0))
			{
				DBObjectCollection col = new DBObjectCollection();
				foreach (Curve c in curves) col.Add(c);
				DBObjectCollection tempReg = Region.CreateFromCurves(col);
				foreach (DBObject obj in tempReg)
					if (obj is Region) list.Add(obj as Region);
				return list;
			}
		}


		/// <summary>
		/// Взорвать регион на состовные объекты
		/// </summary>
		/// <param name="region">Исходный регион</param>
		/// <returns>Колекция полученных объектов</returns>
		public static DBObjectCollection ExplodeRegion(Region region)
		{
			try
			{
				DBObjectCollection retCollection = new DBObjectCollection();
				if (region == null) return retCollection;
				DBObjectCollection regionObjs = new DBObjectCollection();
				region.Explode(regionObjs);
				foreach (DBObject obj in regionObjs)
				{
					if (obj is Region)
					{
						DBObjectCollection simpleObjs = ExplodeRegion(obj as Region);
						// Удаляем ненужный регион. 
						obj.Dispose();
						foreach (DBObject sObj in simpleObjs)
							retCollection.Add(sObj);
					}
					else
						retCollection.Add(obj);
				}
				return retCollection;
			}
			catch (Exception exc)
			{
				EditorRoutine.WriteMessage(exc.ToString());
			}
			return new DBObjectCollection();
		}



		/// <summary>
		/// Создать регион по указанным кривым
		/// </summary>
		/// <param name="curve">Исходная кривая</param>
		/// <returns>Созданный регион</returns>
		/// <remarks>
		/// Если при создании регионов будет создано более одного, то вернется только первый из созданных
		/// </remarks>
		public static Region CreateRegion(Curve curve)
		{
			using (SystemVariableLocker.Use(SystemVariable.DelObj, 0))
			{
				DBObjectCollection col = new DBObjectCollection();
				col.Add(curve);
				DBObjectCollection tempReg = Region.CreateFromCurves(col);
				if (tempReg.Count > 0 && tempReg[0] is Region)
					return tempReg[0] as Region;
				return new Region();
			}
		}

		/// <summary>
		/// Создать регион по указанным кривым
		/// </summary>
		/// <param name="curves">Массив исходных кривых</param>
		/// <returns>Созданный регион</returns>
		/// <remarks>
		/// Если при создании регионов будет создано более одного, то вернется только первый из созданных
		/// </remarks>
		public static Region CreateRegion(params Curve[] curves)
		{
			using (SystemVariableLocker.Use(SystemVariable.DelObj, 0))
			{
				DBObjectCollection col = new DBObjectCollection();
				foreach (Curve c in curves)
					col.Add(c);
				DBObjectCollection tempReg = Region.CreateFromCurves(col);
				if (tempReg.Count > 0)
					return tempReg[0] as Region;
				return new Region();
			}
		}

        /// <summary>
        /// Создать регион по набору точек
        /// </summary>
        /// <param name="points">Исходные точки</param>
        /// <returns>Созданный регион</returns>
        public static Region CreateRegion(Point3dCollection points)
        {
            try
            {
                Polyline poly = new Polyline();
                for (int i = 0; i < points.Count; i++)
                    poly.AddVertexAt(i, new Point2d(points[i].X, points[i].Y), 0, -1, -1);
                poly.Closed = true;

                Region reg = CreateRegion(poly);
                poly.Dispose();
                return reg;
            }
            catch
            {
                return null;
            }
        }
	}
}
