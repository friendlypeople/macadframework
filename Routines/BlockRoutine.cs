// Copyright (c) 2012 Сергей Попов, Виталий Сайдин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Sergey Popov, Vitaliy Saydin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections.Generic;
using System.IO;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using MAcadFramework.Adapters;
using MAcadFramework.Aggregates;

namespace MAcadFramework.Routines
{
	/// <summary>
	/// Класс облегчающий работу с блоками, реализуя часто встречающиеся операции
	/// </summary>
	public static class BlockRoutine
	{
		/// <summary>
		/// Получить идентификатор блока по имени
		/// </summary>
		/// <param name="db">База данных чертежа</param>
		/// <param name="blockName">Имя блока</param>
		/// <returns>Идентификатор объекта AutoCAD</returns>
		public static ObjectId GetBlockId(Database db, string blockName)
		{
			//
            using (Transaction tr = DBRoutine.StartTransaction(db))
			{
				BlockTable bt = (BlockTable)tr.GetObject(db.BlockTableId, OpenMode.ForRead);
				try
				{
					if (bt.Has(blockName))
					{
						ObjectId id = bt[blockName];
						tr.Commit();
						return id;
					}
				}
				catch { tr.Abort(); }
			}
			return ObjectId.Null;
		}

		/// <summary>
		/// Получить идентификатор блока по имени
		/// </summary>
		/// <param name="blockName">Имя блока</param>
		/// <returns>Идентификатор объекта AutoCAD</returns>
		public static ObjectId GetBlockId(string blockName)
		{
			return GetBlockId(DBRoutine.WorkingDatabase, blockName);
		}

		/// <summary>
		/// Копировать блок из указанного файла в текущую базу чертежа
		/// </summary>
		/// <param name="fileName">Путь к файла</param>
		/// <param name="blockName">Имя блока</param>
		/// <returns> <c>true</c> если операции выполнена успешна, иначе <c>false</c></returns>
		public static bool CopyToHostDatabase(string fileName, string blockName)
		{
			try
			{
				Database srcDb = new Database(false, true);
				Database dstDb = DBRoutine.WorkingDatabase;

				if (!File.Exists(fileName)) return false;
				srcDb.ReadDwgFile(fileName, FileShare.Read, false, null);

				IdMapping mapping = new IdMapping();
				ObjectIdCollection blockIds = new ObjectIdCollection();
				TransactionManager tm = srcDb.TransactionManager;
				using (Transaction tr = tm.StartTransaction())
				{
					// Open the block table
					BlockTable bt = (BlockTable)tr.GetObject(srcDb.BlockTableId, OpenMode.ForRead, false);
					blockIds.Add(bt[blockName]);
					tr.Commit();
				}
				srcDb.WblockCloneObjects(blockIds, dstDb.BlockTableId, mapping, DuplicateRecordCloning.Replace, false);
				srcDb.Dispose();
			}
			catch
			{
				return false;
			}
			return true;
		}

		/// <summary>
		/// Добавление внешнего файла в чертеж в виде блока
		/// </summary>
		/// <param name="blockFileName">Имя внешнего файла</param>
		/// <param name="blockName">Имя блока помещаемого в чертеж</param>
		/// <returns>Идентификатор объекта AutoCAD</returns>
		public static ObjectId AddBlockToDocument(string blockFileName, string blockName)
		{
			if (string.IsNullOrEmpty(blockFileName)) return ObjectId.Null;
			if (string.IsNullOrEmpty(blockName)) return ObjectId.Null;

			try
			{
				Database pDb = new Database(false, true);
				pDb.ReadDwgFile(blockFileName, FileShare.Read, false, null);

				ObjectId id = DBRoutine.WorkingDatabase.Insert(blockName, pDb, true);
				return id;
			}
			catch (Exception) { }
			return ObjectId.Null;
		}


		/// <summary>
		/// Метод рекурсивного разбиения блока на составляющие не являющиеся блоками
		/// </summary>
		/// <param name="block">Cсылка на блок AutoCAD</param>
		/// <returns> Список объектов полученных в процессе разбиения </returns>
		public static DBObjectCollection ExplodeBlock(BlockReference block)
		{
			DBObjectCollection retCollection = new DBObjectCollection();
			if (block == null) return retCollection;
			DBObjectCollection blocksObjs = new DBObjectCollection();
			block.Explode(blocksObjs);
			using (SafelyCreater sc = new SafelyCreater())
			{
				foreach (DBObject obj in blocksObjs)
				{
					if (obj is BlockReference)
					{
						sc.Push(obj);
						DBObjectCollection simpleObjs = ExplodeBlock(obj as BlockReference);

						// Удаляем ненужный блок
						foreach (DBObject sObj in simpleObjs)
							retCollection.Add(sObj);
					}
					else
						retCollection.Add(obj);
				}
			}
			return retCollection;
		}


		/// <summary>
		/// Получение матрицы трансформации блока
		/// </summary>
		/// <param name="blockHandle">Дескриптор объекта AutoCAD</param>
		/// <returns>Матрица трансформации AutoCAD</returns>
		public static Matrix3d GetBlockTransform(Handle blockHandle)
		{
			BlockReference br = EntityProvider.GetObjectPointer<BlockReference>(blockHandle);
			return br == null ? Matrix3d.Identity : br.BlockTransform;
		}

		/// <summary>
		/// Получение матрицы трансформации блока
		/// </summary>
		/// <param name="blockId">Идентификатор объекта AutoCAD</param>
		/// <returns>Матрица трансформации AutoCAD</returns>
		public static Matrix3d GetBlockTransform(ObjectId blockId)
		{
			BlockReference br = EntityProvider.GetObjectPointer<BlockReference>(blockId);
			return br == null ? Matrix3d.Identity : br.BlockTransform;
		}

		/// <summary>
		/// Установка матрицы трансформации для блока
		/// </summary>
		/// <param name="blockHandle">Дескриптор блока</param>
		/// <param name="matrix">Матрица трансформации</param>
		/// <returns><c>true</c> если операции выполнена успешна, иначе <c>false</c></returns>
		public static bool SetBlockTransform(Handle blockHandle, Matrix3d matrix)
		{
			using (EntityAdapter<BlockReference> oo = EntityProvider.GetEntityAdapter<BlockReference>(blockHandle, OpenMode.ForWrite))
			{
				if (oo == null) return false;
				oo.Object.BlockTransform = matrix;
			}
			return true;
		}

		/// <summary>
		/// Установка матрицы трансформации для блока
		/// </summary>
		/// <param name="blockId">Идентификатор объекта AutoCAD</param>
		/// <param name="matrix">Матрица трансформации</param>
		/// <returns><c>true</c> если операции выполнена успешна, иначе <c>false</c></returns>
		public static bool SetBlockTransform(ObjectId blockId, Matrix3d matrix)
		{
			using (EntityAdapter<BlockReference> oo = EntityProvider.GetEntityAdapter<BlockReference>(blockId, OpenMode.ForWrite))
			{
				if (oo == null) return false;
				oo.Object.BlockTransform = matrix;
			}
			return true;
		}


		/// <summary>
		/// Установка точки вставки блока
		/// </summary>
		/// <param name="blockHandle">Дескриптор блока</param>
		/// <param name="position">Новая позиция блока</param>
		/// <returns><c>true</c> если операции выполнена успешна, иначе <c>false</c></returns>
		public static bool SetBlockPosition(Handle blockHandle, Point3d position)
		{
			using (EntityAdapter<BlockReference> oo = EntityProvider.GetEntityAdapter<BlockReference>(blockHandle, OpenMode.ForWrite))
			{
				if (oo == null) return false;
				oo.Object.Position = position;
			}
			return true;
		}

		/// <summary>
		/// Установка точки вставки блока
		/// </summary>
		/// <param name="blockId">Идентификатор объекта AutoCAD</param>
		/// <param name="position">Новая позиция блока</param>
		/// <returns><c>true</c> если операции выполнена успешна, иначе <c>false</c></returns>
		public static bool SetBlockPosition(ObjectId blockId, Point3d position)
		{
			using (EntityAdapter<BlockReference> oo = EntityProvider.GetEntityAdapter<BlockReference>(blockId, OpenMode.ForWrite))
			{
				if (oo == null) return false;
				oo.Object.Position = position;
			}
			return true;
		}

		/// <summary>
		/// Получение точки вставки блока
		/// </summary>
		/// <param name="blockHandle">Ссылка на блок</param>
		/// <returns>Точка вставки блока</returns>
		public static Point3d GetBlockPosition(Handle blockHandle)
		{
			BlockReference br = EntityProvider.GetObjectPointer<BlockReference>(blockHandle);
			return br == null ? Point3d.Origin : br.Position;
		}

		/// <summary>
		/// Получение точки вставки блока
		/// </summary>
		/// <param name="blockId">Идентификатор объекта AutoCAD</param>
		/// <returns>Точка вставки блока</returns>
		public static Point3d GetBlockPosition(ObjectId blockId)
		{
			BlockReference br = EntityProvider.GetObjectPointer<BlockReference>(blockId);
			return br == null ? Point3d.Origin : br.Position;
		}

		/// <summary>
		/// Получение поворота блока
		/// </summary>
		/// <param name="blockHandle">Дескриптор блока</param>
		/// <returns>Поворот блока</returns>
		public static double GetBlockRotation(Handle blockHandle)
		{
			BlockReference br = EntityProvider.GetObjectPointer<BlockReference>(blockHandle);
			return br == null ? 0 : br.Rotation;
		}

		/// <summary>
		/// Получение поворота блока
		/// </summary>
		/// <param name="blockId">Идентификатор объекта AutoCAD</param>
		/// <returns>Поворот блока</returns>
		public static double GetBlockRotation(ObjectId blockId)
		{
			BlockReference br = EntityProvider.GetObjectPointer<BlockReference>(blockId);
			return br == null ? 0 : br.Rotation;
		}

		/// <summary>
		/// Установка поворота блока
		/// </summary>
		/// <param name="blockHandle">Дескриптор блока</param>
		/// <param name="rotation">Поворот блока</param>
		/// <returns><c>true</c> если операции выполнена успешна, иначе <c>false</c></returns>
		public static bool SetBlockPosition(Handle blockHandle, double rotation)
		{
			using (EntityAdapter<BlockReference> oo = EntityProvider.GetEntityAdapter<BlockReference>(blockHandle, OpenMode.ForWrite))
			{
				if (oo == null) return false;
				oo.Object.Rotation = rotation;
			}
			return true;
		}

		/// <summary>
		/// Установка поворота блока
		/// </summary>
		/// <param name="blockId">Идентификатор объекта AutoCAD</param>
		/// <param name="rotation">Поворот блока</param>
		/// <returns><c>true</c> если операции выполнена успешна, иначе <c>false</c></returns>
		public static bool SetBlockPosition(ObjectId blockId, double rotation)
		{
			using (EntityAdapter<BlockReference> oo = EntityProvider.GetEntityAdapter<BlockReference>(blockId, OpenMode.ForWrite))
			{
				if (oo == null) return false;
				oo.Object.Rotation = rotation;
			}
			return true;
		}

		/// <summary>
		/// Добавление атрибутов в блока
		/// </summary>
		/// <param name="blockRef">Cсылка на блок AutoCAD</param>
		public static void InsertBlockAttibute(BlockReference blockRef)
		{
			using (Transaction tr = DBRoutine.StartTransaction())
			{
				try
				{
					BlockTableRecord btr = (BlockTableRecord)tr.GetObject(blockRef.BlockTableRecord, OpenMode.ForRead);
					foreach (ObjectId id in btr)
					{
						Object obj = tr.GetObject(id, OpenMode.ForRead);
						if (obj == null || !(obj is AttributeDefinition)) continue;

						AttributeDefinition attDef = (AttributeDefinition)obj;
						AttributeReference attRef = new AttributeReference();
						attRef.SetAttributeFromBlock(attDef, blockRef.BlockTransform);
						// Добавляем атрибут
						blockRef.AttributeCollection.AppendAttribute(attRef);
						tr.AddNewlyCreatedDBObject(attRef, true);
					}
					tr.Commit();
				}
				catch { tr.Abort(); }
			}
		}

		/// <summary>
		/// Обновление в блоке полей и атрибутов их представляющих
		/// </summary>
		/// <param name="blockRefId">Идентификатор объекта AutoCAD</param>
		/// <returns>Успех выполнения операции</returns>
		public static bool UpdateAttribFields(ObjectId blockRefId)
		{
			bool ret = false;
			using (Transaction tr = DBRoutine.StartTransaction())
			{
				try
				{
					BlockReference br = tr.GetObject(blockRefId, OpenMode.ForWrite) as BlockReference;
					if (br != null)
					{
						ret = UpdateAttribFields(br);
						tr.Commit();
					}
				}
				catch { tr.Abort(); }
			}
			return ret;
		}

		/// <summary>
		/// Обновление в блоке полей и атрибутов их представляющих
		/// </summary>
		/// <param name="blockRef">Ссылка на блок</param>
		/// <returns>Успех выполнения операции</returns>
		public static bool UpdateAttribFields(BlockReference blockRef)
		{
			using (Transaction tr = DBRoutine.StartTransaction())
			{
				try
				{
					// Цикл по списку атрибутов блока
					foreach (ObjectId attId in blockRef.AttributeCollection)
					{
						// Текущий атрибут из коллекции (ЧТЕНИЕ)
						AttributeReference att = (AttributeReference)tr.GetObject(attId, OpenMode.ForRead);

						// ID расширенного словаря атрибута
						ObjectId attExtDicId = att.ExtensionDictionary;
						if (!attExtDicId.IsValid) continue;
						// Открываем расширенный словарь атрибута (ЧТЕНИЕ)
						DBDictionary attExtDic = tr.GetObject(attExtDicId, OpenMode.ForRead) as DBDictionary;
						if (attExtDic == null) continue;

						// Получаем ID словаря полей ACAD_FIELD
						ObjectId fieldsDicId = attExtDic.GetAt("ACAD_FIELD");
						// Открываем словарь полей (ЧТЕНИЕ)
						DBDictionary fieldsDic = tr.GetObject(fieldsDicId, OpenMode.ForRead) as DBDictionary;
						if (fieldsDic == null) continue;

						// Цикл по записям словаря
						foreach (DBDictionaryEntry dicEntry in fieldsDic)
						{
							// Открываем текущую запись (ЧТЕНИЕ)
							DBObject fDicEntry = tr.GetObject(dicEntry.Value, OpenMode.ForRead);
							// Обрабатываем запись если она является полем
							if (fDicEntry is Field)
							{
								Field ourField = (Field)fDicEntry;
								// Статус открытия -> ЗАПИСЬ
								ourField.UpgradeOpen();

								// Пересчитываем поле
								ourField.Evaluate();
							}
						}
					}
					tr.Commit();
					return true;
				}
				catch
				{
					tr.Abort();
					return false;
				}
			}
		}

		/// <summary>
		/// Установка значения атрибута блока
		/// </summary>
		/// <param name="blockRef">Cсылка на блок AutoCAD</param>
		/// <param name="tag">Имя атрибута</param>
		/// <param name="value">Значение атрибута</param>
		public static void SetAttributeValue(BlockReference blockRef, string tag, string value)
		{
			using (Transaction tr = DBRoutine.StartTransaction())
			{
				try
				{
					foreach (ObjectId attId in blockRef.AttributeCollection)
					{
						AttributeReference att = (AttributeReference)tr.GetObject(attId, OpenMode.ForRead);
						if (att.Tag.CompareTo(tag) != 0) continue;
						att.UpgradeOpen();
						att.TextString = value;
						tr.Commit();
						return;
					}
				}
				catch (Exception e)
				{
					throw new Exception("Ошибка задания атрибута блока " + tag, e);
				}
				tr.Abort();
			}
		}

		/// <summary>
		/// Установка значения атрибута блока
		/// </summary>
		/// <param name="blockHandle">Дескриптор объекта AutoCAD</param>
		/// <param name="tag">Имя атрибута</param>
		/// <param name="value">Значение атрибута</param>
		public static void SetAttributeValue(Handle blockHandle, string tag, string value)
		{
			using (EntityAdapter<BlockReference> oo = EntityProvider.GetEntityAdapter<BlockReference>(blockHandle, OpenMode.ForWrite))
			{
				SetAttributeValue(oo.Object, tag, value);
			}
		}

		/// <summary>
		/// Установка значения атрибута блока
		/// </summary>
		/// <param name="blockId">Идентификатор объекта AutoCAD</param>
		/// <param name="tag">Имя атрибута</param>
		/// <param name="value">Значение атрибута </param>
		public static void SetAttributeValue(ObjectId blockId, string tag, string value)
		{
			using (EntityAdapter<BlockReference> oo = EntityProvider.GetEntityAdapter<BlockReference>(blockId, OpenMode.ForWrite))
			{
				SetAttributeValue(oo.Object, tag, value);
			}
		}

		/// <summary>
		/// Получение значения атрибута блока по заданному имени
		/// </summary>
		/// <param name="blockHandle">Дескриптор объекта AutoCAD</param>
		/// <param name="tag">Имя атрибута</param>
		/// <returns>Значение атрибута или <c>null</c></returns>
		public static object GetAttributeValue(Handle blockHandle, string tag)
		{
			BlockReference br = EntityProvider.GetObjectPointer<BlockReference>(blockHandle);
			return GetAttributeValue(br, tag);
		}


		/// <summary>
		/// Получение значения атрибута блока по заданному имени
		/// </summary>
		/// <param name="blockRefId">Идентификатор AutoCAD</param>
		/// <param name="tag">Имя атрибута</param>
		/// <returns>Значение атрибута или <c>null</c></returns>
		public static object GetAttributeValue(ObjectId blockRefId, string tag)
		{
			BlockReference br = EntityProvider.GetObjectPointer<BlockReference>(blockRefId);
			return GetAttributeValue(br, tag);
		}

		/// <summary>
		/// Получение значения атрибута блока по заданному имени
		/// </summary>
		/// <param name="blockRef">Cсылка на блок AutoCAD</param>
		/// <param name="tag">Имя атрибута</param>
		/// <returns>Значение атрибута или <c>null</c></returns>
		public static string GetAttributeValue(BlockReference blockRef, String tag)
		{
			if (blockRef == null) return null;
			using (Transaction tr = DBRoutine.StartTransaction())
			{
				try
				{
					foreach (ObjectId attId in blockRef.AttributeCollection)
					{
						AttributeReference att = (AttributeReference)tr.GetObject(attId, OpenMode.ForRead);
						if (!att.Tag.Equals(tag)) continue;
						tr.Commit();
						return att.TextString;
					}
				}
				catch (Exception)
				{
					tr.Abort();
				}
			}
			return null;
		}

		/// <summary>
		/// Получение ID атрибута блока
		/// </summary>
		/// <param name="blockRef">Cсылка на блок AutoCAD</param>
		/// <param name="tag">Наименование атрибута</param>
		/// <returns>Идентификатор объекта AutoCAD</returns>
		public static ObjectId GetAttribReferenceId(BlockReference blockRef, String tag)
		{
			ObjectId value = ObjectId.Null;
			using (Transaction tr = DBRoutine.StartTransaction())
			{
				try
				{
					foreach (ObjectId attId in blockRef.AttributeCollection)
					{
						AttributeReference att = (AttributeReference)tr.GetObject(attId, OpenMode.ForRead);
						if (att.Tag.CompareTo(tag) != 0) continue;
						value = att.Id;
						tr.Commit();
						return value;
					}
					tr.Commit();
					return value;
				}
				catch (Exception) { tr.Abort(); }
			}
			return value;
		}



		/// <summary>
		/// Получение словаря свойств динамического блока
		/// </summary>
		/// <param name="blockId">Идентификатор объекта AutoCAD</param>
		/// <returns>Словарь свойств для блока</returns>
		public static Dictionary<string, object> GetProperties(ObjectId blockId)
		{
			// Словарь свойств
			Dictionary<string, object> props = new Dictionary<string, object>();
			if (DBRoutine.WorkingDatabase == null || !blockId.IsValid) return props;
			
			using (Transaction tr = DBRoutine.StartTransaction())
			{
				if (tr == null) return null;
				try
				{
					DBObject testBlock = tr.GetObject(blockId, OpenMode.ForRead);
					if (testBlock is BlockReference)
					{
						BlockReference blRef = (BlockReference)testBlock;
						DynamicBlockReferencePropertyCollection col = blRef.DynamicBlockReferencePropertyCollection;
						foreach (DynamicBlockReferenceProperty p in col)
						{
							if (!props.ContainsKey(p.PropertyName))
							{
								props.Add(p.PropertyName, p.Value);
							}
						}
					}
					tr.Commit();					
				}
				catch (Exception) { tr.Abort(); }
			}
			return props;
		}

		/// <summary>
		/// Установка значения свойства для динамического блока
		/// </summary>
		/// <param name="blockId">Идентификатор объекта AutoCAD</param>
		/// <param name="prop">Имя свойства</param>
		/// <param name="value">Значение свойства</param>
		public static void SetPropertyValue(ObjectId blockId, string prop, object value)
		{
			using (EntityAdapter<BlockReference> oo = EntityProvider.GetEntityAdapter<BlockReference>(blockId, OpenMode.ForWrite))
			{
				SetPropertyValue(oo.Object, prop, value);
			}
		}

		/// <summary>
		/// Установка значения свойства для динамического блока
		/// </summary>
		/// <param name="blockHandle">Дескриптор блока</param>
		/// <param name="prop">Имя свойства</param>
		/// <param name="value">Значение свойства</param>
		public static void SetPropertyValue(Handle blockHandle, string prop, object value)
		{
			using (EntityAdapter<BlockReference> oo = EntityProvider.GetEntityAdapter<BlockReference>(blockHandle, OpenMode.ForWrite))
			{
				SetPropertyValue(oo.Object, prop, value);
			}
		}

		/// <summary>
		/// Установка значения свойства для динамического блока
		/// </summary>
		/// <param name="blockRef">Ссылка на блок</param>
		/// <param name="prop">Имя свойства</param>
		/// <param name="val">Значение свойства</param>
		public static void SetPropertyValue(BlockReference blockRef, string prop, object val)
		{
			try
			{
				System.Diagnostics.Debug.Assert(blockRef.IsWriteEnabled, "Blocks::SetPropertyValue", "Блок не открыт на запись");
				DynamicBlockReferencePropertyCollection col = blockRef.DynamicBlockReferencePropertyCollection;
				foreach (DynamicBlockReferenceProperty p in col)
				{
					if (p.PropertyName.Equals(prop))
					{
						p.Value = val;
						return;
					}
				}
			}
			catch (Exception e)
			{
				throw new Exception("Ошибка задания значения блока " + prop, e);
			}
		}


		/// <summary>
		/// Получение значения свойства из динамического блока
		/// </summary>
		/// <param name="blockHandle">Дескриптор объекта AutoCAD</param>
		/// <param name="prop">Имя свойства</param>
		/// <returns></returns>
		public static object GetPropertyValue(Handle blockHandle, string prop)
		{
			using (EntityAdapter<BlockReference> oo = EntityProvider.GetEntityAdapter<BlockReference>(blockHandle, OpenMode.ForRead))
			{
				return GetPropertyValue(oo.Object, prop);
			}
		}


		/// <summary>
		/// Получение значения свойства из динамического блока
		/// </summary>
		/// <param name="blockId">Идентификатор объекта AutoCAD</param>
		/// <param name="prop">Имя свойства</param>
		/// <returns>Значение свойства</returns>
		public static object GetPropertyValue(ObjectId blockId, string prop)
		{
			using (EntityAdapter<BlockReference> oo = EntityProvider.GetEntityAdapter<BlockReference>(blockId, OpenMode.ForRead))
			{
				return GetPropertyValue(oo.Object, prop);
			}
		}

		/// <summary>
		/// Получение значения свойства из динамического блока
		/// </summary>
		/// <param name="blockRef">Cсылка на блок AutoCAD</param>
		/// <param name="prop">Имя свойства</param>
		/// <returns>Значение свойства</returns>
		public static object GetPropertyValue(BlockReference blockRef, string prop)
		{
			try
			{
				DynamicBlockReferencePropertyCollection col = blockRef.DynamicBlockReferencePropertyCollection;
				foreach (DynamicBlockReferenceProperty p in col)
				{
					if (p.PropertyName.Equals(prop))
						return p.Value;
				}
			}
			catch (Exception e)
			{
				throw new Exception("Ошибка получения значения блока " + prop, e);
			}
			throw new Exception("Не найдено указанное свойство " + prop);
		}

		///// <summary>
		///// Создать блок по набору объектов имеющихся в чертеже
		///// </summary>
		///// <param name="objectIds">Идентификатор объекта AutoCAD</param>
		///// <param name="blockName">Имя блока</param>
		///// <param name="origin">Точка вставки блока</param>
		///// <returns>Идентификатор созданого блока</returns>
		//public static ObjectId MakeBlock(ObjectIdCollection objectIds, string blockName, Point3d origin)
		//{
		//    ObjectId blockTableRecordId = ObjectId.Null;
		//    try
		//    {
		//        BlockTableRecord pBlockTableRec = new BlockTableRecord();
		//        pBlockTableRec.Name = blockName;
		//        pBlockTableRec.Origin = origin;

		//        ObjectId blockTableId = DBRoutine.WorkingDatabase.BlockTableId;
		//        using (Transaction tr = DBRoutine.StartTransaction())
		//        {
		//            BlockTable pBlockTable = tr.GetObject(blockTableId, OpenMode.ForWrite) as BlockTable;
		//            if (pBlockTable != null)
		//            {
		//                blockTableRecordId = pBlockTable.Add(pBlockTableRec);
		//                pBlockTable.DowngradeOpen();

		//                for (int i = 0; i < objectIds.Count; i++)
		//                {
		//                    // Открываем текущий объект на чтение
		//                    DBObject currObj = tr.GetObject(objectIds[i], OpenMode.ForRead);
		//                    if (currObj is Region)
		//                    {
		//                        // Делаем копию текущего объекта
		//                        Region regClone = currObj.Clone() as Region;
		//                        // Разбиваем регион на составляющие
		//                        DBObjectCollection expRegionObjects = NeoAcadUtils.GeomUtils.ExplodeRegion(regClone);

		//                        foreach (DBObject obj in expRegionObjects)
		//                        {
		//                            // Добавляем копию объекта в блок и получаем его ID
		//                            ObjectId newId = pBlockTableRec.AppendEntity((Entity)obj);
		//                            tr.AddNewlyCreatedDBObject(obj, true);
		//                        }
		//                        DBRoutine.Dispose(regClone);
		//                    }
		//                    else
		//                    {
		//                        // Делаем копию текущего объекта
		//                        DBObject objClone = currObj.Clone() as DBObject;
		//                        // Добавляем копию объекта в блок и получаем его ID
		//                        ObjectId newId = pBlockTableRec.AppendEntity((Entity)objClone);

		//                        // Текущий объект на ЗАПИСЬ
		//                        //currObj.UpgradeOpen();
		//                        // Меняемся ID
		//                        //currObj.SwapIdWith(newId, true, true);

		//                        tr.AddNewlyCreatedDBObject(objClone, true);
		//                    }
		//                }
		//                tr.AddNewlyCreatedDBObject(pBlockTableRec, true);
		//            }
		//            tr.Commit();
		//        }
		//    }
		//    catch
		//    {
		//        return ObjectId.Null;
		//    }
		//    return blockTableRecordId;
		//}

		/// <summary>
		/// Удалить запись в таблице блоков (BlockTableRecord)
		/// </summary>
		/// <param name="blockName">Имя блока </param>
		public static void EraseBlock(string blockName)
		{
			using (Transaction tr = DBRoutine.StartTransaction())
			{
				try
				{
					BlockTable pBlockTable = tr.GetObject(DBRoutine.WorkingDatabase.BlockTableId, OpenMode.ForWrite) as BlockTable;
					if (pBlockTable != null)
					{
						if (pBlockTable.Has(blockName))
						{
							ObjectId blockId = pBlockTable[blockName];
							BlockTableRecord btr = tr.GetObject(blockId, OpenMode.ForWrite) as BlockTableRecord;
							if (btr != null)
							{
								btr.Erase();
							}
						}
					}
					tr.Commit();
				}
				catch { tr.Abort(); }
			}
		}

		/// <summary>
		/// Удалить запись в таблице блоков (BlockTableRecord)
		/// </summary>
		/// <param name="blockId">Идентификатор записи в таблице блоков</param>
		public static void EraseBlock(ObjectId blockId)
		{
			using (Transaction tr = DBRoutine.StartTransaction())
			{
				try
				{

					BlockTableRecord btr = tr.GetObject(blockId, OpenMode.ForWrite) as BlockTableRecord;
					if (btr != null)
					{
						btr.Erase();
					}
					tr.Commit();
				}
				catch (Exception) { tr.Abort(); }
			}
		}
	}
}
