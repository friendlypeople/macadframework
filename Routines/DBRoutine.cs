// Copyright (c) 2012 Сергей Попов, Виталий Сайдин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Sergey Popov, Vitaliy Saydin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections.Generic;
using Autodesk.AutoCAD.DatabaseServices;
using MAcadFramework.Aggregates;

namespace MAcadFramework.Routines
{
	/// <summary>
	/// Класс, облегчающий работу с базой данных AutoCAD, реализуя часто встречающиеся операции
	/// </summary>
	public sealed class DBRoutine
	{
		/// <summary>
		/// Класс, облегчающий сбор данных с чертежа
		/// </summary>
		public static class ObjectGetter
		{
			/// <summary>
			/// Получить все объекты из БД чертежа c указанного пространства
			/// </summary>
			/// <param name="space">Пространство для сбора объектов или имя layout'а</param>
			/// <param name="layer">Слой для сбора объектов</param>
			/// <param name="objectTypes">Типы объектов для сбора</param>
			/// <returns>Список идентификаторов объектов AutoCAD</returns>
            public static List<ObjectId> FromSpace(string space, string layer, params Type[] objectTypes)
			{
			    List<ObjectId> objs = new List<ObjectId>();
			    // Запускаем транзакцию
			    using (Transaction myT = StartTransaction(true))
			    {
			        // Таблица блоков (на чтение)
			        BlockTable bt = (BlockTable) myT.GetObject(WorkingDatabase.BlockTableId, OpenMode.ForRead, false);
			        // Запись таблицы блоков, описывающая пространство модели (на чтение)
			        BlockTableRecord btr = null;
			        if (bt.Has(space))
			            btr = myT.GetObject(bt[space], OpenMode.ForRead, false) as BlockTableRecord;
			        else
			        {
			            bool isLayoutFind = false;
			            foreach (ObjectId btrId in bt)
			            {
			                btr = (BlockTableRecord) myT.GetObject(btrId, OpenMode.ForRead);
			                if (btr.IsLayout)
			                {
			                    Layout layout = (Layout) myT.GetObject(btr.LayoutId, OpenMode.ForRead);
			                    if (layout.LayoutName.ToUpper() == space.ToUpper())
			                    {
			                        isLayoutFind = true;
			                        break;
			                    }
			                }
			            }

			            if (!isLayoutFind)
			                throw new Exception("Не удалось найти layout " + space);
			        }

			        if (btr == null) throw new Exception("Не удалось найти пространство " + space);
			        
                    foreach (ObjectId id in btr)
			        {
			            DBObject obj = myT.GetObject(id, OpenMode.ForRead, false);
			            if (obj == null) continue;

			            // Работает фильтр
			            if (objectTypes != null && objectTypes != Type.EmptyTypes)
			            {
			                // Если тип не входит в фильтр типов, то бежим дальше
			                int filterTypeIndex = Array.IndexOf(objectTypes, obj.GetType());
			                if (filterTypeIndex < 0) continue;
			            }

			            if (obj is Entity)
			            {
			                if (!String.IsNullOrEmpty(layer))
			                {
			                    Entity ent = (Entity) obj;
			                    if (!ent.Layer.Equals(layer)) continue;
			                }
			                objs.Add(obj.Id);
			            }
			        }

			        myT.Commit();
			        return objs;
			    }
			}

		    /// <summary>
			/// Получить все объекты из БД чертежа c указанного пространства
			/// </summary>
            /// <param name="space">Пространство для сбора объектов или имя layout'а</param>
			/// <param name="objectTypes">Типы объектов для сбора</param>
			/// <returns>Список идентификаторов объектов AutoCAD</returns>
			public static List<ObjectId> FromSpace(string space, params Type[] objectTypes)
			{
				return FromSpace(space, string.Empty, objectTypes);
			}

			/// <summary>
			/// Получить все объекты из БД чертежа c указанного пространства
			/// </summary>
            /// <param name="space">Пространство для сбора объектов или имя layout'а</param>
			/// <param name="layer">Слой для сбора объектов</param>
			/// <returns>Список идентификаторов объектов AutoCAD</returns>
			public static List<ObjectId> FromSpace(string space, string layer)
			{
				return FromSpace(space, layer, Type.EmptyTypes);
			}

			/// <summary>
			/// Получить все объекты из БД чертежа c указанного пространства
			/// </summary>
            /// <param name="space">Пространство для сбора объектов или имя layout'а</param>
			/// <returns>Список идентификаторов объектов AutoCAD</returns>
			/// <remarks>
			///		Под пространством для сбора объектов <see cref="space"/> подразумеваются имена записей в BlockTableRecord,
			///		а не имена Layout AtoCAD!
			/// </remarks>
			public static List<ObjectId> FromSpace(string space)
			{
				return FromSpace(space, string.Empty, Type.EmptyTypes);
			}

			/// <summary>
			/// Получить все объекты из БД чертежа c пространства модели
			/// </summary>
			/// <param name="layer">Слой для сбора объектов</param>
			/// <param name="objectTypes">Типы объектов для сбора</param>
			/// <returns>Список идентификаторов объектов AutoCAD</returns>
			public static List<ObjectId> FromModelSpace(string layer, params Type[] objectTypes)
			{
				return FromSpace(BlockTableRecord.ModelSpace, layer, objectTypes);
			}

			/// <summary>
			/// Получить все объекты из БД чертежа c пространства модели
			/// </summary>
			/// <param name="objectTypes">Типы объектов для сбора</param>
			/// <returns>Список идентификаторов объектов AutoCAD</returns>
			public static List<ObjectId> FromModelSpace(params Type[] objectTypes)
			{
				return FromSpace(BlockTableRecord.ModelSpace, String.Empty ,objectTypes);
			}

			/// <summary>
			/// Получить все объекты из БД чертежа c пространства модели
			/// </summary>
			/// <param name="layer">Слой для сбора объектов</param>
			/// <returns>Список идентификаторов объектов AutoCAD</returns>
			public static List<ObjectId> FromModelSpace(string layer)
			{
				return FromSpace(BlockTableRecord.ModelSpace, layer, Type.EmptyTypes);
			}

			/// <summary>
			/// Получить все объекты из БД чертежа c пространства модели
			/// </summary>
			/// <returns>Список идентификаторов объектов AutoCAD</returns>
			public static List<ObjectId> FromModelSpace()
			{
				return FromSpace(BlockTableRecord.ModelSpace, String.Empty, Type.EmptyTypes);
			}

			/// <summary>
			/// Получить все объекты из БД чертежа c пространства листа
			/// </summary>
			/// <param name="layer">Слой для сбора объектов</param>
			/// <param name="objectTypes">Типы объектов для сбора</param>
			/// <returns>Список идентификаторов объектов AutoCAD</returns>
			public static List<ObjectId> FromPaperSpace(string layer, params Type[] objectTypes)
			{
				return FromSpace(BlockTableRecord.PaperSpace, layer, objectTypes);
			}

			/// <summary>
			/// Получить все объекты из БД чертежа c пространства листа
			/// </summary>
			/// <param name="objectTypes">Типы объектов для сбора</param>
			/// <returns>Список идентификаторов объектов AutoCAD</returns>
			public static List<ObjectId> FromPaperSpace(params Type[] objectTypes)
			{
				return FromSpace(BlockTableRecord.PaperSpace, String.Empty, objectTypes);
			}

			/// <summary>
			/// Получить все объекты из БД чертежа c пространства листа
			/// </summary>
			/// <param name="layer">Слой для сбора объектов</param>
			/// <returns>Список идентификаторов объектов AutoCAD</returns>
			public static List<ObjectId> FromPaperSpace(string layer)
			{
				return FromSpace(BlockTableRecord.PaperSpace, layer, Type.EmptyTypes);
			}

			/// <summary>
			/// Получить все объекты из БД чертежа c пространства листа
			/// </summary>
			/// <returns>Список идентификаторов объектов AutoCAD</returns>
			public static List<ObjectId> FromPaperSpace()
			{
				return FromSpace(BlockTableRecord.PaperSpace, String.Empty, Type.EmptyTypes);
			}

			/// <summary>
			/// Получить все объекты из БД чертежа c текущего пространства
			/// </summary>
			/// <param name="layer">Слой для сбора объектов</param>
			/// <param name="objectTypes">Типы объектов для сбора</param>
			/// <returns>Список идентификаторов объектов AutoCAD</returns>
			public static List<ObjectId> FromCurrentSpace(string layer, params Type[] objectTypes)
			{
				short res = SystemVariable.GetSystemVariable<short>(SystemVariable.TileMode);
				string space = BlockTableRecord.ModelSpace;
				if (res == 0)
					space = BlockTableRecord.PaperSpace;
				return FromSpace(space, layer, objectTypes);
			}


			/// <summary>
			/// Получить все объекты из БД чертежа c текущего пространства
			/// </summary>
			/// <param name="objectTypes">Типы объектов для сбора</param>
			/// <returns>Список идентификаторов объектов AutoCAD</returns>
			public static List<ObjectId> FromCurrentSpace(params Type[] objectTypes)
			{
				return FromCurrentSpace(String.Empty, objectTypes);
			}

			/// <summary>
			/// Получить все объекты из БД чертежа c текущего пространства
			/// </summary>
			/// <param name="layer">Слой для сбора объектов</param>
			/// <returns>Список идентификаторов объектов AutoCAD</returns>
			public static List<ObjectId> FromCurrentSpace(string layer)
			{
				return FromCurrentSpace(layer, Type.EmptyTypes);
			}

			/// <summary>
			/// Получить все объекты из БД чертежа c текущего пространства
			/// </summary>
			/// <returns>Список идентификаторов объектов AutoCAD</returns>
			public static List<ObjectId> FromCurrentSpace()
			{
				return FromCurrentSpace(String.Empty, Type.EmptyTypes);
			}
		}

		/// <summary>
		/// Идентификаторы таблиц AutoCAD для текущего чертежа
		/// </summary>
		public static class WellKnownTableId
		{
			/// <summary>
			/// Получить идентификатор таблицы слоев текущего чертежа
			/// </summary>
			/// <value>Идентификатор таблицы слоев текущего чертежа</value>
			public static ObjectId LayerTableId
			{
				get { return WorkingDatabase.LayerTableId; }
			}

			/// <summary>
			/// Получить идентификатор таблицы стилей размеров текущего чертежа
			/// </summary>
			/// <value>Идентификатор таблицы стилей размеров текущего чертежа</value>
			public static ObjectId DimStyleTableId
			{
				get { return WorkingDatabase.DimStyleTableId; }
			}

			/// <summary>
			/// Получить идентификатор таблицы типов линий текущего чертежа
			/// </summary>
			/// <value>Идентификатор таблицы типов линий текущего чертежа</value>
			public static ObjectId LinetypeTableId
			{
				get { return WorkingDatabase.LinetypeTableId; }
			}

			/// <summary>
			/// Получить идентификатор таблицы блоков текущего чертежа
			/// </summary>
			/// <value>Идентификатор таблицы блоков текущего чертежа</value>
			public static ObjectId BlockTableId
			{
				get { return WorkingDatabase.BlockTableId; }
			}

			/// <summary>
			/// Получить идентификатор таблицы вьюпортов текущего чертежа
			/// </summary>
			/// <value>Идентификатор таблицы вьюпортов текущего чертежа</value>
			public static ObjectId ViewportTableId
			{
				get { return WorkingDatabase.ViewportTableId; }
			}

			/// <summary>
			/// Получить идентификатор таблицы видов текущего чертежа
			/// </summary>
			/// <value>Идентификатор таблицы видов текущего чертежа</value>
			public static ObjectId ViewTableId
			{
				get { return WorkingDatabase.ViewTableId; }
			}

			/// <summary>
			/// Получить идентификатор таблицы координатных систем текущего чертежа
			/// </summary>
			/// <value>Идентификатор таблицы координатных систем текущего чертежа</value>
			public static ObjectId UcsTableId
			{
				get { return WorkingDatabase.UcsTableId; }
			}

			/// <summary>
			/// Получить идентификатор таблицы стилей текста текущего чертежа
			/// </summary>
			/// <value>Идентификатор таблицы стилей текста текущего чертежа</value>
			public static ObjectId TextStyleTableId
			{
				get { return WorkingDatabase.TextStyleTableId; }
			}

			/// <summary>
			/// Получить идентификатор таблицы групп текущего чертежа
			/// </summary>
			/// <value>Идентификатор таблицы стилей текста текущего чертежа</value>
			public static ObjectId GroupTableId
			{
				get { return WorkingDatabase.GroupDictionaryId; }
			}
		}

		/// <summary>
		/// Получить базу данных AutoCAD текущего документа
		/// </summary>
		/// <value>База данных AutoCAD текущего документа</value>
		public static Database WorkingDatabase
		{
			get
			{
				try { return HostApplicationServices.WorkingDatabase; }
				catch { return null; }
			}
		}

		/// <summary>
		/// Установить новую базу данных AutoCAD как текущую
		/// </summary>
		/// <param name="extDb">Внешняя база данных AutoCAD</param>
		/// <returns>Предыдущая база данных AutoCAD</returns>
		public static Database SetWorkingDataBase(Database extDb)
		{
			Database prevDb = WorkingDatabase;
			HostApplicationServices.WorkingDatabase = extDb;
			return prevDb;
		}

		/// <summary>
		/// Получить ссылку на объект по его дескриптору
		/// </summary>
		/// <param name="handle">Дескриптор объекта AutoCAD</param>
		/// <returns>Ссылка на объект AutoCAD</returns>
		public static DBObject GetObject(Handle handle)
		{
			return EntityProvider.GetObjectPointerNoException<DBObject>(handle);
		}

		/// <summary>
		/// Получить ссылку на объект по его дескриптору
		/// </summary>
		/// <param name="id">Идентификатор объекта AutoCAD</param>
		/// <returns>Ссылка на объект AutoCAD</returns>
		public static DBObject GetObject(ObjectId id)
		{
			return EntityProvider.GetObjectPointerNoException<DBObject>(id);
		}

		/// <summary>
		/// Добавление не графического объекта в базу
		/// </summary>
		/// <param name="obj">Ссылка на объект AutoCAD для добавления</param>
		public static void AddObject(DBObject obj)
		{
			// Рабочая база
			Database db = WorkingDatabase;
			TransactionManager tm = db.TransactionManager;

			// Запускаем транзакцию
			using (Transaction myT = tm.StartTransaction())
			{
				try
				{
					AddObject(obj, myT);
					myT.Commit();
				}
				catch (Exception ex)
				{
					myT.Abort();
					EditorRoutine.WriteMessage(ex.ToString());
				}
			}
		}

		/// <summary>
		/// Добавление не графического объекта в базу
		/// </summary>
		/// <param name="obj">Ссылка на объект AutoCAD для добавления</param>
		/// <param name="trans">Открытая транзакция AutoCAD</param>
		public static void AddObject(DBObject obj, Transaction trans)
		{
			trans.AddNewlyCreatedDBObject(obj, true);
		}

		/// <summary>
		/// Получить ссылку на объект по его дескриптору
		/// </summary>
		/// <param name="handle">Дескриптор объекта AutoCAD</param>
		/// <returns>Ссылка на объект AutoCAD</returns>
		public static TYPE GetObject<TYPE>(Handle handle) where TYPE : DBObject
		{
			return EntityProvider.GetObjectPointerNoException<TYPE>(handle);
		}

		/// <summary>
		/// Получить ссылку на объект по его дескриптору
		/// </summary>
		/// <param name="id">Идентификатор объекта AutoCAD</param>
		/// <returns>Ссылка на объект AutoCAD</returns>
		public static TYPE GetObject<TYPE>(ObjectId id) where TYPE : DBObject
		{
			return EntityProvider.GetObjectPointerNoException<TYPE>(id);
		}

		/// <summary>
		/// Получение Id объекта в текущей сессии базы данных по ссылке Handle
		/// </summary>
		/// <param name="handle">Дескриптор объекта AutoCAD</param>
		/// <returns>Id объекта</returns>
		public static ObjectId GetObjectId(Handle handle)
		{
            return GetObjectId(handle, WorkingDatabase);
		}

        /// <summary>
        /// Получение Id объекта в текущей сессии базы данных по ссылке Handle
        /// </summary>
        /// <param name="handle">Дескриптор объекта AutoCAD</param>
        /// <param name="db">База данных AutoCAD</param>
        /// <returns>Id объекта</returns>
        public static ObjectId GetObjectId(Handle handle, Database db)
        {
            try
            {
                if (handle.Value > 0)
                {
                    ObjectId id = db.GetObjectId(true, handle, 0);
                    return (id.IsValid && id.IsResident) ? id : ObjectId.Null;
                }
            }
            catch
            {
                EditorRoutine.WriteMessage("Невозможно получить ID по Handle");
            }
            return ObjectId.Null;
        }

		/// <summary>
		/// Проверка дескриптора AutoCAD на валидность
		/// </summary>
		/// <param name="handle">Дескриптор объекта AutoCAD</param>
		/// <returns><c>true</c> если дескриптор пренадлежит базе, иначе <c>false</c></returns>
		/// <remarks>Удаленные объекты считаются не валидными</remarks>
		public static bool CheckHandle(Handle handle)
		{
			return CheckId(GetObjectId(handle));
		}

		/// <summary>
		/// Проверка дескриптора AutoCAD на валидность
		/// </summary>
		/// <param name="handle">Дескриптор объекта AutoCAD</param>
		/// <param name="useErased">Если <c>true</c>, то удаленный объект считать валидным.</param>
		/// <returns><c>true</c> если дескриптор пренадлежит базе, иначе <c>false</c></returns>
		public static bool CheckHandle(Handle handle, bool useErased)
		{
			return CheckId(GetObjectId(handle), useErased);
		}

		/// <summary>
		/// Проверка идентификатора объекета AutoCAD на валидность
		/// </summary>
		/// <param name="id">Идентификатор объекта AutoCAD</param>
		/// <returns><c>true</c> если идентификатор пренадлежит базе, иначе <c>false</c></returns>
		/// <remarks>Удаленные объекты считаются не валидными</remarks>
		public static bool CheckId(ObjectId id)
		{
			return CheckId(id, false);
		}

		/// <summary>
		/// Проверка идентификатора объекета AutoCAD на валидность
		/// </summary>
		/// <param name="id">Идентификатор объекта AutoCAD</param>
		/// <param name="useErased">Если <c>true</c>, то удаленный объект считать валидным.</param>
		/// <returns><c>true</c> если идентификатор пренадлежит базе, иначе <c>false</c></returns>
		public static bool CheckId(ObjectId id, bool useErased)
		{
			if (useErased)
				return (!id.IsNull && id.IsValid) && id.IsResident;
			return (((!id.IsNull && id.IsValid) && id.IsResident) && !id.IsErased) && !id.IsEffectivelyErased;
		}

		#region Добавление объектов в базу

		/// <summary>
		/// Добавление объекта в указанное пространство
		/// </summary>
		/// <param name="obj">Примитив AutoCAD для добавления</param>
		/// <param name="space">Имя пространства для вставки примитива</param>
		/// <param name="trans">Транзакция AutoCAD</param>
		/// <returns>Идентфикатор объекта AutoCAD или ObjectId.Null</returns>
		private static ObjectId AddToChosenSpace(Entity obj, string space, Transaction trans)
		{
			// Рабочая база
			Database db = WorkingDatabase;
			if (obj == null || trans == null) return ObjectId.Null;
			TransactionManager tm = trans.TransactionManager;
			try
			{
				// Таблица блоков (на чтение)
				BlockTable bt = (BlockTable)tm.GetObject(db.BlockTableId, OpenMode.ForRead, false);

				// Запись таблицы блоков, описывающая пространство (на запись)
				BlockTableRecord btr = (BlockTableRecord)tm.GetObject(bt[space], OpenMode.ForWrite, false);
				ObjectId addedObjectId = btr.AppendEntity(obj);
				// Завершительные действия с вновь созданным объектом в транзакциях
				tm.AddNewlyCreatedDBObject(obj, true);
				tm.QueueForGraphicsFlush();
				return addedObjectId;
			}
			catch (Autodesk.AutoCAD.Runtime.Exception exc)
			{
				if (exc.ErrorStatus == Autodesk.AutoCAD.Runtime.ErrorStatus.LockViolation)
					EditorRoutine.WriteMessage("Ошибка блокировки документа");
			}
			catch (Exception exc)
			{
				EditorRoutine.WriteMessage(exc.ToString());
			}
			return ObjectId.Null;
		}

		/// <summary>
		/// Добавление объекта в пространство модели
		/// </summary>
		/// <param name="obj">Примитив AutoCAD для добавления</param>
		/// <param name="trans">Транзакция AutoCAD</param>
		/// <returns>Идентфикатор объекта AutoCAD или ObjectId.Null</returns>
		public static ObjectId AddToModelSpace(Entity obj, Transaction trans)
		{
			return AddToChosenSpace(obj, BlockTableRecord.ModelSpace, trans);
		}


		/// <summary>
		/// Добавление объекта в пространство модели
		/// </summary>
		/// <param name="obj">Примитив AutoCAD для добавления</param>
		/// <returns>Идентфикатор объекта AutoCAD или ObjectId.Null</returns>
		public static ObjectId AddToModelSpace(Entity obj)
		{
			// Рабочая база
			Database db = WorkingDatabase;
			if (obj == null || db == null) return ObjectId.Null;
			TransactionManager tm = db.TransactionManager;
			// Запускаем транзакцию
			using (Transaction myT = tm.StartTransaction())
			{
				ObjectId ret = AddToModelSpace(obj, myT);
				myT.Commit();
				return ret;
			}
		}

		/// <summary>
		/// Добавление объекта в пространство чертежа
		/// </summary>
		/// <param name="obj">Примитив AutoCAD для добавления</param>
		/// <param name="trans">Транзакция AutoCAD</param>
		/// <returns>Идентфикатор объекта AutoCAD или ObjectId.Null</returns>
		public static ObjectId AddToPaperSpace(Entity obj, Transaction trans)
		{
			return AddToChosenSpace(obj, BlockTableRecord.PaperSpace, trans);
		}


		/// <summary>
		/// Добавление объекта в пространство чертежа
		/// </summary>
		/// <param name="obj">Примитив AutoCAD для добавления</param>
		/// <returns>Идентфикатор объекта AutoCAD или ObjectId.Null</returns>
		public static ObjectId AddToPaperSpace(Entity obj)
		{
			// Рабочая база
			Database db = WorkingDatabase;
			if (obj == null || db == null) return ObjectId.Null;
			TransactionManager tm = db.TransactionManager;
			// Запускаем транзакцию
			using (Transaction myT = tm.StartTransaction())
			{
				try
				{
					ObjectId ret = AddToPaperSpace(obj, myT);
					myT.Commit();
					return ret;
				}
				catch (Exception ex)
				{
					myT.Abort();
					EditorRoutine.WriteMessage(ex.ToString());
				}
			}
			return ObjectId.Null;
		}

		/// <summary>
		/// Добавление объекта в текущее пространство
		/// </summary>
		/// <param name="obj">Примитив AutoCAD для добавления</param>
		/// <param name="trans">Транзакция AutoCAD</param>
		/// <returns>Идентфикатор объекта AutoCAD или ObjectId.Null</returns>
		public static ObjectId AddToCurrentSpace(Entity obj, Transaction trans)
		{
			short res = SystemVariable.GetSystemVariable<short>(SystemVariable.TileMode);
			string space = BlockTableRecord.ModelSpace;
			if (res == 0)
				space = BlockTableRecord.PaperSpace;
			return AddToChosenSpace(obj, space, trans);
			
		}


		/// <summary>
		/// Добавление объекта в текущее пространство
		/// </summary>
		/// <param name="obj">Примитив AutoCAD для добавления</param>
		/// <returns>Идентфикатор объекта AutoCAD или ObjectId.Null</returns>
		public static ObjectId AddToCurrentSpace(Entity obj)
		{
			// Рабочая база
			Database db = WorkingDatabase;
			if (obj == null || db == null) return ObjectId.Null;
			TransactionManager tm = db.TransactionManager;
			// Запускаем транзакцию
			using (Transaction myT = tm.StartTransaction())
			{
				try
				{
					ObjectId ret = AddToCurrentSpace(obj, myT);
					myT.Commit();
					return ret;
				}
				catch (Exception ex)
				{
					myT.Abort();
					EditorRoutine.WriteMessage(ex.ToString());
				}
			}
			return ObjectId.Null;
		}
		#endregion


		#region Удаление объектов из базы
		
		/// <summary>
		/// Удаление объекта из базы данных
		/// </summary>
		/// <param name="handle">Дескриптор объекта AutoCAD</param>
		/// <returns> <c>true</c> если операции выполнена успешна, иначе <c>false</c> </returns>
		public static bool EraseObject(Handle handle)
		{
			ObjectId objId = GetObjectId(handle);
			return EraseObject(objId);
		}

		/// <summary>
		/// Удаление объекта из базы данных
		/// </summary>
		/// <param name="handle">Дескриптор объекта AutoCAD</param>
		/// <returns> <c>true</c> если операции выполнена успешна, иначе <c>false</c> </returns>
		public static bool EraseObject(long handle)
		{
			ObjectId objId = GetObjectId(new Handle(handle));
			return EraseObject(objId);
		}

		/// <summary>
		/// Удаление объекта из базы данных
		/// </summary>
		/// <param name="handle">Дескриптор объекта AutoCAD</param>
		/// <param name="trans">Текущая транзакция AutoCAD</param>
		/// <returns> <c>true</c> если операции выполнена успешна, иначе <c>false</c> </returns>
		public static bool EraseObject(Handle handle, Transaction trans)
		{
			ObjectId objId = GetObjectId(handle);
			return EraseObject(objId, trans);
		}

		/// <summary>
		/// Удаление объекта из базы данных
		/// </summary>
		/// <param name="handle">Дескриптор объекта AutoCAD</param>
		/// <param name="trans">Текущая транзакция AutoCAD</param>
		/// <returns> <c>true</c> если операции выполнена успешна, иначе <c>false</c> </returns>
		public static bool EraseObject(long handle, Transaction trans)
		{
			ObjectId objId = GetObjectId(new Handle(handle));
			return EraseObject(objId, trans);
		}

		/// <summary>
		/// Удаление объекта из базы данных
		/// </summary>
		/// <param name="id">Идентификатор объекта AutoCAD</param>
		/// <returns> <c>true</c> если операции выполнена успешна, иначе <c>false</c> </returns>
		public static bool EraseObject(ObjectId id)
		{
			Database db = WorkingDatabase;
			if (!CheckId(id) || db == null) return false;
			// Рабочая база
			TransactionManager tm = WorkingDatabase.TransactionManager;
			// Запускаем транзакцию
			using (Transaction myT = tm.StartTransaction())
			{
				try
				{
					bool ret = EraseObject(id, myT);
					myT.Commit();
					return ret;
				}
				catch (Exception ex)
				{
					myT.Abort();
					EditorRoutine.WriteMessage(ex.ToString());
				}
			}
			return false;
		}

		/// <summary>
		/// Удаление объекта из базы данных
		/// </summary>
		/// <param name="id">Идентификатор объекта AutoCAD</param>
		/// <param name="trans">Текущая транзакция AutoCAD</param>
		/// <returns> <c>true</c> если операции выполнена успешна, иначе <c>false</c> </returns>
		public static bool EraseObject(ObjectId id, Transaction trans)
		{
			if (!CheckId(id) || trans == null) return false;
			try
			{
				DBObject obj = trans.GetObject(id, OpenMode.ForRead, false);
				if (obj != null)
				{
					obj.UpgradeOpen();
					obj.Erase();
				}
				return true;
			}
			catch (Autodesk.AutoCAD.Runtime.Exception exc)
			{
				if (exc.ErrorStatus == Autodesk.AutoCAD.Runtime.ErrorStatus.LockViolation)
					EditorRoutine.WriteMessage("Ошибка блокировки документа");
			}
			catch (Exception exc)
			{
				EditorRoutine.WriteMessage(exc.ToString());
			}
			return false;
		}


		/// <summary>
		/// Удаление объектов по списку идентификаторов
		/// </summary>
		/// <param name="ids">Список идентификаторов удаляемых объектов AutoCAD</param>
		/// <param name="trans">Текущая транзакция AutoCAD</param>
		public static void EraseObjects(ObjectIdCollection ids, Transaction trans)
		{
			List<ObjectId> list = new List<ObjectId>();
			foreach (ObjectId id in ids)
				list.Add(id);
			EraseObjects(list, trans);
		}

		/// <summary>
		/// Удаление объектов по списку идентификаторов
		/// </summary>
		/// <param name="ids">Список идентификаторов удаляемых объектов AutoCAD</param>
		/// <param name="trans">Текущая транзакция AutoCAD</param>
		public static void EraseObjects(IEnumerable<ObjectId> ids, Transaction trans)
		{
			try
			{
				if (trans == null) return;
				TransactionManager tm = trans.TransactionManager;
				foreach (ObjectId id in ids)
				{
					EraseObject(id, trans);
				}
				tm.QueueForGraphicsFlush();
			}
			catch (Exception ex)
			{
				EditorRoutine.WriteMessage(ex.ToString());
			}
		}

		/// <summary>
		/// Удаление объектов по списку идентификаторов
		/// </summary>
		/// <param name="ids">Список идентификаторов удаляемых объектов AutoCAD</param>
		public static void EraseObjects(IEnumerable<ObjectId> ids)
		{
			try
			{
				// Рабочая база
				Database db = WorkingDatabase;
				if (db == null) return;
				TransactionManager tm = db.TransactionManager;

				// Запускаем транзакцию
				using (Transaction myT = tm.StartTransaction())
				{
					EraseObjects(ids, myT);
					myT.Commit();
				}
			}
			catch (Exception ex)
			{
				EditorRoutine.WriteMessage(ex.ToString());
			}
		}

		/// <summary>
		/// Удаление объектов по списку идентификаторов
		/// </summary>
		/// <param name="ids">Список идентификаторов удаляемых объектов AutoCAD</param>
		public static void EraseObjects(ObjectIdCollection ids)
		{
			List<ObjectId> list = new List<ObjectId>();
			foreach (ObjectId id in ids)
				list.Add(id);
			EraseObjects(list);
		}

		/// <summary>
		/// Удаление объектов по списку дескрипторов
		/// </summary>
		/// <param name="handleList">Список дескрипторов удаляемых объектов AutoCAD</param>
		/// <param name="trans">Текущая транзакция AutoCAD</param>
		public static void EraseObjects(List<Handle> handleList, Transaction trans)
		{
			EraseObjects((IEnumerable<Handle>)handleList, trans);
		}

		/// <summary>
		/// Удаление объектов по списку дескрипторов
		/// </summary>
		/// <param name="handleList">Список дескрипторов удаляемых объектов AutoCAD</param>
		/// <param name="trans">Текущая транзакция AutoCAD</param>
		public static void EraseObjects(IEnumerable<Handle> handleList, Transaction trans)
		{
			try
			{
				if (trans == null || handleList == null) return;
				TransactionManager tm = trans.TransactionManager;

				// Запускаем транзакцию
				foreach (Handle handle in handleList)
				{
					EraseObject(handle, trans);
				}
				tm.QueueForGraphicsFlush();
			}
			catch (Exception ex)
			{
				EditorRoutine.WriteMessage(ex.ToString());
			}
		}

		/// <summary>
		/// Удаление объектов по списку дескрипторов
		/// </summary>
		/// <param name="handleList">Список дескрипторов удаляемых объектов AutoCAD</param>
		public static void EraseObjects(IEnumerable<Handle> handleList)
		{
			try
			{
				// Рабочая база
				Database db = WorkingDatabase;
				if (db == null) return;
				TransactionManager tm = db.TransactionManager;

				// Запускаем транзакцию
				using (Transaction myT = tm.StartTransaction())
				{
					EraseObjects(handleList, myT);
					myT.Commit();
				}
			}
			catch (Exception ex)
			{
				EditorRoutine.WriteMessage(ex.ToString());
			}
		}

		/// <summary>
		/// Удаление объектов по списку дескрипторов
		/// </summary>
		/// <param name="handleList">Список дескрипторов удаляемых объектов AutoCAD</param>
		public static void EraseObjects(List<Handle> handleList)
		{
			EraseObjects((IEnumerable<Handle>)handleList);
		}

		/// <summary>
		/// Удаление объектов по списку дескрипторов
		/// </summary>
		/// <param name="handleList">Список дескрипторов удаляемых объектов AutoCAD</param>
		public static void EraseObjects(IEnumerable<long> handleList)
		{
			try
			{
				// Рабочая база
				Database db = WorkingDatabase;
				if (db == null) return;
				TransactionManager tm = db.TransactionManager;

				// Запускаем транзакцию
				using (Transaction myT = tm.StartTransaction())
				{
					EraseObjects(handleList, myT);
					myT.Commit();
				}
			}
			catch (Exception ex)
			{
				EditorRoutine.WriteMessage(ex.ToString());
			}
		}

		/// <summary>
		/// Удаление объектов по списку дескрипторов
		/// </summary>
		/// <param name="handleList">Список дескрипторов удаляемых объектов AutoCAD</param>
		/// <param name="trans">Текущая транзакция AutoCAD</param>
		public static void EraseObjects(IEnumerable<long> handleList, Transaction trans)
		{
			try
			{
				if (trans == null || handleList == null) return;
				TransactionManager tm = trans.TransactionManager;

				// Запускаем транзакцию
				foreach (long handle in handleList)
				{
					EraseObject(handle, trans);
				}
				tm.QueueForGraphicsFlush();
			}
			catch (Exception ex)
			{
				EditorRoutine.WriteMessage(ex.ToString());
			}
		}

		#endregion

		#region Работа с транзакциями
		/// <summary>
		/// Запуск транзакции в указанной базе
		/// Необходима проверка на null
		/// После использования необходимо вызвать Commit() для правильного завершения транзакции
		/// </summary>
		/// <param name="db">База данных в которой запускается транзакция</param>
		/// <param name="generateException">Если<c>true</c> [генерировать исключения при ошибке].</param>
		/// <returns>Открытая транзакция</returns>
		public static Transaction StartTransaction(Database db, bool generateException)
		{
			try
			{
				if (db == null)
				{
					if (generateException)
						throw new Exception("DBRoutine::StartTransaction Database == null");
					return null;
				}
				TransactionManager tm = db.TransactionManager;
				return tm.StartTransaction();
			}
			catch (Exception ex)
			{
				if (generateException)
					throw new Exception("DBRoutine::StartTransaction Ошибка открытия транзакции", ex);
				EditorRoutine.WriteMessage(ex.ToString());
			}
			return null;
		}

        /// <summary>
        /// Запуск транзакции в указанной базе
        /// Необходима проверка на null
        /// После использования необходимо вызвать Commit() для правильного завершения транзакции
        /// </summary>
        /// <param name="db">База данных в которой запускается транзакция</param>
        /// <returns>Открытая транзакция</returns>
        public static Transaction StartTransaction(Database db)
        {
            return StartTransaction(db, false);
        }

		/// <summary>
		/// Запуск транзакции в текущей базе
		/// Необходима проверка на null
		/// После использования необходимо вызвать Commit() для правильного завершения транзакции
		/// </summary>
		/// <returns>Открытая транзакция AutoCAD</returns>
		public static Transaction StartTransaction()
		{
			return StartTransaction(WorkingDatabase);
		}

		/// <summary>
		/// Запуск транзакции в текущей базе с указанием генерации исключений
		/// </summary>
		/// <param name="generateException">Если <c>true</c>, то генерировать исключения при ошибке.</param>
		/// <returns>Открытая транзакция AutoCAD</returns>
		public static Transaction StartTransaction(bool generateException)
		{
			return StartTransaction(WorkingDatabase, generateException);
		}
		#endregion

		/// <summary>
		/// Регистрация приложения в базе. Например для xData
		/// </summary>
		/// <param name="appName">Имя приложения</param>
		/// <remarks>Этот метод достался в наследство, и толком не тестировался. Так что обращаться с осторожностью</remarks>
		public static void RegistryApplication(string appName)
		{
			// Открываем транзакцию
			using (Transaction trans = StartTransaction())
			{
				try
				{
					// Получаем таблицу приложений
					RegAppTable regApp = (RegAppTable)trans.GetObject(WorkingDatabase.RegAppTableId, OpenMode.ForWrite, false);

					// Проверяем, есть ли такое приложение в зарегистрированных
					if (!regApp.Has(appName))
					{
						// Регистрация нового приложения
						RegAppTableRecord rtr = new RegAppTableRecord();
						rtr.Name = appName;
						// Регистрируем приложение
						regApp.Add(rtr);

						// Завершительные действия с транзакциями по закрытию вновь созданного объекта
						trans.TransactionManager.AddNewlyCreatedDBObject(rtr, true);
					}
					// Завершаем транзакцию
					trans.Commit();
				}
				catch (Exception ex)
				{
					EditorRoutine.WriteMessage(ex.ToString());
					trans.Abort();
				}
			}
		}
	}
}
