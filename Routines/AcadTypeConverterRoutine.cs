// Copyright (c) 2012 Сергей Попов, Виталий Сайдин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Sergey Popov, Vitaliy Saydin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;

using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;

namespace MAcadFramework.Routines
{
    /// <summary>
    /// Преобразует объекты БД AutoCAD'a к геометрическим объектам и обратно.
    /// </summary>
  public static class AcadTypeConverterRoutine
  {
    public static Curve2d DBCurveToGeo(Curve curve)
    {
      if(curve.GetType() == typeof(Arc))
      {
        return DBArcToGeo(curve as Arc);
      }
      if (curve.GetType() == typeof(Line))
      {
        return DBLineToGeo(curve as Line);
      }
      if (curve.GetType() == typeof(Circle))
      {
        return DBCircToGeo(curve as Circle);
      }
      if (curve.GetType() == typeof(Ellipse))
      {
        return DBEllipseToGeo(curve as Ellipse);
      }
    return null;
    }

    public static CircularArc2d DBArcToGeo(Arc arc)
    {
      if (arc == null) return null;
      Point2d cen2d = new Point2d(arc.Center.X, arc.Center.Y);
      double rad = arc.Radius;
      double stAng = arc.StartAngle;
      double endAng = arc.EndAngle;
      Vector3d norm = arc.Normal;

      bool isClockWise = false;
      if (norm.IsCodirectionalTo(Vector3d.ZAxis) == false)
        isClockWise = true;

      CircularArc2d c2d = new CircularArc2d(cen2d, rad, stAng, endAng, Vector2d.XAxis, isClockWise);

      if (isClockWise)
      {
        c2d.ReferenceVector = c2d.ReferenceVector.Negate();
      }
      return c2d;
    }

    public static Arc GeoToDBArc(CircularArc2d geoArc)
    {
      throw new Exception("Функция не реализована!");
    }

    public static LineSegment2d DBLineToGeo(Line line)
    {
      if (line == null) return null;
      Point2d pt2d1 = new Point2d(line.StartPoint.X, line.StartPoint.Y);
      Point2d pt2d2 = new Point2d(line.EndPoint.X, line.EndPoint.Y);

      LineSegment2d c2d = new LineSegment2d(pt2d1, pt2d2);
      return c2d;
    }

    public static Line GeoToDBLine(LineSegment2d geoLine)
    {
      throw new Exception("Функция не реализована!");
    }

    public static CircularArc2d DBCircToGeo(Circle circ)
    {
      if (circ == null) return null;
      Point2d cen2d = new Point2d(circ.Center.X, circ.Center.Y);
      double rad = circ.Radius;

      CircularArc2d c2d = new CircularArc2d(cen2d, rad);
      return c2d;
    }

    public static Circle GeoToDBCirc(CircularArc2d geoCirc)
    {
      throw new Exception("Функция не реализована!");
    }

    public static EllipticalArc2d DBEllipseToGeo(Ellipse ellipse)
    {
      if (ellipse == null) return null;
      Point3d center = ellipse.Center;
      Vector3d majorAxis = ellipse.MajorAxis;
      Vector3d minorAxis = ellipse.MinorAxis;
      double startAngle = ellipse.StartAngle;
      double endAngle = ellipse.EndAngle;

        Point2d cen2d = new Point2d(center.X, center.Y);
      Vector2d majorAxis2d = new Vector2d(majorAxis.X, majorAxis.Y);
      majorAxis2d = majorAxis2d.GetNormal();

      Vector2d minorAxis2d = new Vector2d(minorAxis.X, minorAxis.Y);
      minorAxis2d = minorAxis2d.GetNormal();

      double majorRadius = majorAxis.Length;
      double minorRadius = minorAxis.Length;

      EllipticalArc2d c2d = new EllipticalArc2d(cen2d, majorAxis2d, minorAxis2d,
                      majorRadius, minorRadius, startAngle, endAngle);

      return c2d;
    }

    public static Ellipse GeoToDBEllipse(EllipticalArc2d geoEllipse)
    {
      throw new Exception("Функция не реализована!");
    }
  }
}
