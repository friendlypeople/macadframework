// Copyright (c) 2012 Сергей Попов, Виталий Сайдин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Sergey Popov, Vitaliy Saydin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System.Collections.Generic;
using Autodesk.AutoCAD.DatabaseServices;
using MAcadFramework.Aggregates;

namespace MAcadFramework.Routines
{
	/// <summary>
	/// Класс облегчающий работу с группами AutoCAD, реализуя часто встречающиеся операции
	/// </summary>
	public class AcadGroupRoutine
	{
		/// <summary>Получить имена всех групп базы</summary>
		/// <param name="includeNoneSelectable">Если <c>true</c> то включать имена не выбираемых групп.</param>
		/// <returns>Список имен групп базы</returns>
		public static IEnumerable<string> GetGroupNames(bool includeNoneSelectable)
		{
			DBDictionary dictionary = GetReadonlyGroupCollection();
			
			List<string> stringList = new List<string>();

			foreach (DBDictionaryEntry entry in dictionary)
			{
				Group group = EntityProvider.GetObjectPointer<Group>(entry.Value);
				if (group == null) continue;

				if (!group.Selectable && !includeNoneSelectable) continue;
				stringList.Add(group.Name);
			}
			return stringList;
		}


		/// <summary>
		/// Определяет наличие группы с заданным именем в базе
		/// </summary>
		/// <param name="groupName">Имя группы.</param>
		/// <returns><c>true</c> если группа присутствует, иначе <c>false</c></returns>
		public static bool Exists(string groupName)
		{
			DBDictionary dictionary = GetReadonlyGroupCollection();
			return dictionary.Contains(groupName);
		}

		/// <summary>
		/// Создать группу в базе
		/// </summary>
		/// <param name="groupName">Имя группы</param>
		/// <returns><c>true</c> если группа создана успешна, иначе <c>false</c></returns>
		public static bool MakeGroup(string groupName)
		{
			return MakeGroup(groupName, "", new ObjectId[]{ });
		}

		/// <summary>
		/// Создать группу в базе
		/// </summary>
		/// <param name="groupName">Имя группы</param>
		/// <param name="entities">Список примитивов для добавления в группу.</param>
		/// <returns><c>true</c> если группа создана успешна, иначе <c>false</c></returns>
		public static bool MakeGroup(string groupName, IEnumerable<ObjectId> entities)
		{
			return MakeGroup(groupName, "", entities);
		}

		/// <summary>
		/// Создать группу в базе
		/// </summary>
		/// <param name="groupName">Имя группы</param>
		/// <param name="groupDesc">Описание группы.</param>
		/// <param name="entities">Список примитивов для добавления в группу.</param>
		/// <returns><c>true</c> если группа создана успешна, иначе <c>false</c></returns>
		public static bool MakeGroup(string groupName, string groupDesc, IEnumerable<ObjectId> entities)
		{
			using (Transaction trans = DBRoutine.StartTransaction(true))
			{
				DBObject dbObject = trans.GetObject(DBRoutine.WellKnownTableId.GroupTableId, OpenMode.ForWrite);
				DBDictionary dic = dbObject as DBDictionary;
				if (dic == null) return false;

				Group group;
				if (dic.Contains(groupName))
				{
					group = (Group) trans.GetObject(dic.GetAt(groupName), OpenMode.ForWrite);
				}
				else
				{
					group = new Group(groupDesc, true);
					dic.SetAt(groupName, group);
					trans.AddNewlyCreatedDBObject(group, true);
				}

				if (group == null) return false;
				foreach (ObjectId id in entities)
					group.Append(id);

				trans.Commit();
			}
			return true;
		}

		/// <summary>
		/// Удалить группу по имени
		/// </summary>
		/// <param name="groupName">Имя группы для удаления</param>
		/// <returns><c>true</c> если группа удалена, иначе <c>false</c></returns>
		public static bool RemoveGroup(string groupName)
		{
			return RemoveGroup(groupName, true);
		}

		/// <summary>
		/// Удалить группу по имени
		/// </summary>
		/// <param name="groupName">Name of the group.</param>
		/// <param name="includeEntities">Если <c>true</c> то удалять вместе с элеметами группы.</param>
		/// <returns><c>true</c> если группа удалена, иначе <c>false</c></returns>
		public static bool RemoveGroup(string groupName, bool includeEntities)
		{
			using (Transaction trans = DBRoutine.StartTransaction(true))
			{
				DBObject dbObject = trans.GetObject(DBRoutine.WellKnownTableId.GroupTableId, OpenMode.ForWrite);
				DBDictionary dic = dbObject as DBDictionary;
				if (dic == null) return false;

				if (!dic.Contains(groupName))
					return false;

				if (includeEntities)
				{
					Group group = (Group)trans.GetObject(dic.GetAt(groupName), OpenMode.ForRead);
					if (group != null)
					{
						ObjectId[] ids = group.GetAllEntityIds();
						DBRoutine.EraseObjects(ids, trans);
					}
				}

				ObjectId id = dic.Remove(groupName);
				trans.Commit();
				return id != ObjectId.Null;
			}
		}

		/// <summary> Получить идентфикаторы объектов входящих в группу </summary>
		/// <param name="groupName">Имя группы.</param>
		/// <returns>Список идентификаторов</returns>
		public static IEnumerable<ObjectId> GetGroupEntities(string groupName)
		{
			List<ObjectId> list = new List<ObjectId>();
			DBDictionary dictionary = GetReadonlyGroupCollection();
			if (dictionary.Contains(groupName))
			{
				ObjectId objectId = dictionary.GetAt(groupName);
				Group group = EntityProvider.GetObjectPointer<Group>(objectId);
				list.AddRange(group.GetAllEntityIds());
			}
			return list;
		}

		private static DBDictionary GetReadonlyGroupCollection()
		{
			return DBRoutine.GetObject<DBDictionary>(DBRoutine.WellKnownTableId.GroupTableId);
		}
	}
}
