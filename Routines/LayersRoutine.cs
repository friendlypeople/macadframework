// Copyright (c) 2012 Сергей Попов, Виталий Сайдин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Sergey Popov, Vitaliy Saydin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections.Generic;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.LayerManager;

namespace MAcadFramework.Routines
{
	/// <summary>
	/// Класс облегчающий работу со слоями, реализуя часто встречающиеся операции
	/// </summary>
	public static class LayersRoutine
	{
		/// <summary>
		/// Получение списка слоев в текущем чертеже
		/// </summary>
		/// <param name="includeFrozen"><c>true</c> если включать выключенные слои, иначе <c>false</c></param>
		/// <param name="includeOff"><c>true</c> если включать замороженные слои, иначе <c>false</c></param>
		/// <returns>Список имен найденных слоев</returns>
		public static List<string> LayersList(bool includeFrozen, bool includeOff)
		{
			using (Transaction trans = DBRoutine.StartTransaction())
			{
				List<string> layersNameList = new List<string>();
				try
				{
					// Получаем таблицу слоев
					LayerTable allLayers = (LayerTable)trans.GetObject(DBRoutine.WellKnownTableId.LayerTableId, OpenMode.ForRead, false);
					if (allLayers == null)
					{
						trans.Abort();
						return layersNameList;
					}

					foreach (ObjectId layerId in allLayers)
					{
						LayerTableRecord layer = (LayerTableRecord)trans.GetObject(layerId, OpenMode.ForRead, false);
						// Если слой заморожен, а мы замороженные не включаем, то пропускаем
						if (layer.IsFrozen && !includeFrozen) continue;

						// Если слой скрыт, а мы скрытые не включаем, то пропускаем
						if (layer.IsOff && !includeOff) continue;

						// Добавляем слой в список
						layersNameList.Add(layer.Name);
					}
					trans.Commit();
				}
				catch
				{
					trans.Abort();
				}
				return layersNameList;
			}
		}

		/// <summary>
		/// Получить имя текущего слоя
		/// </summary>
		/// <returns>Имя текущего слоя</returns>
		public static string CurrentLayer()
		{
			using (Transaction trans = DBRoutine.StartTransaction())
			{
				try
				{
					ObjectId currLayerId = DBRoutine.WorkingDatabase.Clayer;
					LayerTableRecord layer = (LayerTableRecord)trans.GetObject(currLayerId, OpenMode.ForRead, false);
					string layerName = layer.Name;
					trans.Commit();
					return layerName;
				}
				catch { trans.Abort(); }
				return String.Empty;
			}
		}

		/// <summary>
		/// Установка текущего слоя
		/// </summary>
		/// <param name="layerName">Имя слоя</param>
		/// <returns><c>true</c> если операции выполнена успешна, иначе <c>false</c></returns>
		public static bool SetCurrentLayer(string layerName)
		{
			try
			{
				using (EditorRoutine.LockDoc())
				{
					ObjectId needLayerId = LayerId(layerName);
					if (needLayerId.IsValid)
					{
						DBRoutine.WorkingDatabase.Clayer = needLayerId;
						return true;
					}
				}
			}
			catch (Exception ex)
			{
				EditorRoutine.WriteMessage(ex.ToString());
			}
			return false;
		}

		/// <summary>
		/// Проверка существования слоя
		/// </summary>
		/// <param name="layerName">Имя слоя</param>
		/// <param name="lookForErased"><c>true</c> если учитывать удаленные слои, иначе<c>false</c></param>
		/// <returns><c>true</c> если операции выполнена успешна, иначе <c>false</c> </returns>
		public static bool Exists(string layerName, bool lookForErased)
		{
			using (Transaction trans = DBRoutine.StartTransaction())
			{
				try
				{
					// Получаем таблицу слоев
					LayerTable allLayers = (LayerTable)trans.GetObject(DBRoutine.WellKnownTableId.LayerTableId, OpenMode.ForRead, false);
					if (allLayers != null)
					{
						foreach (ObjectId layerId in allLayers)
						{
							if (!lookForErased && layerId.IsErased) continue;

							// Открываем слой
							LayerTableRecord layer = (LayerTableRecord) trans.GetObject(layerId, OpenMode.ForRead, lookForErased);
							// Если слой заморожен, а мы замороженные не включаем, то пропускаем
							if (layer.Name.CompareTo(layerName) == 0)
							{
								trans.Commit();
								return true;
							}
						}
					}
					trans.Commit();
					
				}
				catch (Exception ex)
				{
					EditorRoutine.WriteMessage(ex.ToString());
					trans.Abort();
				}
				return false;
			}
		}

		/// <summary>
		/// Получение id слоя, который не был удален
		/// </summary>
		/// <param name="layerName">Имя слоя</param>
		/// <returns> Идентификатор объекта AutoCAD </returns>
		public static ObjectId LayerId(string layerName)
		{
			using (Transaction trans = DBRoutine.StartTransaction())
			{
				try
				{
					// Получаем таблицу слоев
					LayerTable allLayers = (LayerTable)trans.GetObject(DBRoutine.WellKnownTableId.LayerTableId, OpenMode.ForRead, false);
					if (allLayers != null)
					{
						// Если есть в таблице слоев слой с именем и он не удален...
						foreach (ObjectId layerId in allLayers)
						{
							if (layerId.IsErased) continue;
							// Открываем слой
							LayerTableRecord layer = (LayerTableRecord) trans.GetObject(layerId, OpenMode.ForRead, false);
							// Проверка по имени слоя
							if (layer.Name.CompareTo(layerName) == 0)
							{
								trans.Commit();
								return layer.Id;
							}
						}
					}
					trans.Commit();
					
				}
				catch (Exception ex)
				{
					EditorRoutine.WriteMessage(ex.ToString());
					trans.Abort();
				}
				return ObjectId.Null;
			}
		}

		/// <summary>
		/// Установить видимость слоя
		/// </summary>
		/// <param name="layerName">Имя слоя.</param>
		/// <param name="visible">Если <c>true</c> [видимый].</param>
		/// <returns><c>true</c> если операции выполнена успешна, иначе <c>false</c></returns>
		public static bool SetLayerVisible(string layerName, bool visible)
		{
			using (Transaction trans = DBRoutine.StartTransaction())
			{
				try
				{
					// Получаем таблицу слоев
					LayerTable allLayers = (LayerTable)trans.GetObject(DBRoutine.WellKnownTableId.LayerTableId, OpenMode.ForWrite, false);
					if (allLayers != null)
					{
						// Если есть в таблице слоев слой с именем...
						if (allLayers.Has(layerName))
						{
							LayerTableRecord layer = (LayerTableRecord) trans.GetObject(allLayers[layerName], OpenMode.ForWrite, false);
							// Видимость и замороженность слоя
							layer.IsOff = !visible;

							if (layer.IsFrozen && visible)
							{
								layer.IsFrozen = false;
							}
							trans.TransactionManager.QueueForGraphicsFlush();
							trans.Commit();
							return true;
						}
					}
					trans.Commit();
				}
				catch (Exception exc)
				{
					EditorRoutine.WriteMessage(exc.ToString());
					trans.Abort();
				}
				return false;
			}
		}

		/// <summary>
		/// Установить блокировку слоя
		/// </summary>
		/// <param name="layerName">Имя слоя.</param>
		/// <param name="locking">Если <c>true</c> [блокировать].</param>
		/// <returns><c>true</c> если операции выполнена успешна, иначе <c>false</c></returns>
		public static bool SetLayerLocked(string layerName, bool locking)
		{
			using (Transaction trans = DBRoutine.StartTransaction())
			{
				try
				{
					// Получаем таблицу слоев
					LayerTable allLayers = (LayerTable)trans.GetObject(DBRoutine.WellKnownTableId.LayerTableId, OpenMode.ForWrite, false);
					if (allLayers != null)
					{
						// Если есть в таблице слоев слой с именем...
						if (allLayers.Has(layerName))
						{
							LayerTableRecord layer = (LayerTableRecord) trans.GetObject(allLayers[layerName], OpenMode.ForWrite, false);
							// Блокировка слоя
							layer.IsLocked = locking;

							trans.Commit();
							return true;
						}
					}
					trans.Commit();
				}
				catch (Exception exc)
				{
					EditorRoutine.WriteMessage(exc.ToString());
					trans.Abort();
				}
				return false;
			}
		}

		/// <summary>
		/// Проверить, заблокирован ли слой
		/// </summary>
		/// <param name="layerName">Имя слоя</param>
		/// <returns><c>true</c> если слой заблокирован, иначе <c>false</c> </returns>
		public static bool IsLayerLocked(string layerName)
		{
			using (Transaction trans = DBRoutine.StartTransaction())
			{
				try
				{
					// Получаем таблицу слоев
					LayerTable allLayers =
						(LayerTable) trans.GetObject(DBRoutine.WellKnownTableId.LayerTableId, OpenMode.ForWrite, false);
					if (allLayers != null)
					{
						// Если есть в таблице слоев слой с именем...
						if (allLayers.Has(layerName))
						{
							LayerTableRecord layer = (LayerTableRecord) trans.GetObject(allLayers[layerName], OpenMode.ForWrite, false);

							bool locking = layer.IsLocked;
							trans.Commit();
							return locking;
						}
					}
					trans.Commit();
				}
				catch (Exception exc)
				{
					EditorRoutine.WriteMessage(exc.Message);
					trans.Abort();
				}
			}
			return false;
		}

		/// <summary>
		/// Создание нового слоя
		/// </summary>
		/// <remarks>
		/// Создает новый слой в базе AutoCAD по имени при условии отсутствия такого слоя.
		/// </remarks>
		/// <param name="layerName">Имя слоя</param>
		/// <returns> <c>true</c> если операции выполнена успешна, иначе <c>false</c> </returns>
		public static bool MakeLayer(string layerName)
		{
			using (Transaction trans = DBRoutine.StartTransaction())
			{
				try
				{
					// Получаем таблицу слоев
					LayerTable allLayers = (LayerTable)trans.GetObject(DBRoutine.WellKnownTableId.LayerTableId, OpenMode.ForWrite, false);
					if (allLayers != null)
					{
						// Если есть в таблице слоев слой с именем...
						if (!Exists(layerName, false))
						{
							LayerTableRecord layer = new LayerTableRecord();
							layer.Name = layerName;
							allLayers.Add(layer);

							// Завершительные действия с транзакциями по закрытию вновь созданного объекта
							trans.TransactionManager.AddNewlyCreatedDBObject(layer, true);
							trans.Commit();
							return true;
						}
					}
					
				}
				catch (Exception ex)
				{
					EditorRoutine.WriteMessage(ex.ToString());
					trans.Abort();
				}
				return false;
			}
		}

		/// <summary>
		/// Удалить все объекты со слоя layerName
		/// </summary>
		/// <param name="layerName">Имя слоя</param>
		/// <param name="tr">Текущая транзакция AutoCAD</param>
		/// <returns><c>true</c> если операции выполнена успешна, иначе <c>false</c></returns>
		public static bool EraseAllObjectsOnLayer(string layerName, Transaction tr)
		{
			// Запускаем транзакцию
			try
			{
				//ObjectId addedObjectId = ObjectId.Null;
				// Таблица блоков (на чтение)
				BlockTable bt = (BlockTable)tr.GetObject(DBRoutine.WellKnownTableId.BlockTableId, OpenMode.ForRead, false);

				// Запись таблицы блоков, описывающая пространство модели (на запись)
				BlockTableRecord btr = (BlockTableRecord)tr.GetObject(bt[BlockTableRecord.ModelSpace], OpenMode.ForRead, false);

				foreach (ObjectId id in btr)
				{
					DBObject obj = tr.GetObject(id, OpenMode.ForRead, false);
					if (obj == null) continue;
					if (obj is Entity)
					{
						Entity ent = (Entity) obj;
						if (ent.Layer != layerName) continue;
						ent.UpgradeOpen();
						ent.Erase();
					}
				}
				// Запись таблицы блоков, описывающая пространство листа (на запись)
				BlockTableRecord btrPs = (BlockTableRecord)tr.GetObject(bt[BlockTableRecord.PaperSpace], OpenMode.ForRead, false);

				foreach (ObjectId id in btrPs)
				{
					DBObject obj = tr.GetObject(id, OpenMode.ForRead, false);
					if (obj == null) continue;
					if (obj is Entity)
					{
						Entity ent = (Entity) obj;
						if (ent.Layer != layerName) continue;
						ent.UpgradeOpen();
						ent.Erase();
					}
				}
				return true;
			}
			catch (Exception ex)
			{
				EditorRoutine.WriteMessage(ex.ToString());
			}
			return false;
		}

		/// <summary>
		/// Удалить все объекты со слоя layerName
		/// </summary>
		/// <param name="layerName">Имя слоя</param>
		/// <returns> <c>true</c> если операции выполнена успешна, иначе <c>false</c> </returns>
		public static bool EraseAllObjectsOnLayer(string layerName)
		{
			// Запускаем транзакцию
			using (Transaction myT = DBRoutine.StartTransaction())
			{
				try
				{
					bool ret = EraseAllObjectsOnLayer(layerName, myT);
					myT.Commit();
					return ret;
				}
				catch (Exception ex)
				{
					EditorRoutine.WriteMessage(ex.ToString());
					myT.Abort();
				}
				return false;
			}
		}

		/// <summary>
		/// Удаление слоя
		/// </summary>
		/// <param name="layerName">Имя слоя</param>
		/// <param name="eraseAllObjects">Удалять ли объекты со слоя</param>
		/// <returns><c>true</c> если операции выполнена успешна, иначе <c>false</c></returns>
		/// <remarks>
		/// Если на слое есть объекты, они переместяться на слой 0!
		/// Параметр eraseAllObjects регламетирует полное удаление объектов с данного слоя
		/// </remarks>
		public static bool RemoveLayer(string layerName, bool eraseAllObjects)
		{
			if (layerName == "0") return false;
			// Если текущим является слой layerName, то делаем текущим неудаляемый слой "0"
			if (CurrentLayer() == layerName)
			{
				if (!SetCurrentLayer("0")) return false;
			}

			using (Transaction trans = DBRoutine.StartTransaction())
			{
				// Удаляем все объекты со слоя
				if (eraseAllObjects)
				{
					EraseAllObjectsOnLayer(layerName, trans);
				}

				try
				{
					// Получаем таблицу слоев
					LayerTable allLayers = (LayerTable)trans.GetObject(DBRoutine.WellKnownTableId.LayerTableId, OpenMode.ForRead, false);
					// Если пусто или нет такого слоя, то выходим
					if (allLayers == null || !Exists(layerName, false))
					{
						trans.Abort();
						return false;
					}
					ObjectId layId = LayerId(layerName);
					if (!layId.IsValid)
					{
						trans.Abort();
						return false;
					}
					DBObject obj = trans.GetObject(layId, OpenMode.ForRead, false);
					if (obj == null)
					{
						trans.Abort();
						return false;
					}
					if (obj.Id.IsErased)
					{
						trans.Abort();
						return false;
					}

					obj.UpgradeOpen();
					obj.Erase();
					trans.Commit();
					return true;
				}
				catch (Exception e)
				{
					EditorRoutine.WriteMessage(e.ToString());
					trans.Abort();
				}
				return false;
			}
		}

		/// <summary>
		/// Создать группу слоев
		/// </summary>
		/// <param name="layerPrefix">Префикс имени слоя для помещения в группу</param>
		/// <param name="layerGroupName">Имя группы слоев</param>
		static public void CreateLayerGroup(string layerPrefix, string layerGroupName)
		{
			Database db = DBRoutine.WorkingDatabase;

			// Список ID выбранных слоев.
			ObjectIdCollection lids = new ObjectIdCollection();

			// Запуск транзакции
			Transaction tr = db.TransactionManager.StartTransaction();
			using (tr)
			{
				// Получаем таблицу слоев
				LayerTable lt = (LayerTable)tr.GetObject(db.LayerTableId, OpenMode.ForRead);

				// Цикл по всем объектам в таблице слоев
				foreach (ObjectId lid in lt)
				{
					// Конкретная запись таблицы слоев
					LayerTableRecord ltr = (LayerTableRecord)tr.GetObject(lid, OpenMode.ForRead);
					// Если имя слоя начинается с указанного префикса
					if (ltr.Name.StartsWith(layerPrefix))
					{
						// Добавляем ID слоя в набор
						lids.Add(lid);
					}
				}
			}

			// Теперь создаем группу
			try
			{
				if (lids.Count > 0)
				{
					// Получаем текущее дерево фильтров слоев
					LayerFilterTree lft = db.LayerFilters;
					LayerFilterCollection lfc = lft.Root.NestedFilters;

					// Создаем новую группу слоев
					LayerGroup lg = new LayerGroup();

					// Имя ей
					lg.Name = layerGroupName;

					// Добавляем наши слои к списку слоев группы
					foreach (ObjectId id in lids)
						lg.LayerIds.Add(id);

					// Добавляем группу в коллекцию
					lfc.Add(lg);

					// Устанавливаем дерево фильтров обратно в БД
					db.LayerFilters = lft;
				}
			}

			catch (Exception ex)
			{
				EditorRoutine.WriteMessage(ex.ToString());
			}
		}

		/// <summary>
		/// Переносим объект на другой слой
		/// </summary>
		/// <param name="layer">Слой на который переносим</param>
		/// <param name="trans">Текущая транзакция AutoCAD</param>
		/// <param name="objIds">Массив идентификаторов объектов AutoCAD</param>
		public static void MoveObjectToLayer(string layer, Transaction trans, params ObjectId[] objIds)
		{
			try
			{
				foreach (ObjectId objId in objIds)
				{
					Entity dbObj = (Entity) trans.GetObject(objId, OpenMode.ForWrite, false, true);
					if (dbObj == null) continue;
					MakeLayer(layer);
					bool lockLayer = IsLayerLocked(layer);

					if (lockLayer)
						SetLayerLocked(layer, false);

					dbObj.Layer = layer;
					if (lockLayer)
						SetLayerLocked(layer, true);
				}
			}
			catch (Exception exc)
			{
				EditorRoutine.WriteMessage(exc.ToString());
			}
		}

		/// <summary>
		/// Переносим объект на другой слой
		/// </summary>
		/// <param name="layer">Слой на который переносим</param>
		/// <param name="objIds">Массив идентификаторов объектов AutoCAD</param>
		public static void MoveObjectToLayer(string layer, params ObjectId[] objIds)
		{
			using (Transaction tr = DBRoutine.StartTransaction())
			{
				try
				{
					MoveObjectToLayer(layer, tr, objIds);
					tr.Commit();
				}
				catch (Exception ex)
				{
					
					EditorRoutine.WriteMessage(ex.ToString());
					tr.Abort();
				}
			}
		}


		/// <summary>
		/// Копируем объект на другой слой
		/// </summary>
		/// <param name="layer">Слой на который копируем</param>
		/// <param name="trans">Текущая транзакция AutoCAD</param>
		/// <param name="objIds">Массив идентификаторов объектов AutoCAD</param>
		/// <returns> Массив полученных идентификаторов объектов </returns>
		public static ObjectId[] CopyObjectToLayer(string layer, Transaction trans, params ObjectId[] objIds)
		{
			List<ObjectId> ids = new List<ObjectId>(5);
			try
			{
				foreach (ObjectId objId in objIds)
				{
					Entity dbObj = (Entity) trans.GetObject(objId, OpenMode.ForRead, false, true);
					if (dbObj == null) continue;
					Entity entClone = dbObj.Clone() as Entity;
					if (entClone != null)
					{
						MakeLayer(layer);
						bool lockLayer = IsLayerLocked(layer);

						if (lockLayer)
							SetLayerLocked(layer, false);

						entClone.Layer = layer;
						ObjectId id = DBRoutine.AddToCurrentSpace(entClone);
						if (DBRoutine.CheckId(id))
							ids.Add(id);

						if (lockLayer)
							SetLayerLocked(layer, true);
					}
				}
			}
			catch (Exception ex)
			{
				EditorRoutine.WriteMessage(ex.ToString());
			}
			return ids.ToArray();
		}

		/// <summary>
		/// Копируем объект на другой слой
		/// </summary>
		/// <param name="layer">Слой на который копируем</param>
		/// <param name="objIds">Массив идентификаторов объектов AutoCAD</param>
		/// <returns>Массив полученных идентификаторов объектов </returns>
		public static ObjectId[] CopyObjectToLayer(string layer, params ObjectId[] objIds)
		{
			using (Transaction tr = DBRoutine.StartTransaction())
			{
				try
				{
					ObjectId[] ids = CopyObjectToLayer(layer, tr, objIds);
					tr.Commit();
					return ids;
				}
				catch (Exception ex)
				{
					tr.Abort();
					EditorRoutine.WriteMessage(ex.ToString());
				}
				
			}
			return new ObjectId[0];
		}
	}
}
