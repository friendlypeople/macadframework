// Copyright (c) 2012 Сергей Попов, Виталий Сайдин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Sergey Popov, Vitaliy Saydin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using MAcadFramework.Routines;

namespace MAcadFramework.Routines
{
	/// <summary>
	/// Класс облегчающий работу с вьюпортами, реализуя часто встречающиеся операции
	/// </summary>
	public static class ViewportsRoutine
	{
		/// <summary>
		/// Получить систему координат вьюпорта.
		/// </summary>
		/// <param name="vportId">Идентификатор вьюпорта AutoCAD</param>
		/// <returns>Матрица трансформации вьюпорта</returns>
		public static Matrix3d GetViewPortCoordinateSystem(ObjectId vportId)
		{
			// Обнуляем матрицу на всякий случай
			Matrix3d matrix = Matrix3d.Identity;
			if (vportId == ObjectId.Null || vportId.IsErased) return matrix;

			using (Transaction tr = DBRoutine.StartTransaction())
			{
				try
				{
					Viewport pVp = tr.GetObject(vportId, OpenMode.ForRead) as Viewport;
					if (pVp != null)
					{
						double scale = pVp.CustomScale; // Масштаб
						double ang = pVp.TwistAngle; // Угол поворота
						// Поправка на настройку TARGET (без нее работает некорректно после выполения команды CAMERA, изменяющей настройку TARGET (см.ScaledViewportErr3.zip))
						Point3d target = pVp.ViewTarget;
						Point3d vpCen = pVp.CenterPoint; // Центр вьюпорта на листе
						Point2d modelCen = pVp.ViewCenter + new Vector2d(target.X, target.Y); // Центр вьюпорта в координатах пространства модели


						// Устанавливаем трансформирующую матрицу
						matrix = Matrix3d.AlignCoordinateSystem(Point3d.Origin, Vector3d.XAxis, Vector3d.YAxis, Vector3d.ZAxis,
																new Point3d(vpCen.X - modelCen.X * scale, vpCen.Y - modelCen.Y * scale, 0),
																new Vector3d(Math.Cos(ang) * scale, Math.Sin(ang) * scale, 0),
																new Vector3d(-Math.Sin(ang) * scale, Math.Cos(ang) * scale, 0),
																new Vector3d(0, 0, scale));

					}
				}
				catch
				{
					matrix = Matrix3d.Identity;
				}
				tr.Commit();
			}
			return matrix;
		}

		/// <summary>
		/// Есть ли в таблице вьюпортов вьюпорт с заданным именем
		/// </summary>
		/// <param name="nameOfViewport">Имя вьюпорта</param>
		/// <returns>Есть/Нет</returns>
		public static bool HasNamedViewport(string nameOfViewport)
		{
			EditorRoutine.Editor.UpdateTiledViewportsInDatabase();

			Database db = DBRoutine.WorkingDatabase;
			if (db == null) return false;
			// ID таблицы вьюпортов модели
			ObjectId vportTableId = db.ViewportTableId;
			if (vportTableId.IsNull || !vportTableId.IsValid) return false;

			// Запускаем транзакцию
			using (Transaction tr = DBRoutine.StartTransaction())
			{
				try
				{
					// Открываем таблицу вьюпортов
					ViewportTable vportTable = tr.GetObject(vportTableId, OpenMode.ForRead) as ViewportTable;
					if (vportTable != null)
					{
						// Проверка наличия именованного вьюпорта
						return vportTable.Has(nameOfViewport);
					}
				}
				catch (Exception exc)
				{
					EditorRoutine.WriteMessage("Невозможно обработать таблицу видовых экранов!");
					System.Diagnostics.Debug.WriteLine("ViewsManipulate.HasNamedViewport.exc: " + exc.Message + "\n" + exc.StackTrace);
				}
				finally
				{
					tr.Commit();
				}
			}
			return false;
		}

		/// <summary>
		/// Зумировать все активные вьюпорты методом ZoomExtents
		/// </summary>
		public static void ZoomAllActiveViewports()
		{
			EditorRoutine.Editor.UpdateTiledViewportsInDatabase();

			Database db = DBRoutine.WorkingDatabase;
			if (db == null) return;
			// ID таблицы вьюпортов модели
			ObjectId vportTableId = db.ViewportTableId;
			if (vportTableId.IsNull || !vportTableId.IsValid) return;

			// Кол-во активных вьюпортов
			int actives = 0;
			using (Transaction tr = DBRoutine.StartTransaction())
			{
				try
				{
					// Открываем таблицу вьюпортов
					ViewportTable vportTable = tr.GetObject(vportTableId, OpenMode.ForRead) as ViewportTable;
					if (vportTable != null)
					{
						// Цикл по всем вьюпортам
						foreach (ObjectId vportId in vportTable)
						{
							// Открываем очередной вьюпорт
							ViewportTableRecord vportTableRecord = tr.GetObject(vportId, OpenMode.ForRead) as ViewportTableRecord;
							if (vportTableRecord == null) continue;
							if (vportTableRecord.Name == "*Active")
							{
								// Считаем кол-во активных (Отображаемых в ModelSpace)
								actives++;
							}
						}
					}
				}
				catch (Exception exc)
				{
					EditorRoutine.WriteMessage("Невозможно обработать активные видовые экраны!");
					System.Diagnostics.Debug.WriteLine("ViewsManipulate.ZoomAllViewports.exc: " + exc.Message + "\n" + exc.StackTrace);
				}
				tr.Commit();
			}


			// Сохраняем текущее значение CVPORT
			object saveCVportValue = Application.GetSystemVariable("CVPORT");
			try
			{
				for (int i = 2; i < actives + 2; i++)
				{
					Application.SetSystemVariable("CVPORT", i);
					ZoomRoutine.Zoom();
				}
			}
			catch (Exception exc1)
			{
				System.Diagnostics.Debug.WriteLine("ViewsManipulate.ZoomAllViewports.exc1: " + exc1.Message + "\n" + exc1.StackTrace);
			}
			// Восстанавливаем значение CVPORT
			Application.SetSystemVariable("CVPORT", saveCVportValue);
		}
	}
}
