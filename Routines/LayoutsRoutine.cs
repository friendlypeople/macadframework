// Copyright (c) 2012 Сергей Попов, Виталий Сайдин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Sergey Popov, Vitaliy Saydin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections.Generic;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Interop;
using MAcadFramework.Aggregates;

namespace MAcadFramework.Routines
{
	/// <summary>
	/// Класс облегчающий работу со лайоутами, реализуя часто встречающиеся операции
	/// </summary>
	public static class LayoutsRoutine
	{
		/// <summary>
		/// Создание лайоута с именем
		/// </summary>
		/// <param name="layoutName">Имя создаваемого лайоута</param>
		/// <returns> Идентификатор созданного или имеющегося лайоута  </returns>
		public static ObjectId MakeLayout(string layoutName)
		{
			LayoutManager layoutManager = LayoutManager.Current;

			ObjectId idLayout = layoutManager.GetLayoutId(layoutName);
			if (!idLayout.IsNull || idLayout.IsValid || idLayout.IsResident)
			{
				return idLayout;
			}
			return layoutManager.CreateLayout(layoutName);
		}

		/// <summary>
		/// Получение идентификатора лайоута по имени
		/// </summary>
		/// <param name="layoutName">Имя лайоута</param>
		/// <returns>Идентификатор лайоута</returns>
		public static ObjectId GetObjectId(string layoutName)
		{
			LayoutManager layoutManager = LayoutManager.Current;
			return layoutManager.GetLayoutId(layoutName);
		}

		/// <summary>
		/// Установить активным лайоут по имени
		/// </summary>
		/// <param name="layoutName">Имя лайоута</param>
		public static void SetActiveLayout(string layoutName)
		{
			// Менеджер лайоутов
			LayoutManager layoutManager = LayoutManager.Current;
			layoutManager.CurrentLayout = layoutName;
			EditorRoutine.Regen();
		}

		/// <summary>
		/// Зуммирование лайоута по принципу Extents
		/// </summary>
		public static void ZoomExtents(string layoutName)
		{
			// Установка текущим лайоута по имени
			SetActiveLayout(layoutName);
			// Зуммирование на пространстве модели
			AcadApplication app = (AcadApplication)Autodesk.AutoCAD.ApplicationServices.Application.AcadApplication;
			app.ZoomExtents();
		}

		/// <summary>
		/// Удалить лейоут
		/// </summary>
		/// <param name="layoutName">Имя лейоута</param>
		/// <remarks>
		/// Если метод выполняется не в контексте документа, то 
		/// необходимо использовать блокировку документа.
		/// </remarks>
		public static void DeleteLayout(string layoutName)
		{
			LayoutManager layoutManager = LayoutManager.Current;
			layoutManager.DeleteLayout(layoutName);
		}


		/// <summary>
		/// Получение объектов с лайоута
		/// </summary>
		/// <param name="layoutName">Имя лайоута</param>
		/// <returns>Список дескрипторов полученных объектов</returns>
		public static List<Handle> GetAllObjectsFromLayout(string layoutName)
		{
			// Запускаем транзакцию
			using (Transaction myT = DBRoutine.StartTransaction())
			{
				List<Handle> objs = new List<Handle>(25);
				try
				{
					// Таблица блоков (на чтение)
					ObjectId layoutId = LayoutManager.Current.GetLayoutId(layoutName);
					if (!DBRoutine.CheckId(layoutId))
					{
						myT.Abort();
						return objs;
					}

					Layout layout = (Layout)(myT.GetObject(layoutId, OpenMode.ForRead));
					if (layout != null)
					{
						BlockTableRecord btr = (BlockTableRecord)(myT.GetObject(layout.BlockTableRecordId, OpenMode.ForRead, false));
						if (btr != null)
						{
							foreach (ObjectId id in btr)
								objs.Add(id.Handle);
						}
					}
					myT.Commit();
				}
				catch (Exception ex)
				{
					EditorRoutine.WriteMessage(ex.ToString());
					myT.Abort();
				}
				return objs;
			}
		}

		/// <summary>
		/// Проверка на наличие лайоута
		/// </summary>
		/// <param name="layoutName">Имя лайоута</param>
		/// <returns> <c>true</c> Если лайоут найден, иначе <c>false</c> </returns>
		public static bool Exists(string layoutName)
		{
			LayoutManager layoutManager = LayoutManager.Current;

			ObjectId idLayout = layoutManager.GetLayoutId(layoutName);
			if (!idLayout.IsNull || idLayout.IsValid || idLayout.IsResident)
				return true;
			return false;
		}

		/// <summary>
		/// Очистить лайоута от примитивов
		/// </summary>
		/// <param name="layoutName">Имя лайоута</param>
		public static void ClearLayout(string layoutName)
		{
			try
			{
				List<Handle> objs = GetAllObjectsFromLayout(layoutName);
				DBRoutine.EraseObjects(objs);
			}
			catch (Exception exc)
			{
				EditorRoutine.WriteMessage(exc.ToString());
			}
		}

		/// <summary>
		/// Добавить примитив на пространство листа
		/// </summary>
		/// <param name="obj">Объект для добавления на лайоут</param>
		/// <param name="layoutName">Имя лайоута</param>
		/// <param name="trans">Открытая транзакция AutoCAD.</param>
		/// <returns> Идентификатор объекта AutoCAD </returns>
		public static ObjectId AddToPaperSpace(Entity obj, string layoutName, Transaction trans)
		{
			ObjectId addedObjectId = ObjectId.Null;
			// Менеджер лайоутов
			LayoutManager lm = LayoutManager.Current;
			// ID лайоута по имени
			ObjectId layId = lm.GetLayoutId(layoutName);

			// Объект Layout на чтение
			Layout layout = trans.GetObject(layId, OpenMode.ForRead) as Layout;
			if (layout != null)
			{
				// ID таблицы блоков для лайоута
				ObjectId btrId = layout.BlockTableRecordId;
				// Запись таблицы блоков, описывающая пространство лайоута
				BlockTableRecord btr = (BlockTableRecord)trans.GetObject(btrId, OpenMode.ForWrite, false);
				// Добавляем объект
				addedObjectId = btr.AppendEntity(obj);
				// Завершительные действия с вновь созданным объектом в транзакциях
				trans.AddNewlyCreatedDBObject(obj, true);
				trans.TransactionManager.QueueForGraphicsFlush();
			}
			return addedObjectId;
		}

		/// <summary>
		/// Добавить примитив на пространство листа
		/// </summary>
		/// <param name="obj">Объект для добавления на лайоут</param>
		/// <param name="layoutName">Имя лайоута</param>
		/// <returns>  Идентификатор объекта AutoCAD </returns>
		public static ObjectId AddToPaperSpace(Entity obj, string layoutName)
		{
			// Запускаем транзакцию
			using (Transaction myT = DBRoutine.StartTransaction())
			{
				ObjectId addedObjectId = ObjectId.Null;
				try
				{
					addedObjectId = AddToPaperSpace(obj, layoutName, myT);
					myT.Commit();
				}
				catch { myT.Abort(); }
				return addedObjectId;
			}
		}
	}
}
