// Copyright (c) 2012 Сергей Попов, Виталий Сайдин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Sergey Popov, Vitaliy Saydin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections.Generic;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using MAcadFramework.Aggregates;

namespace MAcadFramework.Routines
{
    /// <summary>
	/// Класс облегчающий работу с геометрией, реализуя часто встречающиеся операции
    /// </summary>
	public static class GeometryRoutine
	{
		/// <summary>
		/// Создание полилинии по Extents3d
		/// </summary>
		/// <param name="ext3d">Габаритная область примитива</param>
		/// <returns>Полилиния 3D</returns>
		public static Polyline3d MakePolyByExtents3d(Extents3d ext3d)
		{
			Point3dCollection vertexes = new Point3dCollection();

			vertexes.Add(ext3d.MinPoint);
			vertexes.Add(new Point3d(ext3d.MinPoint.X, ext3d.MaxPoint.Y, 0));
			vertexes.Add(ext3d.MaxPoint);
			vertexes.Add(new Point3d(ext3d.MaxPoint.X, ext3d.MinPoint.Y, 0));

			Polyline3d poly = new Polyline3d(Poly3dType.SimplePoly, vertexes, true);
			return poly;
		}


		/// <summary>
		/// Получить габаритную область примитива.
		/// </summary>
		/// <param name="handle">Дескриптор объекта AutoCAD</param>
		/// <returns>Габаритная область примитива</returns>
		public static Extents3d GetExtents(Handle handle)
		{
			return GetExtents(DBRoutine.GetObjectId(handle));
		}


		/// <summary>
		/// Получить габаритную область примитива.
		/// </summary>
		/// <param name="id">Идентификатор объекта AutoCAD</param>
		/// <returns>Габаритная область примитива</returns>
		public static Extents3d GetExtents(ObjectId id)
		{
			Extents3d ext = new Extents3d();
			try
			{
				Entity obj = EntityProvider.GetObjectPointer<Entity>(id);
				if (obj != null)
					ext.AddExtents(obj.GeometricExtents);
			}
			catch (Exception ex)
			{
				EditorRoutine.WriteMessage(ex.ToString());
			}

			return ext;
		}

		/// <summary>
		/// Получить границы по набору объектов
		/// </summary>
		/// <param name="objects">Набор объектов</param>
		/// <returns>Граница обрамляющая набор объектов</returns>
		public static Extents3d GetExtents(DBObjectCollection objects)
		{
			Extents3d ext = new Extents3d();
			try
			{
                
				if (objects == null) return ext;
				foreach (DBObject obj in objects)
				{
					Entity ent = obj as Entity;
					if (ent == null) continue;

                    try
                    {
                        Extents3d currExt = ent.GeometricExtents;
                        ext.AddExtents(currExt);
                    }
                    catch (Exception)
                    {
                    }
				}
			}
			catch(Exception ex)
			{
				EditorRoutine.WriteMessage(ex.ToString());
			}
			return ext;
		}

		/// <summary>
		/// Получить границы по набору объектов
		/// </summary>
		/// <param name="objects">Набор объектов</param>
		/// <returns>Граница обрамляющая набор объектов</returns>
		public static Extents3d GetExtents(List<Handle> objects)
		{
			Extents3d ext = new Extents3d();
			try
			{
				if (objects == null || objects.Count == 0) return ext;
				foreach (Handle h in objects)
				{
					Entity ent = EntityProvider.GetObjectPointerNoException<Entity>(h);
					if (ent == null) continue;
					Extents3d currExt = ent.GeometricExtents;
					ext.AddExtents(currExt);
				}
			}
			catch (Exception ex)
			{
				EditorRoutine.WriteMessage(ex.ToString());
			}
			return ext;
		}

		/// <summary>
		/// Пересечение плоскости с отрезком
		/// </summary>
		/// <param name="plane">Геометрическое представление плоскости</param>
		/// <param name="line">Отрезок</param>
		/// <returns>Список точек пересечения или null, если пересечения нет</returns>
		public static Point3d[] PlaneWithLineEntIntersec(Plane plane, Line line)
		{
			Curve crv = line;
			LineSegment3d ls = new LineSegment3d(crv.StartPoint, crv.EndPoint);

			return PlaneWithLineEntIntersec(plane, ls);

			// Работает только в ARX 2009
			//Point3d[] pts = plane.IntersectWith(ls);
			//return pts;
		}

		/// <summary>
		/// Пересечение плоскости с отрезком
		/// </summary>
		/// <param name="plane">Геометрическое представление плоскости</param>
		/// <param name="lineSeg">Отрезок</param>
		/// <returns>Список точек пересечения или null, если пересечения нет</returns>
		public static Point3d[] PlaneWithLineEntIntersec(Plane plane, LineSegment3d lineSeg)
		{
			// Работает только в ARX 2009
			//Point3d[] pts = plane.IntersectWith(lineSeg);
			//return pts;

			// Обходим так:
			Point3d[] pts = plane.ClosestPointToLinearEntity(lineSeg);
			if (pts == null) return null;
			if (pts.Length < 2) return null;
			if (pts[0].DistanceTo(pts[1]) > 0.001) return null;

			Point3d[] ourPts = new Point3d[1];
			ourPts[0] = pts[0];

			return ourPts;
		}

		/// <summary>
		/// Тест на определение вхождения региона в область между двумя плоскостями смотрящими нормалями
		/// друг на друга
		/// </summary>
		/// <param name="testRegion">Тестируемый регион</param>
		/// <param name="plane1">Первая плоскость</param>
		/// <param name="plane2">Вторая плоскость</param>
		/// <returns> <c>true</c> если регион хоть точкой попадает в область, иначе <c>false</c> </returns>
		public static bool TestRegionInsideTwoEyeTiEyePlanes(Region testRegion, Plane plane1, Plane plane2)
		{
			DBObjectCollection objects = ExplodeRegion(testRegion);

			// Цикл по объектам
			foreach (DBObject obj in objects)
			{
				// Приводим к типу кривой
				Curve crv = obj as Curve;
				if (crv == null) continue;

				crv.GetProjectedCurve(plane1, plane1.Normal);

			}

			return false;
		}

		/// <summary>
		/// Взорвать область.
		/// </summary>
		/// <param name="region">Область.</param>
		/// <returns>Набор объектов полученных в результате взрыва</returns>
		public static DBObjectCollection ExplodeRegion(Region region)
		{
			try
			{
				DBObjectCollection retCollection = new DBObjectCollection();
				if (region == null) return retCollection;
				DBObjectCollection regionObjs = new DBObjectCollection();
				region.Explode(regionObjs);
				foreach (DBObject obj in regionObjs)
				{
					if (obj is Region)
					{
						DBObjectCollection simpleObjs = ExplodeRegion(obj as Region);
						
						// Удаляем ненужный регион. 
						SafelyCreater.Dispose(obj);
						foreach (DBObject sObj in simpleObjs)
						{
							retCollection.Add(sObj);
						}
					}
					else
						retCollection.Add(obj);
				}
				return retCollection;
			}
			catch (Exception exc)
			{
				return new DBObjectCollection();
			}
		}

        /// <summary>
        /// Набор регионов преобразуем в связанные регионы
        /// </summary>
        /// <param name="regionsForUnion">Набор регионов для объединения</param>
		/// <returns>Набор итоговых регионов</returns>
		public static DBObjectCollection UnionRegions(DBObjectCollection regionsForUnion)
		{
        	if (regionsForUnion == null) throw new ArgumentNullException("regionsForUnion");

        	DBObjectCollection regions = new DBObjectCollection();
			if (regionsForUnion.Count < 1) return regions;

			// Делаем копию массива регионов
			foreach (DBObject oReg in regionsForUnion)
			{
				Region reg = oReg.Clone() as Region;
				if (reg == null) continue;
				regions.Add(reg);
			}

		    Region regRes = regions[0] as Region;
		    for (int i = 1; i < regions.Count; i++)
		    {
		        Region regCurr = regions[i] as Region;
		        Point3dCollection pts = new Point3dCollection();
		        
                // Определяем наличие пересечения регионов
		        regRes.IntersectWith(regCurr, Intersect.OnBothOperands, pts, 0, 0);

		        // В случае пересечения регионов делаем их объединение
		        if (pts != null && pts.Count > 0)
		        {
		            regRes.BooleanOperation(BooleanOperationType.BoolUnite, regCurr);

		            regCurr.Dispose();
		        }
		        else
		        {
		            if (regCurr.Area > regRes.Area)
		            {
		                Region regTest = regRes;
		                regRes = regCurr;
		                regCurr = regTest;
		            }

		            // Создаем временные регионы
		            Region testRegRes = regRes.Clone() as Region;
		            Region testRegCurr = regCurr.Clone() as Region;

		            // Сохраняем текущую площадь региона
		            double saveArea = testRegRes.Area;

		            // Пробуем выполнить операцию объединения
		            testRegRes.BooleanOperation(BooleanOperationType.BoolUnite, testRegCurr);

		            // Сравниваем полученную площадь с сохраненной.
		            // Если площадь не изменилась, значит текущий регион лежит внутри
		            // результирующего и надо выполнить операцию вычитания
		            if (Math.Abs(testRegRes.Area - saveArea) < 0.001)
		            {
		                // Вычитание
		                regRes.BooleanOperation(BooleanOperationType.BoolSubtract, regCurr);
		            }
		                // Площадь изменилась. Объединяем!
		            else
		            {
		                // Объединение
		                regRes.BooleanOperation(BooleanOperationType.BoolUnite, regCurr);
		            }
		            testRegCurr.Dispose();
		            testRegRes.Dispose();
		            regCurr.Dispose();
		        }
		    }

		    DBObjectCollection retObjs = new DBObjectCollection();
		    retObjs.Add(regRes);

		    // Цикл по оставшимся регионам
		    for (int j = 0; j < regions.Count; j++)
		    {
		        Region regCurr = regions[j] as Region;
		        if (regCurr == null || regCurr.IsDisposed || regCurr.IsNull) continue;

		        if (!retObjs.Contains(regCurr))
		            retObjs.Add(regCurr);
		    }
		    return retObjs;
		}

		/// <summary>
		/// Формирование регионов по контурам сечения
		/// </summary>
		/// <param name="curves">Набор кривых</param>
		/// <param name="unoinRegions">Объединять регионы?</param>
		/// <returns>Набор итоговых регионов</returns>
		public static DBObjectCollection MakeRegionsFromCurves(List<Curve> curves, bool unoinRegions)
		{
			// Пустой набор регионов. Возвращаемое значение по умолчанию в случае ошибки ф-ии
			DBObjectCollection retRegions = new DBObjectCollection();

			// Набор объектов для формирования регионов
			DBObjectCollection objsForRegions = new DBObjectCollection();

			// Цикл по всем линиям
			foreach (Curve crv in curves)
			{
				Curve crvClone = crv.Clone() as Curve;
				if (crvClone != null)
				{
					// Добавляем кривую в набор объектов необходимых для формирования регионов
					objsForRegions.Add(crv);
				}

				// ОТЛАДКА
				//objsForRegions.Add(crv.Clone() as Curve);
				//DBRoutine.AddToModelSpace(crv);
			}

			// Формируем регионы по кривым
			try
			{
				if (objsForRegions != null && objsForRegions.Count > 0)
				{
					// Вызов ф-ии формирования регионов по набору линий
					retRegions = Region.CreateFromCurves(objsForRegions);

                    SafelyCreater.Dispose(objsForRegions);
					objsForRegions.Clear();
				}
				else
				{
					return retRegions;
				}
			}
			catch (Exception e1)
			{
				//System.Windows.Forms.MessageBox.Show("Невозможно сформировать регион!", "Region.CreateFromCurves");
				System.Diagnostics.Debug.WriteLine("[Активируйте отладку]GeomUtils.MakeRegionsFromSectionCurves.e1: " + e1.Message + "\n" + e1.StackTrace);
				EditorRoutine.WriteMessage("GeomUtils.MakeRegionsFromSectionCurves.e1: Невозможно сформировать регион!");

                SafelyCreater.Dispose(objsForRegions);
				objsForRegions.Clear();

				return retRegions;
			}

			// Если не надо объединять регионы, то возвращаем набор
			if (!unoinRegions)
			{
				return retRegions;
			}

			DBObjectCollection unRegions = UnionRegions(retRegions);
            SafelyCreater.Dispose(retRegions);
			return unRegions;
		}



		/// <summary>
		/// Сортировка массива точек по удалению от базовой точки
		/// </summary>
		/// <param name="ptFrom">Базовая точка</param>
		/// <param name="pts">Набор точек для сортировки</param>
		/// <returns> <c>true</c> если операции выполнена успешна, иначе <c>false</c> </returns>
		public static bool SortPointsFrom(Point3d ptFrom, List<Point3d> pts)
		{
			if (pts == null) throw new ArgumentNullException("pts");
			if (pts.Count < 2) return false;

			DistanceComparator dcmp = new DistanceComparator(ptFrom);
			pts.Sort(dcmp);

			// Смерть пузырьковым сортировкам!!!!
			//for (int i = 0; i < pts.Count; i++)
			//{
			//    for (int j = i + 1; j < pts.Count; j++)
			//    {
			//        if (ptFrom.DistanceTo(pts[j]) < ptFrom.DistanceTo(pts[i]) + 0.001)
			//        {
			//            Point3d ptTemp = pts[j];
			//            pts[j] = pts[i];
			//            pts[i] = ptTemp;
			//        }
			//    }
			//}
			return true;
		}

		/// <summary>
		/// Сортировка массива точек по приближению к базовой точке
		/// </summary>
		/// <param name="ptFrom">Базовая точка</param>
		/// <param name="pts">Набор точек для сортировки</param>
		/// <returns> <c>true</c> если операции выполнена успешна, иначе <c>false</c> </returns>
		public static bool SortPointsTo(Point3d ptFrom, List<Point3d> pts)
		{
			if (!SortPointsFrom(ptFrom, pts)) return false;
			pts.Reverse();
			return true;
		}

		internal sealed class DistanceComparator : IComparer<Point3d>
		{
			private readonly Point3d _startPoint;

			public DistanceComparator(Point3d startPoint)
			{
				_startPoint = startPoint;
			}

			#region Implementation of IComparer<Point3d>

			public int Compare(Point3d x, Point3d y)
			{
				double distanceX = _startPoint.DistanceTo(x);
				double distanceY = _startPoint.DistanceTo(y);
				return distanceX.CompareTo(distanceY);
			}
			#endregion
		}
	}
}

