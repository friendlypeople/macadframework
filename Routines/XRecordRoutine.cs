// Copyright (c) 2012 Сергей Попов, Виталий Сайдин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Sergey Popov, Vitaliy Saydin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Autodesk.AutoCAD.DatabaseServices;

namespace MAcadFramework.Routines
{
	/// <summary>
	/// Класс облегчающий работу с расширенными записями (XRecord), реализуя часто встречающиеся операции
	/// </summary>
	public class XRecords
	{
		/// <summary>
		/// Создать пустую запись в словаре для сохранения своих данных
		/// </summary>
		/// <param name="dictionaryRecord">Имя записи словаря</param>
		/// <returns> <c>true</c> если операции выполнена успешна, иначе <c>false</c></returns>
		public static bool CreateDicRecord(string dictionaryRecord)
		{
			using (Transaction tr = DBRoutine.StartTransaction())
			{
				if (tr == null) return false;
				try
				{
					using (EditorRoutine.LockDoc())
					{
						using (Xrecord rec = new Xrecord())
						{
							// Создаем пустую запись
							rec.Data = new ResultBuffer(new TypedValue((Int16)DxfCode.Text, ""));

							// Открываем словарь именованных объектов
							DBDictionary dict = (DBDictionary)tr.GetObject(DBRoutine.WorkingDatabase.NamedObjectsDictionaryId, OpenMode.ForWrite, false);

							if (!dict.Contains(dictionaryRecord))
							{
								dict.SetAt(dictionaryRecord, rec);
								tr.AddNewlyCreatedDBObject(rec, true);
							}
							tr.Commit();
							return true;
						}
					}
				}
				catch
				{
					tr.Abort();
					// Не возможно записать настройки
					return false;
				}
			}
		}

		/// <summary>
		/// Удаление словаря
		/// </summary>
		/// <param name="dictionaryRecord">Имя записи словаря</param>
		/// <returns> <c>true</c> если операции выполнена успешна, иначе <c>false</c> </returns>
		public static bool Reset(string dictionaryRecord)
		{
			using (Transaction tr = DBRoutine.StartTransaction())
			{
				if (tr == null) return false;
				try
				{
					using (EditorRoutine.LockDoc())
					{
						// Открываем словарь именованных объектов
						DBDictionary dict = (DBDictionary)tr.GetObject(DBRoutine.WorkingDatabase.NamedObjectsDictionaryId, OpenMode.ForWrite, false);

						if (dict.Contains(dictionaryRecord))
						{
							dict.Remove(dictionaryRecord);
						}
						tr.Commit();
						return true;
					}
				}
				catch
				{
					tr.Abort();
					// Не возможно
					return false;
				}
			}
		}

        /// <summary>
        /// Преобразование в строку стека.
        /// </summary>
		private static List<string> ConvertToStringStack(Stream ms)
		{
			List<string> sb = new List<string>();
			ms.Position = 0;
			int readSize = 768;
			long ost = ms.Length;
			while (ost != 0)
			{
				if (ost < 768)
					readSize = (int)ost;

				byte[] buffer = new byte[readSize];
				ost -= ms.Read(buffer, 0, readSize);
				string str = Convert.ToBase64String(buffer);
				sb.Add(str);
			}
			return sb;
		}

		/// <summary>
		/// Записать данные.
		/// </summary>
		/// <param name="dictionaryRecord">Имя записи словаря.</param>
		/// <param name="stream">Поток.</param>
		/// <returns>  <c>true</c> если операции выполнена успешна, иначе <c>false</c> </returns>
		public static bool WriteData(string dictionaryRecord, Stream stream)
		{
			using (Transaction tr = DBRoutine.StartTransaction())
			{
				if (tr == null) return false;
				try
				{
					using (EditorRoutine.LockDoc())
					{
						using (Xrecord rec = new Xrecord())
						{
							stream.Position = 0;
							//StreamReader sr = new StreamReader(stream, Encoding.Default, false);
							
							ResultBuffer rb = new ResultBuffer(new TypedValue((Int16) DxfCode.Text, "<scdcpool"));
							List<string> list = ConvertToStringStack(stream);
							foreach (string str in list)
								rb.Add(new TypedValue((Int16)DxfCode.Text, str));

							////int start = 0;
							//// Из 768 байт получиться 1024 символа Base64 строки.
							//int readSize = 768;
							//long ost = stream.Length;
							//while (ost != 0)
							//{
							//    // = stream.Length - start;
							//    if (ost < 768) readSize = (int) ost;

							//    byte[] buffer = new byte[readSize];
							//    ost -= stream.Read(buffer, 0, readSize);
							//    string str = Convert.ToBase64String(buffer);
								
							//}
							rb.Add(new TypedValue((Int16) DxfCode.Text, "scdcpool>"));
							rec.Data = rb;
							//sr.Close();

							DBDictionary dict =
								(DBDictionary) tr.GetObject(DBRoutine.WorkingDatabase.NamedObjectsDictionaryId, OpenMode.ForWrite, false);

							if (dict.Contains(dictionaryRecord))
							{
								dict.SetAt(dictionaryRecord, rec);
								tr.AddNewlyCreatedDBObject(rec, true);
							}
						}
						tr.Commit();
						return true;
					}
				}
				catch {  }
				return false;
			}
		}

		/// <summary>
		/// Сброс настроек в словарь
		/// </summary>
		/// <param name="dictionaryRecord">Имя записи словаря.</param>
		/// <param name="settingString">The setting string.</param>
		/// <returns> <c>true</c> если операции выполнена успешна, иначе <c>false</c> </returns>
		public static bool WriteSettings(string dictionaryRecord, string settingString)
		{
			using (Transaction tr = DBRoutine.StartTransaction())
			{
				if (tr == null) return false;
				try
				{
					using (EditorRoutine.LockDoc())
					{
						using (Xrecord rec = new Xrecord())
						{
							// Если длина строки > 1024
							if (settingString.Length > 1024)
							{
								// Разобъем строку на подстроки длиной до 1024 символов
								string temp = new string(settingString.ToCharArray());

								int start = 0;
								int length = 1024;

								List<String> strings = new List<string>();

								while (!string.IsNullOrEmpty(temp))
								{
									string s = temp.Substring(start, Math.Min(length, temp.Length));
									temp = temp.Remove(start, Math.Min(length, temp.Length));
									strings.Add(s);
								}

								ResultBuffer rb = new ResultBuffer(new TypedValue((Int16)DxfCode.Text, ""));

								foreach (String str in strings)
								{
									try
									{
										rb.Add(new TypedValue((Int16)DxfCode.Text, str));
									}
									catch (Exception eee)
									{

									}
								}
								rec.Data = rb;
							}
							else
							{
								rec.Data = new ResultBuffer(new TypedValue((Int16)DxfCode.Text, settingString));
							}

							DBDictionary dict = (DBDictionary)tr.GetObject(DBRoutine.WorkingDatabase.NamedObjectsDictionaryId, OpenMode.ForWrite, false);

							if (dict.Contains(dictionaryRecord))
							{
								//dict.GetAt("MBKS_SETTINGS")
								dict.SetAt(dictionaryRecord, rec);
								tr.AddNewlyCreatedDBObject(rec, true);
							}
						}
						tr.Commit();
						return true;
					}
				}
				catch//(Exception exc)
				{
					tr.Abort();
					// Не возможно записать настройки
					//System.Windows.Forms.MessageBox.Show("Не возможно записать настройки");
					return false;
				}
			}
		}

		/// <summary>
		/// Загрузка настроек из словаря
		/// </summary>
		/// <param name="dictionaryRecord">Имя записи словаря.</param>
		/// <returns>Загруженная строка</returns>
		public static string ReadSettings(string dictionaryRecord)
		{
			using (Transaction tr = DBRoutine.StartTransaction())
			{
				if (tr == null || DBRoutine.WorkingDatabase == null) return String.Empty;
				try
				{
					DBDictionary dict = (DBDictionary)tr.GetObject(DBRoutine.WorkingDatabase.NamedObjectsDictionaryId, OpenMode.ForRead, false);

					if (dict.Contains(dictionaryRecord))
					{
						//dict.GetAt("MBKS_SETTINGS")
						ObjectId xrecId = dict.GetAt(dictionaryRecord);

						DBObject obj = tr.GetObject(xrecId, OpenMode.ForRead);
						if (obj is Xrecord)
						{
							Xrecord xr = (Xrecord)obj;
							ResultBuffer rb = xr.Data;
							tr.Commit();

							if (rb != null)
							{
								TypedValue[] tvs = rb.AsArray();
								if (tvs.Length < 1) return String.Empty;

								if (tvs.Length > 1)
								{
									// Составная строка
									string fullString = string.Empty;
									// Флаг изменения типа в последовательности.
									// Последовательность должна состоять из строк.
									// Если встречаем иной тип данных когда fullString уже не пуста, то прерываем операцию
									// и возвращаем то, что имеем в fullString
									bool br = false;
									for (int i = 0; i < tvs.Length; i++)
									{
										if (br) break;
										if (tvs[i].TypeCode == (Int16)DxfCode.Text)
										{
											fullString += tvs[i].Value.ToString();
										}
										else if (string.IsNullOrEmpty(fullString))
										{
											br = true;
										}
									}
									return fullString;
								}
							    return tvs[0].Value.ToString();
							}
							return String.Empty;
						}
					}
					tr.Commit();
					return String.Empty;
				}
				catch
				{
					tr.Abort();
					// Не возможно считать настройки
					//System.Windows.Forms.MessageBox.Show("Не возможно считать настройки");
					return String.Empty;
				}
			}
		}

		/// <summary>
		/// Преобразование из строки стека.
		/// </summary>
		private static Stream ConvertFromStringStack(List<string> list)
		{
			MemoryStream ms = new MemoryStream();
			foreach (string str in list)
			{
				byte[] buffer = Convert.FromBase64String(str);
				ms.Write(buffer, 0, buffer.Length);
			}
			return ms;
		}

		/// <summary>
		/// Читать данные из словаря.
		/// </summary>
		/// <param name="dictionaryRecord">Имя записи словаря.</param>
		/// <param name="db">База данных AutoCAD.</param>
		/// <returns>Поток содержащий прочитанные данные</returns>
		public static Stream ReadData(string dictionaryRecord, Database db)
		{
			if (db == null) return null;
			try
			{
				ResultBuffer rb = null;
				using (Transaction tr = db.TransactionManager.StartTransaction())
				{
					if (tr == null) return null;
					DBDictionary dict = (DBDictionary) tr.GetObject(db.NamedObjectsDictionaryId, OpenMode.ForRead, false);

					if (!dict.Contains(dictionaryRecord)) return null;
					
					ObjectId xrecId = dict.GetAt(dictionaryRecord);
					DBObject obj = tr.GetObject(xrecId, OpenMode.ForRead);
					if (obj is Xrecord)
					{
						Xrecord xr = (Xrecord)obj;
						rb = xr.Data;
						tr.Commit();
					}
				}
				if (rb != null)
				{
					TypedValue[] tvs = rb.AsArray();
					if (tvs.Length < 1) return null;
					//ms = new MemoryStream(4096);
					bool startRead = false;
					List<string> list = new List<string>();
					for (int i = 0; i < tvs.Length; i++)
					{
						if (tvs[i].TypeCode == (Int16)DxfCode.Text)
						{
							if (!startRead && tvs[i].Value.ToString().Equals("<scdcpool"))
							{
								startRead = true;
								continue;
							}
							if (startRead)
							{
								string str = tvs[i].Value.ToString();
								if (str.Equals("scdcpool>")) break;
								list.Add(str);
							}
						}
					}
					Stream stream = ConvertFromStringStack(list);
					stream.Position = 0;
					return stream;
				}

			}
			catch { }
			return null;
		}

		/// <summary>
		/// Читать данные из словаря.
		/// </summary>
		/// <param name="dictionaryRecord">Имя записи словаря.</param>
		/// <param name="db">База данных AutoCAD.</param>
		/// <returns> Строка содержащая прочитанные данные </returns>
		public static string ReadSettings(string dictionaryRecord, Database db)
		{
			if (db == null) return String.Empty;
			using (Transaction tr = db.TransactionManager.StartTransaction())
			{
				if (tr == null) return String.Empty;
				try
				{
					//Xrecord rec = new Xrecord();
					//rec.Data = new ResultBuffer(new TypedValue((Int16)DxfCode.Text, settingString));
					DBDictionary dict = (DBDictionary)tr.GetObject(db.NamedObjectsDictionaryId, OpenMode.ForRead, false);

					if (dict.Contains(dictionaryRecord))
					{
						//dict.GetAt("MBKS_SETTINGS")
						ObjectId xrecId = dict.GetAt(dictionaryRecord);

						DBObject obj = tr.GetObject(xrecId, OpenMode.ForRead);
						if (obj is Xrecord)
						{
							Xrecord xr = (Xrecord)obj;
							ResultBuffer rb = xr.Data;
							tr.Commit();

							if (rb != null)
							{
								TypedValue[] tvs = rb.AsArray();
								if (tvs.Length < 1) return String.Empty;

								if (tvs.Length > 1)
								{
									// Составная строка
									StringBuilder sb = new StringBuilder(10);
									bool startRead = true;
									//string fullString = string.Empty;
									// Флаг изменения типа в последовательности.
									// Последовательность должна состоять из строк.
									// Если встречаем иной тип данных когда fullString уже не пуста, то прерываем операцию
									// и возвращаем то, что имеем в fullString
									bool br = false;
									for (int i = 0; i < tvs.Length; i++)
									{
										if (br) break;
										if (tvs[i].TypeCode == (Int16)DxfCode.Text)
										{
											sb.Append(tvs[i].Value.ToString());
											startRead = false;
										}
										else if (!startRead)
										{
											br = true;
										}
									}
									return sb.ToString();
								}
								return tvs[0].Value.ToString();
							}
							return String.Empty;
						}
					}
					tr.Commit();
					return String.Empty;
				}
				catch
				{
					tr.Abort();
					// Не возможно считать настройки
					//System.Windows.Forms.MessageBox.Show("Не возможно считать настройки");
					return String.Empty;
				}
			}
		}
	}

}
